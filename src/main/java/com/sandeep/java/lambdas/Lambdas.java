package com.sandeep.java.lambdas;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by smahanty on 12/7/16.
 */
public class Lambdas {

    public static void process(Runnable r) {
        r.run();
    }

    public static String processFile(FileProcessor f) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("testData/lambdas.txt"))) {
            return f.process(br);
        }
    }

    public static void main(String[] args) throws IOException {
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("Ran with anonymous class");
            }
        };
        Runnable r2 = () -> System.out.println("Ran with lambda variable");
        process(r1);
        process(r2);
        process(() -> System.out.println("Ran with lambda expression directly passed to method."));
        System.out.println(processFile((BufferedReader br) -> br.readLine()));
        System.out.println(processFile((BufferedReader br) -> br.readLine() + "\n" + br.readLine()));
    }
}
