package com.sandeep.java.lambdas;

import com.sandeep.java.common.Dish;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by smahanty on 12/20/16.
 */
public class Streams {
    public static void main(String[] args) {
        List<Dish> menu = Arrays.asList(
                new Dish("pork", false, 800, Dish.Type.MEAT), new Dish("beef", false, 700, Dish.Type.MEAT), new Dish("chicken", false, 400, Dish.Type.MEAT),
                new Dish("french fries", true, 530, Dish.Type.OTHER), new Dish("rice", true, 350, Dish.Type.OTHER),
                new Dish("season fruit", true, 120, Dish.Type.OTHER), new Dish("pizza", true, 550, Dish.Type.OTHER),
                new Dish("prawns", false, 300, Dish.Type.FISH), new Dish("salmon", false, 450, Dish.Type.FISH));

        findThreeHighCaloriesBySequentialStream(menu);
        findThreeHighCalories(menu);
        findThreeHighCaloriesByParallelStream(menu);
    }

    public static void findThreeHighCaloriesBySequentialStream(List<Dish> menu) {
        long startTime = System.currentTimeMillis();
        List<String> threeHighCalorieDishes = menu.stream().filter(d -> d.getCalories() > 300).map(Dish::getName).limit(3).collect(Collectors.toList());
        System.out.println("List: " + threeHighCalorieDishes);
        long stopTime = System.currentTimeMillis();
        System.out.println("Time taken for streams : " + (stopTime - startTime));
    }

    public static void findThreeHighCalories(List<Dish> menu) {
        long startTime = System.currentTimeMillis();
        List<String> result = new ArrayList<>();
        int count = 0;
        for (Dish dish : menu) {
            if (dish.getCalories() > 300) {
                count++;
                result.add(dish.getName());
            }
            if (count >= 3) {
                break;
            }
        }
        System.out.println("List: " + result);
        long stopTime = System.currentTimeMillis();
        System.out.println("Time taken for normal: " + (stopTime - startTime));
    }

    public static void findThreeHighCaloriesByParallelStream(List<Dish> menu) {
        long startTime = System.currentTimeMillis();
        List<String> threeHighCalorieDishes = menu.parallelStream().filter(d -> d.getCalories() > 300).map(Dish::getName).limit(3).collect(Collectors.toList());
        System.out.println("List: " + threeHighCalorieDishes);
        long stopTime = System.currentTimeMillis();
        System.out.println("Time taken for parallel: " + (stopTime - startTime));
    }
}
