package com.sandeep.java.lambdas;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by smahanty on 12/7/16.
 */
@FunctionalInterface
public interface FileProcessor {
    public String process(BufferedReader br) throws IOException;
}
