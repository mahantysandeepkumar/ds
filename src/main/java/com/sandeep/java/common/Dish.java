package com.sandeep.java.common;

/**
 * Created by smahanty on 12/20/16.
 */
public class Dish {
    private final String name;
    private final boolean vegetarian;
    private final int calories;
    private final Type type;

    public Dish(String name, boolean isVegetarian, int calories, Type type) {
        this.name = name;
        this.vegetarian = isVegetarian;
        this.calories = calories;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public int getCalories() {
        return calories;
    }

    public Type getType() {
        return type;
    }

    public enum Type {MEAT, FISH, OTHER}

    @Override
    public String toString() {
        return "[name=" + this.name + ", vegetarian=" + vegetarian + ", calories=" + this.calories + ", type=" + type.name() + "]";
    }
}
