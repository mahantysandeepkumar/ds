package com.sandeep.java.common;

/**
 * Created by smahanty on 11/18/16.
 */
public abstract class Fruit {
    protected String color;
    protected int weight;

    public abstract String getColor();

    public abstract int getWeight();
}
