package com.sandeep.java.common;

/**
 * Created by smahanty on 11/18/16.
 */
public class Apple extends Fruit {
    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }

    public Apple(String color, int weight) {
        this.color = color;
        this.weight = weight;
    }

    public Apple() {

    }
}
