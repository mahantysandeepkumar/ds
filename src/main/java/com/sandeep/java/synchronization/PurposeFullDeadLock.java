package com.sandeep.java.synchronization;


public class PurposeFullDeadLock {

    private String obj1 = new String("One");
    private String obj2 = new String("Two");

    public static void main(String[] args) throws Exception {
        final PurposeFullDeadLock obj = new PurposeFullDeadLock();
        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                try {
                    obj.printOneTwo();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        }

        new Thread(() -> {
            for (int i = 0; i < 2; i++) {
                new Thread(() -> {
                    try {
                        obj.printTwoOne();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).run();
            }
        }).start();
    }

    public void printOneTwo() throws Exception {
        synchronized (obj1) {
            Thread.sleep(1000);
            synchronized (obj2) {
                System.out.print(obj1);
                System.out.println(obj2);
            }
        }
    }

    public void printTwoOne() throws Exception {
        synchronized (obj2) {
            Thread.sleep(1000);
            synchronized (obj1) {
                System.out.print(obj2);
                System.out.println(obj1);
            }
        }
    }
}
