package com.sandeep.java.synchronization;

import java.util.concurrent.Semaphore;

/**
 * Created by smahanty on 5/18/17.
 */
public class SemaphoreTest {
    /**
     * A semaphore controls access to a shared resource through the use of a counter. If the counter is greater than zero,
     * then access is allowed. If it is zero, then access is denied. What the counter is counting are permits that allow
     * access to the shared resource. Thus, to access the resource, a thread must be granted a permit from the semaphore.
     */

    int n;
    static Semaphore semConsumer = new Semaphore(0);
    static Semaphore semProducer = new Semaphore(1);

    void get() {
        try {
            semConsumer.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
        System.out.println("Got : " + n);
        semProducer.release();
        System.out.println("Locks: Consumer: " + semConsumer.toString() + "     Producer: " + semProducer.toString());
    }

    void put(int n) {
        try {
            semProducer.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Exception: " + ex.getMessage());
        }
        this.n = n;
        System.out.println("Put : " + n);
        semConsumer.release();
        System.out.println("Locks: Consumer: " + semConsumer.toString() + "     Producer: " + semProducer.toString());
    }

    public static void main(String[] args) {
        SemaphoreTest obj = new SemaphoreTest();
        new Consumer(obj);
        new Producer(obj);
    }
}

class Producer implements Runnable {
    SemaphoreTest st;

    public Producer(SemaphoreTest st) {
        this.st = st;
        new Thread(this, "Producer").start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            st.put(i);
        }
    }
}

class Consumer implements Runnable {
    SemaphoreTest st;

    public Consumer(SemaphoreTest st) {
        this.st = st;
        new Thread(this, "Consumer").start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            st.get();
        }
    }
}


