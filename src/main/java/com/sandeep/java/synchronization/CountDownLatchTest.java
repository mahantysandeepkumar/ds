package com.sandeep.java.synchronization;

import java.util.concurrent.CountDownLatch;

/**
 * Created by smahanty on 5/18/17.
 */
public class CountDownLatchTest {
    /**
     * Sometimes you will want a thread to wait until one or more events have occurred. To handle such a situation, the
     * concurrent API supplies CountDownLatch. A CountDownLatch is initially created with a count of the number of
     * events that must occur before the latch is released. Each time an event happens, the count is decremented. When
     * the count reaches zero, the latch opens.
     */

    public static void main(String[] args) {
        CountDownLatch cdl = new CountDownLatch(5);
        new MyThread(cdl);

        try {
            cdl.await();
        } catch (InterruptedException ex) {
            System.out.printf("Interrupted Exception: " + ex.getMessage());
        }
        System.out.println("Done");
    }

}

class MyThread implements Runnable {

    CountDownLatch cdl;

    public MyThread(CountDownLatch cdl) {
        this.cdl = cdl;
        new Thread(this, "MyThread").start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Latch: " + i);
            cdl.countDown();
        }
    }
}
