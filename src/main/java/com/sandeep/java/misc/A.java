package com.sandeep.java.misc;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by smahanty on 5/15/17.
 */
public class A implements Serializable {
    byte[] bytes = new byte[1024];
    private B a;

    public B getB() {
        return new B();
    }

    private class B implements Serializable {

    }
    public static void main(String[] args) throws IOException, InterruptedException {
        B b = new A().getB();
        ByteArrayOutputStream ous = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(ous);
        out.writeObject(b);
        out.flush();
        HashMap<String, String> map = new HashMap<>();
        Set<Map.Entry<String, String>> entries = map.entrySet();
        System.out.println("Out: " + ous.toByteArray().length);


        ArrayBlockingQueue<Integer> ctl = new ArrayBlockingQueue<Integer>(5);

        for (int i = 0; i < 5; i++) {
            ctl.put(i);
        }
        ctl.offer(1);
        System.out.println(ctl);
    }
}
