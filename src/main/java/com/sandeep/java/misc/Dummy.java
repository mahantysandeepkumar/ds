package com.sandeep.java.misc;

import java.io.IOException;
import java.util.Arrays;

class AB {
    private void print() {
        System.out.println("Prints without exception A");
    }

    public void printWithException() throws IndexOutOfBoundsException {
        System.out.println("Prints with exception A");
    }
}

class CD extends AB {
    public void print() {
        System.out.println("Prints without exception A");
    }

    public void printWithException() throws ArrayIndexOutOfBoundsException {

    }
}

class Dummy {

    public static void main(String[] args) {
        foo("hello world".toCharArray());
    }

    static void foo(char[] c) {
        int end = c.length -1;
        int start = 0;
        while(start < end){
            char temp  = c[start];
            c[start] = c[end];
            c[end--] = temp;
            start++;

        }
        System.out.println(Arrays.toString(c));
    }
}
