package com.sandeep.java.util;

import org.testng.Assert;

import java.util.Base64;

/**
 * Created by smahanty on 11/18/16.
 */
public class Base64EncoderDecoder {
    public static String encode(String input) {
        byte[] bytes = Base64.getEncoder().encode(input.getBytes());
        return new String(bytes);
    }

    public static String decode(String input) {
        byte[] bytes = Base64.getDecoder().decode(input.getBytes());
        return new String(bytes);
    }

    public static void main(String[] args) {
        String input = "DDNGrw-2Zzc-LV5A-uemhF9c-jjCQMU-uAzG:bLqp9t-HbLzQPX-DLG2Tqh3-gcbSq-zzxD4z";
        String encodedString = encode(input);
        String decodedString = decode(encodedString);
        System.out.println("Encode: " + encodedString);
        System.out.println("Decode: " + decodedString);
        Assert.assertTrue(input.equals(decodedString));
    }
}