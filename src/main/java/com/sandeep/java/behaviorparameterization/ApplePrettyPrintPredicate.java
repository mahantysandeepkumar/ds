package com.sandeep.java.behaviorparameterization;

import com.sandeep.java.common.Apple;

/**
 * Created by smahanty on 11/18/16.
 */
public interface ApplePrettyPrintPredicate {
    public String prettyString(Apple apple);
}
