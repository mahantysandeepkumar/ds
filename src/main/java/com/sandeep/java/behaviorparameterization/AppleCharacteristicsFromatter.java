package com.sandeep.java.behaviorparameterization;

import com.sandeep.java.common.Apple;

/**
 * Created by smahanty on 11/18/16.
 */
public class AppleCharacteristicsFromatter implements ApplePrettyPrintPredicate {
    @Override
    public String prettyString(Apple apple) {
        String characteristic = (apple.getWeight() > 150 ? "heavy" : "light");
        return "A " + characteristic + " " + apple.getColor() + " apple";
    }
}
