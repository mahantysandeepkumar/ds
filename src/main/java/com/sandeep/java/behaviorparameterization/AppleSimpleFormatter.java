package com.sandeep.java.behaviorparameterization;

import com.sandeep.java.common.Apple;

/**
 * Created by smahanty on 11/18/16.
 */
public class AppleSimpleFormatter implements ApplePrettyPrintPredicate {
    @Override
    public String prettyString(Apple apple) {

        return "An apple of " + apple.getWeight() + "g";
    }
}
