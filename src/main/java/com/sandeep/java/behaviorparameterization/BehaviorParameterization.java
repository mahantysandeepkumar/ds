package com.sandeep.java.behaviorparameterization;

import com.sandeep.java.common.Apple;

/**
 * Created by smahanty on 11/18/16.
 */
public class BehaviorParameterization {
    public String getApplePrettyPrint(Apple apple, ApplePrettyPrintPredicate predicate) {
        return predicate.prettyString(apple);
    }
}
