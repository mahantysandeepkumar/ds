package com.sandeep.java.designpatterns.creational.singleton;

/**
 * Created by smahanty on 5/29/17.
 */
public class BillPughSingleton {
    private static class SingletonHelper {
        private static final BillPughSingleton INSTANCE = new BillPughSingleton();
    }

    private BillPughSingleton() {

    }

    public static BillPughSingleton getInstance() {
        return SingletonHelper.INSTANCE;
    }
}
