package com.sandeep.java.designpatterns.creational.factory;

/**
 * Created by smahanty on 5/29/17.
 */
public class GamingPC implements Computer {
    private String cpu;
    private String gpu;
    private int hdd;
    private int ram;

    @Override
    public String getCpu() {
        return cpu;
    }

    @Override
    public int getRam() {
        return ram;
    }

    @Override
    public int getHdd() {
        return hdd;
    }

    @Override
    public String getGPU() {
        return gpu;
    }

    public GamingPC(String cpu, int ram, int hdd, String gpu) {
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
        this.gpu = gpu;
    }

    @Override
    public String toString() {
        return "GamingPC{" +
                "cpu='" + cpu + '\'' +
                ", hdd=" + hdd +
                ", ram=" + ram +
                ", gpu=" + gpu +
                '}';
    }
}
