package com.sandeep.java.designpatterns.creational.prototype;

/**
 * Created by smahanty on 6/10/17.
 */
public interface Item extends Cloneable {
    // you can only clone this if the objects extends Cloneable
    Item clone();
}
