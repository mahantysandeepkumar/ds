package com.sandeep.java.designpatterns.creational.strategy;

/**
 * Created by smahanty on 5/3/17.
 */
public class NoFly implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("This duck cannot fly");
    }
}
