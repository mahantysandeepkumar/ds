package com.sandeep.java.designpatterns.creational.prototype;

/**
 * Created by smahanty on 6/10/17.
 */
public class Book implements Item {
    private String title;
    private String author;
    private double price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public Item clone() {
        Item clonedItem = null;
        try {
            clonedItem = (Item) super.clone();
            // This item can be modified here if required
        } catch (Exception ex) {
            System.out.println("Could not clone due to " + ex.getMessage());
            ex.printStackTrace();
        }
        return clonedItem;
    }

}
