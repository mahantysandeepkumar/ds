package com.sandeep.java.designpatterns.creational.singleton;

/**
 * Created by smahanty on 5/29/17.
 */
public class DoubleCheckLockingSingleton {
    private static DoubleCheckLockingSingleton INSTANCE;

    private DoubleCheckLockingSingleton() {

    }

    public static DoubleCheckLockingSingleton getInstance() {
        if (INSTANCE == null) {
            synchronized (DoubleCheckLockingSingleton.class) {
                if (INSTANCE == null) {
                    INSTANCE = new DoubleCheckLockingSingleton();
                }
            }
        }
        return INSTANCE;
    }
}
