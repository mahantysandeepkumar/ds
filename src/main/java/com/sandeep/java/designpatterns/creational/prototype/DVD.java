package com.sandeep.java.designpatterns.creational.prototype;

/**
 * Created by smahanty on 6/10/17.
 */
public class DVD implements Item {

    private String title;
    private String artist;
    private double price;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "DVD{" +
                "title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", price=" + price +
                '}';
    }

    @Override
    public Item clone() {
        Item clonedItem = null;
        try {
            clonedItem = (Item) super.clone();
            // This item can be modified here if required
        } catch (Exception ex) {
            System.out.println("Could not clone due to " + ex.getMessage());
        }
        return clonedItem;
    }
}
