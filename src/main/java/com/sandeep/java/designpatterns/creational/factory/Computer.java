package com.sandeep.java.designpatterns.creational.factory;

/**
 * Created by smahanty on 5/29/17.
 */
public interface Computer {

    public String getGPU();

    public String getCpu();

    public int getRam();

    public int getHdd();

}
