package com.sandeep.java.designpatterns.creational.factory;

/**
 * Created by smahanty on 5/29/17.
 */
public class ServerPCFactory implements AbstractComputerFactory {

    private String cpu;
    private int hdd;
    private int ram;

    public ServerPCFactory(String cpu, int ram, int hdd) {
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
    }

    @Override
    public Computer createComputer() {
        return new ServerPC(cpu, ram, hdd, null);
    }
}