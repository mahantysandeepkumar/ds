package com.sandeep.java.designpatterns.creational.builder;

/**
 * Created by smahanty on 5/29/17.
 */
public class Clan {
    private String name;
    private boolean defence;
    private boolean builder;
    private boolean army;
    private boolean resources;

    public String getName() {
        return name;
    }

    public boolean isDefence() {
        return defence;
    }

    public boolean isBuilder() {
        return builder;
    }

    public boolean isArmy() {
        return army;
    }

    public boolean isResources() {
        return resources;
    }

    @Override
    public String toString() {
        return "Clan{" +
                "name='" + name + '\'' +
                ", defence=" + defence +
                ", builder=" + builder +
                ", army=" + army +
                ", resources=" + resources +
                '}';
    }

    public static class ClanBuilder {
        private String name;
        private boolean defence;
        private boolean builder;
        private boolean army;
        private boolean resources;

        public ClanBuilder setDefence(boolean defence) {
            this.defence = defence;
            return this;
        }

        public ClanBuilder setBuilder(boolean builder) {
            this.builder = builder;
            return this;
        }

        public ClanBuilder setArmy(boolean army) {
            this.army = army;
            return this;
        }

        public ClanBuilder setResources(boolean resources) {
            this.resources = resources;
            return this;
        }

        public Clan build() {
            return new Clan(this);
        }

        public ClanBuilder(String name) {
            this.name = name;
        }
    }

    public Clan(ClanBuilder clanBuilder) {
        this.army = clanBuilder.army;
        this.builder = clanBuilder.builder;
        this.defence = clanBuilder.defence;
        this.resources = clanBuilder.resources;
        this.name = clanBuilder.name;
    }
}
