package com.sandeep.java.designpatterns.creational.strategy;

/**
 * Created by smahanty on 5/3/17.
 */
public interface FlyBehavior {
    void fly();
}
