package com.sandeep.java.designpatterns.creational.factory;

/**
 * Created by smahanty on 5/29/17.
 */
public class GamingPCFactory implements AbstractComputerFactory {

    private String cpu;
    private String gpu;
    private int hdd;
    private int ram;

    public GamingPCFactory(String cpu, int ram, int hdd, String gpu) {
        this.cpu = cpu;
        this.ram = ram;
        this.hdd = hdd;
        this.gpu = gpu;
    }

    @Override
    public Computer createComputer() {
        return new GamingPC(cpu, ram, hdd, gpu);
    }
}
