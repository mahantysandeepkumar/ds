package com.sandeep.java.designpatterns.creational.prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smahanty on 5/29/17.
 */
public class Employees {
    private List<String> employees;

    public void loadData() {
        employees.add("Sandeep");
        employees.add("Kumar");
        employees.add("Mahanty");
    }

    public Employees clone() {
        final List<String> emps = new ArrayList<>();
        employees.stream().forEach(emp -> {
            emps.add(emp);
        });
        return new Employees(emps);
    }

    public List<String> getEmployees() {
        return employees;
    }

    public Employees(List<String> employees) {
        this.employees = employees;
    }

    public Employees() {
        this.employees = new ArrayList<>();
    }
}
