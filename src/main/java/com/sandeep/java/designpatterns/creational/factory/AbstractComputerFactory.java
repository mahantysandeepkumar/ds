package com.sandeep.java.designpatterns.creational.factory;

/**
 * Created by smahanty on 5/29/17.
 */
public interface AbstractComputerFactory {
    public Computer createComputer();
}
