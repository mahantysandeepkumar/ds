package com.sandeep.java.designpatterns.creational.factory;

/**
 * Created by smahanty on 5/29/17.
 */
public class ComputerFactory {
    public static Computer getComputer(String type, String cpu, String gpu, int ram, int hdd) {
        if (type.equalsIgnoreCase("GAMINGPC")) {
            return new GamingPC(cpu, ram, hdd, gpu);
        } else if (type.equalsIgnoreCase("SERVER")) {
            return new ServerPC(cpu, ram, hdd, null);
        }
        return null;
    }

    public static Computer getComputer(AbstractComputerFactory factory) {
        return factory.createComputer();
    }
}
