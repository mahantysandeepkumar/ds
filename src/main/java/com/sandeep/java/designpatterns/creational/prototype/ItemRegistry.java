package com.sandeep.java.designpatterns.creational.prototype;

import java.util.HashMap;

/**
 * Created by smahanty on 6/10/17.
 */
public class ItemRegistry {
    HashMap<String, Item> itemHashMap;

    public void registerItem(String itemType, Item item) {
        itemHashMap.put(itemType, item);
    }

    public Item getItem(String itemType) {
        return itemHashMap.get(itemType).clone();
    }

    public ItemRegistry() {
        this.itemHashMap = new HashMap<>();
    }
}
