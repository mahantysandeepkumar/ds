package com.sandeep.java.designpatterns.creational.prototype;

/**
 * Created by smahanty on 6/10/17.
 */
public class ItemStore {
    public static void main(String[] args) {

        ItemRegistry itemRegistry = new ItemRegistry();

        //Register Items
        Book book = new Book();
        book.setAuthor("Sandeep");
        book.setTitle("Testament");
        book.setPrice(1234.5);
        DVD dvd = new DVD();
        dvd.setTitle("Mass Effect");
        dvd.setArtist("Bioware");
        dvd.setPrice(3500.00);
        itemRegistry.registerItem("Book", book);
        itemRegistry.registerItem("DVD", dvd);

        Book clonedBook = (Book) itemRegistry.getItem("Book");
        DVD clonedDvd = (DVD) itemRegistry.getItem("DVD");
        System.out.println("Book : " + book);
        System.out.println("DVD: " + dvd);
    }
}
