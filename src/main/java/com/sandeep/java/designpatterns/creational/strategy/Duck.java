package com.sandeep.java.designpatterns.creational.strategy;

/**
 * Created by smahanty on 5/3/17.
 */
public abstract class Duck {
    FlyBehavior flyBehavior;
    QuackBehavior quackBehavior;

    public void swim() {
        System.out.println("The duck is swimming");
    }

    void performQuack() {
        flyBehavior.fly();
    }

    void performFly() {
        quackBehavior.quack();
    }
}
