package com.sandeep.java.designpatterns.structural.adapter;

import sun.security.provider.SHA;

/**
 * Created by smahanty on 6/10/17.
 */
public class LineAdapter implements Shape {
    Line line = new Line();

    @Override
    public void draw(int x, int y, int width, int height) {
        line.drawLine(x, y, width, height);
    }
}
