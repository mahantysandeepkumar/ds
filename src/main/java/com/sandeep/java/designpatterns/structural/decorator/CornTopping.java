package com.sandeep.java.designpatterns.structural.decorator;

public class CornTopping implements ToppingsDecorator {
    private Pizza pizza;

    @Override
    public double getPrice() {
        return pizza.getPrice() + 0.25;
    }

    @Override
    public String getDescription() {
        return this.pizza.getDescription() + ", corn topping";
    }

    public CornTopping(Pizza pizza) {
        this.pizza = pizza;
    }
}
