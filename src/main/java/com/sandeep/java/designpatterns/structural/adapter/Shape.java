package com.sandeep.java.designpatterns.structural.adapter;

/**
 * Created by smahanty on 6/10/17.
 */
public interface Shape {
    void draw(int x, int y, int width, int height);
}
