package com.sandeep.java.designpatterns.structural.proxy;

/**
 * Created by smahanty on 5/30/17.
 */
public interface CommandExecutor {
    boolean execute(String command);
}
