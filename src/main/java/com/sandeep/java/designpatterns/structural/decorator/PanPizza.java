package com.sandeep.java.designpatterns.structural.decorator;

public class PanPizza implements Pizza {
    private String decription = "Medium sized pan pizza";

    @Override
    public double getPrice() {
        return 100.0;
    }

    @Override
    public String getDescription() {
        return decription;
    }
}
