package com.sandeep.java.designpatterns.structural.proxy;

/**
 * Created by smahanty on 5/30/17.
 */
public class CommandExecutorProxy implements CommandExecutor {
    private boolean isAdmin;
    CommandExecutor commandExecutor;

    public CommandExecutorProxy(String username, String password) {
        if (username.equalsIgnoreCase("sandeep") && password.equalsIgnoreCase("sandeep")) {
            isAdmin = true;
        }
        commandExecutor = new CommandExecutorImpl();
    }

    @Override
    public boolean execute(String command) {
        if (isAdmin) {
            return commandExecutor.execute(command);
        } else {
            if (command.trim().startsWith("rm")) {
                return false;
            } else {
                return commandExecutor.execute(command);
            }
        }
    }
}
