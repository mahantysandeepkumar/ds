package com.sandeep.java.designpatterns.structural.adapter;

/**
 * Created by smahanty on 6/10/17.
 */
public class DrawingPad {
    public static void main(String[] args) {
        DrawingPad dp = new DrawingPad();
        dp.drawCircle(1, 2, 3, 4);
        dp.drawLine(1, 2, 3, 4);
        dp.drawRectangle(1, 2, 3, 4);
    }

    public void drawCircle(int x, int y, int width, int height) {
        Shape shape = new CircularShape();
        shape.draw(x, y, width, height);
    }

    public void drawRectangle(int x, int y, int width, int height) {
        Shape shape = new RectangluarShape();
        shape.draw(x, y, width, height);
    }

    public void drawLine(int x, int y, int width, int height) {
        Shape shape = new LineAdapter();
        shape.draw(x, y, width, height);
    }
}
