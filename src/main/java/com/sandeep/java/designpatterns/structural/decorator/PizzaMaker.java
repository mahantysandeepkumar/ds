package com.sandeep.java.designpatterns.structural.decorator;

public class PizzaMaker {
    public static void main(String[] args) {
        Pizza pizza = new PanPizza();
        // Regular Pizza
        System.out.println("order: " + pizza.getDescription() + " total bill : " + pizza.getPrice());
        // Add just corn
        Pizza cornToppingPizza = new CornTopping(new PanPizza());
        System.out.println("order: " + cornToppingPizza.getDescription() + " total bill : " + cornToppingPizza.getPrice());

        // Add just cheese
        Pizza cheeseToppingPizza = new ExtraCheeseTopping(new PanPizza());
        System.out.println("order: " + cheeseToppingPizza.getDescription() + " total bill : " + cheeseToppingPizza.getPrice());

        // Add everything
        pizza = new CornTopping(pizza);
        pizza = new ExtraCheeseTopping(pizza);
        System.out.println("order: " + pizza.getDescription() + " total bill : " + pizza.getPrice());
    }
}
