package com.sandeep.java.designpatterns.structural.adapter;

/**
 * Created by smahanty on 6/10/17.
 */
public class Line {
    public void drawLine(int startX, int startY, int endX, int endY) {
        System.out.println("Draw line from from (" + startX + "," + startY + ") to  ( " + endX + "," + endY + ")");
    }
}
