package com.sandeep.java.designpatterns.structural.adapter;

/**
 * Created by smahanty on 6/10/17.
 */
public class RectangluarShape implements Shape {
    @Override
    public void draw(int x, int y, int width, int height) {
        System.out.println("Draw rectangle from " + x + "," + y + " with width " + width + " and height " + height);
    }
}
