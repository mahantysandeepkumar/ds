package com.sandeep.java.designpatterns.structural.decorator;

public class ExtraCheeseTopping implements ToppingsDecorator {
    private Pizza pizza;

    @Override
    public double getPrice() {
        return pizza.getPrice() + 0.35;
    }

    @Override
    public String getDescription() {
        return this.pizza.getDescription() + ", extra cheese topping";
    }

    public ExtraCheeseTopping(Pizza pizza) {
        this.pizza = pizza;
    }
}
