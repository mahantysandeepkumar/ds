package com.sandeep.java.designpatterns.structural.decorator;

public interface Pizza {
    double getPrice();
    String getDescription();
}
