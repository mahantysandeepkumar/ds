package com.sandeep.java.designpatterns.structural.proxy;

import java.io.IOException;

/**
 * Created by smahanty on 5/30/17.
 */
public class CommandExecutorImpl implements CommandExecutor {

    @Override
    public boolean execute(String command) {
        try {
            Runtime.getRuntime().exec(command);
            return true;
        } catch (IOException e) {
            System.out.println("Error : " + e.getMessage());
        }
        return false;
    }
}
