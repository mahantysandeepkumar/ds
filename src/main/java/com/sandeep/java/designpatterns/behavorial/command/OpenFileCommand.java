package com.sandeep.java.designpatterns.behavorial.command;


/**
 * Created by smahanty on 6/10/17.
 */
public class OpenFileCommand implements Command {
    FileSystem fileSystem;

    public void setFileSystem(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public void execute() {
        fileSystem.openFile();
    }

    public OpenFileCommand(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    public OpenFileCommand() {

    }
}
