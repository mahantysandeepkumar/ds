package com.sandeep.java.designpatterns.behavorial.command;

/**
 * Created by smahanty on 6/10/17.
 */
public class UnixFileSystem implements FileSystem {
    @Override
    public void openFile() {
        System.out.println("Opening file in unix");
    }

    @Override
    public void closeFile() {
        System.out.println("Closing file in unix");
    }

    @Override
    public void writeFile() {
        System.out.println("Writing to a file in unix");
    }
}
