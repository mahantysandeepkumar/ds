package com.sandeep.java.designpatterns.behavorial.command;

import java.io.File;

/**
 * Created by smahanty on 6/10/17.
 */
public class CommandPatternClient {
    private FileManager fileManager;

    public static void main(String[] args) {
        CommandPatternClient obj = new CommandPatternClient();

        obj.fileManager = new FileManager();
        obj.executeCommandOnMac();

    }

    public void executeCommandOnMac() {
        Command openFileCommand = new OpenFileCommand(new MacFileSystem());
        Command closeFileCommand = new CloseFileCommand(new MacFileSystem());
        Command writeFileCommand = new WriteFileCommand(new MacFileSystem());

        fileManager.setCommand(openFileCommand);
        fileManager.execute();
        fileManager.setCommand(writeFileCommand);
        fileManager.execute();
        fileManager.setCommand(closeFileCommand);
        fileManager.execute();
    }

    public void executeCommandOnUnix(Command command) {
        FileSystem unixFileSystem = new UnixFileSystem();
        command.setFileSystem(unixFileSystem);
        command.execute();
    }

    public void executeCommandOnWindows(Command command) {
        FileSystem windowsFileSystem = new WindowsFileSystem();
        command.setFileSystem(windowsFileSystem);
        command.execute();
    }
}
