package com.sandeep.java.designpatterns.behavorial.command;

import java.io.File;

/**
 * Created by smahanty on 6/10/17.
 */
public class WriteFileCommand implements Command {
    FileSystem fileSystem;

    public void setFileSystem(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public void execute() {
        fileSystem.writeFile();
    }

    public WriteFileCommand(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    public WriteFileCommand() {

    }
}
