package com.sandeep.java.designpatterns.behavorial.command;

/**
 * Created by smahanty on 6/10/17.
 */
public class FileManager {
    Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    public void execute() {
        this.command.execute();
    }

    public FileManager(Command command) {
        this.command = command;
    }

    public FileManager() {

    }
}
