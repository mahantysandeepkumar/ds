package com.sandeep.java.designpatterns.behavorial.command;

/**
 * Created by smahanty on 6/10/17.
 */
public interface FileSystem {
    void openFile();

    void closeFile();

    void writeFile();
}
