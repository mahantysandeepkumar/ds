package com.sandeep.utils;

import com.sandeep.ml.DataPoint;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by smahanty on 5/23/17.
 */
public class FileUtil {

    public static Map<List<Double>, String> getFeaturesFromFile() {
        Map<List<Double>, String> featuesMap = new HashMap<>();
        File file = new File("testData/trainingDigits");
        if (file.exists() && file.isDirectory()) {
            File[] files = file.listFiles();

            for (int i = 0; i < files.length; i++) {
                //System.out.println("FileName: " + files[i].getName());
                String label = String.valueOf(files[i].getName().charAt(0));
                List<Double> features = new ArrayList<>();

                try {
                    FileReader fileReader = new FileReader(files[i]);
                    int c = fileReader.read();
                    while (c != -1) {
                        if (c > 47) {
                            features.add((double) (c - 48));
                        }
                        c = fileReader.read();
                    }

                    fileReader.close();
                } catch (Exception ex) {
                    //System.out.println("Failed due to: " + ex.getMessage());
                }
                //System.out.println("Features count: " + features.size());
                featuesMap.put(features, label);
            }
        }
        //System.out.println("MapSize: " + featuesMap.size());
        return featuesMap;
    }

    public static List<DataPoint> getFeaturesListFromFile() {
        List<DataPoint> featuresList = new ArrayList<>();
        File file = new File("testData/trainingDigits");
        if (file.exists() && file.isDirectory()) {
            File[] files = file.listFiles();

            for (int i = 0; i < files.length; i++) {
                //System.out.println("FileName: " + files[i].getName());
                String label = String.valueOf(files[i].getName().charAt(0));
                double[] features = new double[1024];
                DataPoint dataPoint = new DataPoint();
                dataPoint.setLabel(label);
                int count = 0;
                try {
                    FileReader fileReader = new FileReader(files[i]);
                    int c = fileReader.read();
                    while (c != -1) {
                        if (c > 47) {
                            features[count++] = ((double) (c - 48));
                        }
                        c = fileReader.read();
                    }

                    fileReader.close();
                } catch (Exception ex) {
                    //System.out.println("Failed due to: " + ex.getMessage());
                }
                dataPoint.setFeature(features);
                //System.out.println("Features count: " + features.size());
                featuresList.add(dataPoint);
            }
        }
        //System.out.println("MapSize: " + featuesMap.size());
        return featuresList;
    }

    public static List<Double> getFeaturesFromFileToTest(String fileName) {
        List<Double> features = new ArrayList<>();

        File file = new File("testData/trainingDigits/" + fileName);
        if (file.exists()) {
            try {
                FileReader fileReader = new FileReader(file);
                int c = fileReader.read();
                while (c != -1) {
                    if (c > 47) {
                        features.add((double) (c - 48));
                    }
                    c = fileReader.read();
                }

                fileReader.close();
            } catch (Exception ex) {
                // System.out.println("Failed due to: " + ex.getMessage());
            }
            //System.out.println("Features count: " + features.size());
        }
        return features;
    }

    public static DataPoint getFeatureFromFileToTest(String fileName) {
        DataPoint dataPoint = new DataPoint();
        double[] features = new double[1024];
        int count = 0;
        File file = new File("testData/trainingDigits/" + fileName);
        if (file.exists()) {
            try {
                FileReader fileReader = new FileReader(file);
                int c = fileReader.read();
                while (c != -1) {
                    if (c > 47) {
                        features[count++] = ((double) (c - 48));
                    }
                    c = fileReader.read();
                }
                dataPoint.setFeature(features);
                fileReader.close();
            } catch (Exception ex) {
                // System.out.println("Failed due to: " + ex.getMessage());
            }
            //System.out.println("Features count: " + features.size());
        }

        return dataPoint;
    }

}
