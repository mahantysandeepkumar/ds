package com.sandeep.leetcode;

/* package codechef; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class CodeChef {
    public static void main(String[] args) throws Exception {
        InputReader br = new InputReader(System.in);
        int t = br.nextInt();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < t; i++) {
            String s = br.next();
            findPermutations(s.toCharArray());
        }
        System.out.println("Time: " + (System.currentTimeMillis() - startTime) + " ms");
    }

    static void findPermutations(char[] s) {
        if (s.length == 1) {
            System.out.println(1);
            return;
        }

        if (s.length == 2) {
            if (s[0] == s[1]) {
                System.out.println("1 2");
            } else {
                System.out.println(-1);
            }
            return;
        }

        /*if (checkPalindrome(s)) {
            printArray(s.length);
            return;
        }*/

        HashMap<Character, List<Integer>> indexMap = new HashMap<>();

        for (int i = 0; i < s.length; i++) {
            if (!indexMap.containsKey(s[i])) {
                indexMap.put(s[i], new ArrayList<>());
            }
            indexMap.get(s[i]).add(i);
        }
        //System.out.println(indexMap);

        int count = 0;
        char midPoint = s[0];
        for (Map.Entry<Character, List<Integer>> entry : indexMap.entrySet()) {
            if (entry.getValue().size() % 2 != 0) {
                count++;
                midPoint = entry.getKey();
                if (count > 1) {
                    System.out.println(-1);
                    return;
                }
            }
        }

        StringBuilder result = new StringBuilder("");
        int[] resultArray = new int[s.length];
        int leftPos = s.length / 2;

        if (midPoint == s[0]) {
            leftPos -= (indexMap.get(midPoint).size() - 1);
        } else {
            leftPos -= indexMap.get(midPoint).size() / 2;
        }
        int rightPos = leftPos + indexMap.get(midPoint).size();
        System.arraycopy(indexMap.get(midPoint), 0, resultArray, leftPos, indexMap.get(midPoint).size());

     /*   for (int a : indexMap.get(midPoint)) {
            result.append((a + 1) + " ");
        }*/

        indexMap.remove(midPoint);

        for (Map.Entry<Character, List<Integer>> entry : indexMap.entrySet()) {
            List<Integer> indices = entry.getValue();
            int i = 0, j = indices.size() - 1;
            leftPos -= indices.size() / 2;
            System.arraycopy(indices, 0, resultArray, leftPos, indices.size() / 2);

            System.arraycopy(indices, indices.size() / 2 + 1, resultArray, rightPos, indices.size() / 2);
            rightPos += indices.size()/2;

            /*while (i < j) {
                result.insert(0, (indices.get(i) + 1) + " ");
                result.append(1 + indices.get(j) + " ");
                i++;
                j--;
            }*/

        }
        for(int i = 0; i < resultArray.length; i++) {
            System.out.print(resultArray[i] +" ");
        }
        System.out.println();
        //System.out.println(result);
    }

    static boolean checkPalindrome(char[] s) {
        boolean isPalindrome = true;
        for (int i = 0, j = s.length - 1; i < j; i++, j--) {
            if (s[i] != s[j]) {
                isPalindrome = false;
                break;
            }
        }
        return isPalindrome;
    }

    static void printArray(int length) {
        for (int i = 0; i < length; i++) {
            System.out.print((i + 1) + " ");
        }
        System.out.println();
    }

    static class InputReader {

        private InputStream stream;
        private byte[] buf = new byte[1024];
        private int curChar;
        private int numChars;

        public InputReader(InputStream stream) {
            this.stream = stream;
        }

        public int read() {
            if (numChars == -1)
                throw new InputMismatchException();
            if (curChar >= numChars) {
                curChar = 0;
                try {
                    numChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (numChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }

        public int nextInt() {
            int c = read();
            while (isSpaceChar(c))
                c = read();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = read();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c & 15;
                c = read();
            } while (!isSpaceChar(c));
            return res * sgn;
        }

        public String next() {
            int c = read();
            while (isSpaceChar(c))
                c = read();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = read();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public static boolean isSpaceChar(int c) {
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }

    }
}
