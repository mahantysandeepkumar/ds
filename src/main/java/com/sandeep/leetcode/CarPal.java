package com.sandeep.leetcode;


import java.util.Scanner;

public class CarPal {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);

        int t = sc.nextInt();

        for (int i = 0; i < t; i++) {
            int tunnels = sc.nextInt();

            long[] times = new long[tunnels];

            for (int j = 0; j < tunnels; j++) {
                times[j] = sc.nextLong();
            }

            int cars = sc.nextInt();
            int distance = sc.nextInt();
            int speed = sc.nextInt();
            double time = distance / speed;

            double[][] timesMatrix = new double[cars][tunnels];
            timesMatrix[0][0] = times[0];

            for (int j = 1; j < tunnels; j++) {
                timesMatrix[0][j] = timesMatrix[0][j - 1] + time + times[j];
            }
            //System.out.println();
            double delay = 0;
            timesMatrix[1][0] = timesMatrix[0][0] + times[0];
            for (int k = 1; k < tunnels; k++) {
                double timeToReach = timesMatrix[1][k - 1] + time;

                if (timeToReach < timesMatrix[0][k]) {
                    delay += timesMatrix[0][k] - timeToReach;
                }
                timesMatrix[1][k] += times[k] + timeToReach;
            }
            //print(timesMatrix);
            System.out.println((delay + times[0]) * (cars - 1));
        }

    }

    static void print(double[][] arr) {
        for (int i = 0; i < arr[0].length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}
