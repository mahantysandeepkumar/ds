package com.sandeep.leetcode;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Palin {
    public static void main(String[] args) throws Exception {
        CodeChef.InputReader br = new CodeChef.InputReader(System.in);
        int t = Integer.parseInt(br.next());
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < t; i++) {
            String s = br.next();
            findPermutations(s.toCharArray());
        }
        System.out.println("Time: " + (System.currentTimeMillis() - startTime) + " ms");
    }


    static void findPermutations(char[] s) {
        if (s.length == 1) {
            System.out.println(1);
            return;
        }

        if (s.length == 2) {
            if (s[0] == s[1]) {
                System.out.println("1 2");
            } else {
                System.out.println(-1);
            }
            return;
        }

        int[] indices = new int[s.length];

        for (int i = 0; i < indices.length; i++) {
            indices[i] = i;
        }
        int mid = 0;
        if (s.length % 2 != 0) {
            int[] count = new int[26];
            for (int u = 0; u < s.length; u++) {
                count[s[u] - 'a']++;
            }

            int odd = 0;
            char midC = '0';
            for (int u = 0; u < count.length; u++) {
                if (count[u] % 2 != 0 && count[u] != 0) {
                    odd++;
                    midC = (char) ('a' + u);
                }
                if (odd > 1) {
                    System.out.println(-1);
                    return;
                }
            }
            for (int u = 0; u < s.length; u++) {
                if (s[u] == midC) {
                    mid = u;
                }
            }
            swap(s, indices, mid, s.length / 2);
        }

        int i = s.length / 2 - 1;
        int j = s.length / 2 + 1;

        while (i >= 0 && j < s.length) {
            if (s[i] != s[j]) {
                int index = findIndex(s, i, j, s[i]);
                if (index != -1) {
                    swap(s, indices, index, j);
                } else {
                    System.out.println("-1");
                    return;
                }
            }
            i--;
            j++;
        }
        printArray(indices);
        //System.out.println(result);
    }

    static int findIndex(char[] s, int low, int high, char c) {
        for (int i = high + 1, j = low - 1; j >= 0 && i < s.length; i++, j--) {
            if (s[i] == c) {
                return i;
            }
            if (s[j] == c) {
                return j;
            }
        }
        return -1;
    }

    static void swap(char[] s, int[] indices, int a, int b) {
        int tempIndex = indices[a];
        indices[a] = indices[b];
        indices[b] = tempIndex;
        char tempChar = s[a];
        s[a] = s[b];
        s[b] = tempChar;

    }

    static boolean checkPalindrome(char[] s) {
        boolean isPalindrome = true;
        for (int i = 0, j = s.length - 1; i < j; i++, j--) {
            if (s[i] != s[j]) {
                isPalindrome = false;
                break;
            }
        }
        return isPalindrome;
    }

    static void printArray(int[] indices) {
        for (int i = 0; i < indices.length; i++) {
            System.out.print((indices[i] + 1) + " ");
        }
        System.out.println();
    }
}
