package com.sandeep.ml.supervised;

import com.sandeep.ml.DataPoint;

import java.util.*;

/**
 * Created by smahanty on 5/12/17.
 */
public class Classification {

    //[2,0],"A" --> DataPoint

    public String getClassificationWithKNearest(List<DataPoint> dataPoints, int k, DataPoint newDataPoint) {
        TreeMap<Double, String> distanceList = getEucledianDistanceMap(dataPoints, newDataPoint);
        System.out.println("Eucledian distance: " + distanceList);
        return findMajorityElement(distanceList, k);
    }

    public TreeMap<Double, String> getEucledianDistanceMap(List<DataPoint> dataPoints, DataPoint newDataPoint) {
        TreeMap<Double, String> distanceList = new TreeMap<>();
        for (DataPoint dataPoint : dataPoints) {
            double[] diffFeature = dataPoint.getFeature();
            double eucledianDistance = 0;
            for (int i = 0; i < diffFeature.length; i++) {
                diffFeature[i] = Math.pow((diffFeature[i] - newDataPoint.getFeature()[i]), 2);
                eucledianDistance += diffFeature[i];
            }
            distanceList.put(eucledianDistance, dataPoint.getLabel());
        }
        return distanceList;
    }

    public String findMajorityElement(TreeMap<Double, String> distanceList, int k) {
        int[] count = new int[26];
        int counter = 0;
        if (k > distanceList.size()) {
            k = distanceList.size();
        }

        Set<Map.Entry<Double, String>> entrySet = distanceList.entrySet();
        Iterator itr = entrySet.iterator();
        while (counter < k) {
            Map.Entry<Double, String> entry = (Map.Entry<Double, String>) itr.next();
            count[entry.getValue().charAt(0) - 48]++;
            counter++;
        }
        int max = 0;
        for (int i = 0; i < count.length; i++) {
            if (count[i] > count[max]) {
                max = i;
            }
        }
        return String.valueOf((char) (max + 48));
    }
}
