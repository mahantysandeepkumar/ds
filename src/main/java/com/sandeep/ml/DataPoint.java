package com.sandeep.ml;

import java.util.Arrays;

/**
 * Created by smahanty on 5/12/17.
 */
public class DataPoint {
    private String label;
    private double[] feature;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double[] getFeature() {
        return feature;
    }

    public void setFeature(double[] feature) {
        this.feature = feature;
    }

    public DataPoint() {

    }

    @Override
    public String toString() {
        return "DataPoint{" +
                "label='" + label + '\'' +
                ", feature=" + Arrays.toString(feature) +
                '}';
    }

    public DataPoint(double[] feature, String label) {
        this.feature = feature;
        this.label = label;
    }
}
