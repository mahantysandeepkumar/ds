package com.sandeep.ds.dynamicprogramming;

public class MaximumSumContiguousSubArray {
    /** -2, 1, -3, 4, -1, 2, 1, -5, 4 **/
    public int maxSubArray(int[] nums) {
        int maxSum = nums[0];
        int soFar = nums[0];

        for (int i = 1; i < nums.length; i++) {
            soFar = Math.max(nums[i], soFar + nums[i]);
            maxSum = Math.max(soFar, maxSum);
        }
        return maxSum;
    }
}
