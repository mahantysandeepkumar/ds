package com.sandeep.ds.dynamicprogramming;

public class SubsetsWithSpecificSum {

    public static void main(String[] args) {
        SubsetsWithSpecificSum obj = new SubsetsWithSpecificSum();
        System.out.println(obj.getSubsetCount(new int[]{8, 3, 1, 2}, 3));
    }

    public int getSubsetCount(int[] arr, int sum) {
        int[][] memo = new int[sum + 1][arr.length + 1];
        return getSubset(arr, 0, sum, memo);
    }

    public int getSubset(int[] arr, int i, int sum, int[][] memo) {
        // any empty subset can be counted as result
        int subsetCount = 0;
        if (memo[sum][i] != 0) {
            return memo[sum][i];
        }
        if (sum == 0) {
            return 1;
        }
        if (sum < 0) {
            return 0;
        }
        if (i >= arr.length) {
            return 0;
        }
        if (sum < arr[i]) {
            subsetCount = getSubset(arr, i + 1, sum, memo);
        } else {
            subsetCount = getSubset(arr, i + 1, sum, memo) + getSubset(arr, i + 1, sum - arr[i], memo);
        }
        memo[sum][i] = subsetCount;
        return subsetCount;
    }
}
