package com.sandeep.ds.dynamicprogramming;

import java.util.Arrays;

public class HouseRobber {
    //TODO: consider the possiblity that the robber doesnot goes for all the alternate houses if any two
    //TODO: houses more than 1 house apart give more money ([2,1,1,2]) answer should be 4 and not 3

    public int rob(int[] cost) {

        if (cost.length == 0) {
            return 0;
        }
        if (cost.length == 1) {
            return cost[0];
        }
        if (cost.length == 2) {
            return cost[0] > cost[1] ? cost[0] : cost[1];
        }

        // (f[i]) = c[i] + (f[i + 2],f[i+3].....)
        int[] maxCost = new int[cost.length];
        maxCost[cost.length - 1] = cost[cost.length - 1];

        for (int i = cost.length - 2; i >= 0; i--) {
            maxCost[i] = cost[i] ;

            if (i + 2 < cost.length){
                maxCost[i] += maxCost[i + 2];
            }
        }

        int max = -1;

        for (int i = 0; i < cost.length; i++) {
            if (maxCost[i] > max) {
                max = maxCost[i];
            }
        }

        System.out.println(Arrays.toString(maxCost));
        return max;
    }
}
