package com.sandeep.ds.dynamicprogramming;

import java.util.Arrays;

public class ArithematicSlices {

    public static void main(String[] args) {
        int[] numbers = new int[]{1, 3, 5, 7, 9};
        int total = 0;
        int[] memo = new int[numbers.length];

       /* if (numbers.length < 3){
            return 0;
        } */
        // for length 3

        for (int i = 2; i < numbers.length; i++) {
            int prevDiff = (numbers[i - 1] - numbers[i - 2]);
            int diff = (numbers[i] - numbers[i - 1]);
            if (prevDiff == diff) {
                memo[i] = 1 + memo[i - 1];
                total+= memo[i];
            }else {
                memo[i] = 1;
            }
        }
        System.out.println(Arrays.toString(memo));
        System.out.println(total);
    }

}
