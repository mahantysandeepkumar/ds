package com.sandeep.ds.dynamicprogramming;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

public class CoinChange {
    int getChangeCount(int n, int[] coins) {
        int[] dp = new int[n + 1];
        dp[0] = 1;
        for (int coin = 0; coin < coins.length; coin++) {
            for (int sum = 1; sum <= n; sum++) {
                if (sum - coins[coin] >= 0) {
                    dp[sum] += dp[sum - coins[coin]];
                }
            }
            System.out.println(Arrays.toString(dp));
        }
        return dp[n];
    }

    @Test
    public void testOne() {
        int[] coins = {1, 2, 3};
        int sum = 10;
        int expected = 14;
        int actual = getChangeCount(sum, coins);
        Assert.assertEquals(actual, expected);
    }
}
