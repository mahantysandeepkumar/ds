package com.sandeep.ds.dynamicprogramming;

/**
 * Created by smahanty on 3/15/17.
 */
public class UglyNumbers {
    /**
     * Ugly numbers are numbers whose only prime factors are 2, 3 or 5. The sequence 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, … shows the first 11 ugly numbers. By convention, 1 is included.
     * Given a number n, the task is to find n’th Ugly number.
     */

    public int getUglyNumber(int n) {
        int[] ugly = new int[n];
        int twoCount = 0;
        int threeCount = 0;
        int fiveCount = 0;
        int nextUgly = 1;
        int nextUglyTwo = 2;
        int nextUglyThree = 3;
        int nextUglyFive = 5;

        ugly[0] = nextUgly;
        for (int i = 1; i < n; i++) {
            nextUgly = Math.min(Math.min(nextUglyTwo, nextUglyThree), nextUglyFive);
            ugly[i] = nextUgly;

            if (nextUgly == nextUglyTwo) {
                twoCount++;
                nextUglyTwo = ugly[twoCount] * 2;
            }
            if (nextUgly == nextUglyThree) {
                threeCount++;
                nextUglyThree = ugly[threeCount] * 3;
            }
            if (nextUgly == nextUglyFive) {
                fiveCount++;
                nextUglyFive = ugly[fiveCount] * 5;
            }
        }

        System.out.print(nextUgly + ", ");
        return nextUgly;
    }

    public static void main(String[] args) {
        UglyNumbers obj = new UglyNumbers();
        obj.getUglyNumber(1);
        obj.getUglyNumber(2);
        obj.getUglyNumber(3);
        obj.getUglyNumber(4);
        obj.getUglyNumber(5);
        obj.getUglyNumber(6);
        obj.getUglyNumber(7);
        obj.getUglyNumber(8);
        obj.getUglyNumber(9);
        obj.getUglyNumber(10);
        obj.getUglyNumber(11);
    }
}
