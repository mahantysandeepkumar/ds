package com.sandeep.ds.dynamicprogramming;

import java.util.Arrays;

public class RangeSumQuery {
    private int[] nums;

    public RangeSumQuery(int[] nums) {
        this.nums = nums;
        constructSumArray();
    }

    public void constructSumArray() {
        for (int i = 1; i < nums.length; i++) {
            nums[i] += nums[i - 1];
        }
        //System.out.println(Arrays.toString(nums));
    }

    /**
     * -2, 0, 3, -5, 2, -1
     */
    public int sumRange(int i, int j) {
        if (i == 0) {
            return nums[j];
        }
        return nums[j] - nums[i - 1];
    }
}
