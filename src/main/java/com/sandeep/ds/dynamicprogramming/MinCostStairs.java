package com.sandeep.ds.dynamicprogramming;

import java.util.Arrays;

public class MinCostStairs {

    public int minCostClimbingStairs(int[] cost) {
        int[] minCost = new int[cost.length];
        minCost[cost.length - 1] = cost[cost.length - 1];
        minCost[cost.length - 2] = cost[cost.length - 2];

        for (int i = cost.length - 3; i >= 0; i--) {
            minCost[i] = cost[i] + Math.min(minCost[i + 1], minCost[i + 2]);
        }

        //System.out.println(Arrays.toString(minCost));
        return Math.min(minCost[0], minCost[1]);
    }
}
