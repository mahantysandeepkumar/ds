package com.sandeep.ds.dynamicprogramming;

import java.util.Arrays;
import java.util.HashMap;

public class IntegerBreak {
    HashMap<String, Integer> value;

    public static void main(String[] args) {
        IntegerBreak br = new IntegerBreak();
        System.out.println(br.integerBreak(4));
    }

    public int integerBreak(int n) {
        if (n < 2) {
            return 0;
        }

        if (n == 2) {
            return 1;
        }
        int[][] memo = new int[n][n];
        // 1,1,1 ; 1,2
        // f(4) = f(2) * f(4-2)
        memo[1][2] = 1;
        int product = 1;
        for (int i = 1; i < n; i++) {
            int num = i;
            for (int j = 1; j < i; j++) {
                if (num - j >= 0) {
                    memo[i][j] = Math.max(memo[j][num - j], memo[j + 1][num - j + 1]);
                }
                num -= j;
            }
        }
        /// Example for 2
        /// 0,1,2,3,4,5,6,7,8,9
        /// 10,11,12,13,14,15,16,17,18,19 -1
        /// 20,21,22,23,24,25,26,27,28,29 -1
        /// 90,91,92,93,94,95,96,97,98,99 -1 (-1 * 9)
        /// 10 * 9 - 9 + 10 , 10 *10 - 9
        /// 90 + 1 --> 91
        /// Lets start with 3
        /// 0,1,2,3,4,5,6,7,8,9,...99 --> as it is
        /// 100,101,102,103,104,105,106,107,108,109 -1 * 2
        /// 110,111,112,113,114,115,116,117,118,119 -1 * 10 * 9
        /// 120,121,122,123,124,125,126,127,128,129 -1 * 2
        /// 130,131,132,133,134,135,136,137,138,139 -1 * 2
        /// 210,211,212,213,214,215,216,217,218,219 -1 * 2
        /// 990,991,992,993,994,995,996,997,998,999 -1 * 10 * 9
        /// 91 * 9 - (10 * 9 + 9 * 9 ) + (91) , 91 * 10 - ()
        /// 739 * 9 - (100 * 9 + 9 *  9 * 9) + 739
        for (int i = 0; i < n; i++) {
            System.out.println(Arrays.toString(memo[i]));
        }
        return product;
    }
}
