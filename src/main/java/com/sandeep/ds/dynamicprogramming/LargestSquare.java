package com.sandeep.ds.dynamicprogramming;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

public class LargestSquare {

    public int getSquare(int[][] matrix) {
        int largest = 0;
        int rows = matrix.length;
        int cols = matrix[0].length;

        int[][] memo = new int[rows][cols];

        for (int i = rows - 1; i >= 0; i--) {
            for (int j = cols - 1; j >= 0; j--) {
                if (matrix[i][j] == 0) {
                    memo[i][j] = 0;
                } else if (i < rows - 1 && j < cols - 1) {
                    int right = memo[i][j + 1];
                    int bottom = memo[i + 1][j];
                    int bottomRight = memo[i + 1][j + 1];
                    int min = Math.min(bottomRight, Math.min(right, bottom));
                    memo[i][j] = 1 + min;
                } else {
                    memo[i][j] = matrix[i][j];
                }
            }
        }
        //printMatrix(memo);
        int count = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (memo[i][j] > largest) {
                    largest = memo[i][j];
                }
                count += memo[i][j];
            }
        }
        System.out.println("Num squares: "+count);
        return largest;
    }

    private void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }

    @Test
    public void allZerosTest() {
        int[][] matrix = {
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0},
        };

        int expected = 0;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void squareMatrixTest() {
        int[][] matrix = {
                {1, 0, 1},
                {1, 1, 1},
                {1, 1, 1},
        };

        int expected = 2;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void rectangleMatrixTest() {
        int[][] matrix = {
                {1, 0, 1, 1},
                {1, 1, 1, 1},
                {1, 1, 1, 1},
                {1, 1, 1, 1},
                {1, 1, 1, 1},
        };

        int expected = 4;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void rectangleIslandMatrixTest() {
        int[][] matrix = {
                {0, 0, 0, 0},
                {0, 1, 1, 0},
                {0, 1, 1, 0},
                {0, 1, 1, 0},
                {0, 0, 0, 0},
        };

        int expected = 2;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void rectangleSparseMatrixTest() {
        int[][] matrix = {
                {0, 0, 0, 0},
                {1, 1, 1, 0},
                {0, 1, 1, 1},
                {0, 1, 1, 1},
                {0, 0, 0, 0},
        };

        int expected = 2;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void singleZeroElementMatrixTest() {
        int[][] matrix = {
                {0}
        };

        int expected = 0;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void singleOneElementMatrixTest() {
        int[][] matrix = {
                {1}
        };

        int expected = 1;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void noElementMatrixTest() {
        int[][] matrix = {
                {}
        };

        int expected = 0;
        int actual = getSquare(matrix);
        Assert.assertEquals(expected, actual);
    }
}
