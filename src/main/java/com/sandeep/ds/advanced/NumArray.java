package com.sandeep.ds.advanced;

import java.util.Arrays;

class NumArray {

    private int[] tree;
    private int[] elements;

    private void buildTree(int[] arr, int current, int left, int right) {
        if (left == right) {
            tree[current] = arr[left];
        } else {
            int mid = (left + right) / 2;
            buildTree(arr, 2 * current, left, mid);
            buildTree(arr, 2 * current + 1, mid + 1, right);
            tree[current] = tree[2 * current] + tree[2 * current + 1];
        }
    }

    private int rangeQuery(int current, int left, int right, int start, int end) {
        if (left > end || right < start) {
            return 0;
        }
        // current range is within whatever is being asked
        if (left >= start && right <= end) {
            return tree[current];
        }
        int mid = (left + right) / 2;
        return rangeQuery(2 * current, left, mid, start, end) + rangeQuery(2 * current + 1, mid + 1, right, start, end);
    }

    private void updateQuery(int current, int left, int right, int index, int value) {
        if (left == right) {
            elements[index - 1] = value;
            tree[current] = value;
        } else {
            int mid = (left + right) / 2;
            if (index >= left && index <= mid) {
                updateQuery(2 * current, left, mid, index, value);
            } else {
                updateQuery(2 * current + 1, mid + 1, right, index, value);
            }
            tree[current] = tree[2 * current] + tree[2 * current + 1];
        }
    }

    public NumArray(int[] nums) {
        this.elements = nums;
        int height = (int) Math.ceil(Math.log(elements.length) / Math.log(2));
        this.tree = new int[2 * (int) Math.pow(2, height)];
        if (elements.length > 0) {
            buildTree(nums, 1, 0, elements.length - 1);
        }
    }

    public void update(int i, int val) {
        if (elements.length < 1) {
            return;
        }
        updateQuery(1, 1, elements.length, i + 1, val);
    }

    public int sumRange(int i, int j) {
        if (elements.length < 1) {
            return 0;
        }
        return rangeQuery(1, 1, elements.length, i + 1, j + 1);
    }

    public static void main(String[] args) {
        NumArray obj = new NumArray(new int[]{-1});
        System.out.println(Arrays.toString(obj.tree));
        System.out.println(obj.sumRange(0, 0));
        obj.update(0, 1);
        System.out.println(Arrays.toString(obj.tree));
        System.out.println(obj.sumRange(0, 0));
    }
}