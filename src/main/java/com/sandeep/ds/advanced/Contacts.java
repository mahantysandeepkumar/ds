package com.sandeep.ds.advanced;

import lombok.Data;

public class Contacts {
    private Node root;

    public void insert(String s) {
        if (root == null) {
            root = new Node();
        }

        Node temp = root;

        for (int i = 0; i < s.length(); i++) {
            int ch = s.charAt(i) - 'a';
            if (temp.nodes[ch] == null) {
                temp.nodes[ch] = new Node();
            }
            temp.nodes[ch].children++;
            temp = temp.nodes[ch];
        }
        temp.children --;
        temp.isLeaf = true;
    }

    public int find(String prefix) {
        Node temp = root;

        for (int i = 0; i < prefix.length() && temp != null; i++) {
            int ch = prefix.charAt(i) - 'a';
            temp = temp.nodes[ch];
        }

        if (temp == null) {
            return 0;
        }
        if (temp.isLeaf) {
            return temp.children + 1;
        } else {
            return temp.children;
        }
    }

    public void delete(String s) {
        if (root == null) {
            root = new Node();
        }

        Node temp = root;

        for (int i = 0; i < s.length(); i++) {
            int ch = s.charAt(i) - 'a';
            if (temp.nodes[ch] == null) {
                temp.nodes[ch] = new Node();
            }

            temp = temp.nodes[ch];
        }
        temp.isLeaf = true;
    }

    public boolean isPresent(String searchString) {
        Node temp = root;

        for (int i = 0; i < searchString.length(); i++) {
            int ch = searchString.charAt(i) - 'a';
            if (temp.nodes[ch] == null) {
                return false;
            }
            temp = temp.nodes[ch];
        }

        return temp != null && temp.isLeaf;
    }

    @Data
    class Node {
        private Node[] nodes = new Node[26];
        private boolean isLeaf;
        private int children;
    }
}
