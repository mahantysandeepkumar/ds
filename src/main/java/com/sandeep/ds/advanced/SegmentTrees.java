package com.sandeep.ds.advanced;

import java.io.BufferedReader;
import java.io.StringReader;

import java.util.Arrays;


public class SegmentTrees {
    /**
     * Problem https://www.hackerearth.com/practice/data-structures/advanced-data-structures/segment-trees/tutorial/
     */
    static int[] segmentTree;

    public String query(String input) throws Exception {
        String result = "";
        BufferedReader br = new BufferedReader(new StringReader(input));

        String[] t = br.readLine().split(" ");
        int n = Integer.parseInt(t[0]);
        int q = Integer.parseInt(t[1]);
        String[] elements = br.readLine().split(" ");

        segmentTree = new int[2 * n];

        buildTree(elements, 1, 0, elements.length - 1);
        //System.out.println(Arrays.toString(segmentTree));

        for (int i = 0; i < q; i++) {
            String[] tokens = br.readLine().split(" ");
            if (tokens[0].equals("u")) {
                int index = Integer.parseInt(tokens[1]);
                int value = Integer.parseInt(tokens[2]);
                update(tokens, 1, 1, n - 1, index - 1, value);
                //System.out.println(Arrays.toString(segmentTree));
            } else {
                int start = Integer.parseInt(tokens[1]);
                int end = Integer.parseInt(tokens[2]);
                result += query(1, 0, elements.length - 1, start -1, end-1) + " ";
            }
        }
        return result;
    }

    private static void buildTree(String[] tokens, int node, int start, int end) {
        if (start == end && start < tokens.length) {
            segmentTree[node] = Integer.parseInt(tokens[start]);
        } else {
            int mid = (start + end) / 2;
            // build left
            buildTree(tokens, 2 * node, start, mid);
            // build right
            buildTree(tokens, 2 * node + 1, mid + 1, end);
            // add left and right child to parent
            if (2 * node + 1 < segmentTree.length) {
                segmentTree[node] = Math.min(segmentTree[2 * node], segmentTree[2 * node + 1]);
            } else {
                return;
            }
        }
    }

    private static void update(String[] tokens, int node, int start, int end, int idx, int val) {
        if (start == end) {
            tokens[idx] = String.valueOf(val);
            segmentTree[node] = val;
        } else {
            int mid = (start + end) / 2;
            if (start <= idx && idx <= mid) {
                update(tokens, 2 * node, start, mid, idx, val);
            } else {
                update(tokens, 2 * node + 1, mid + 1, end, idx, val);
            }
            segmentTree[node] = Math.min(segmentTree[2 * node], segmentTree[2 * node + 1]);
        }
    }

    private static int query(int node, int start, int end, int l, int r) {
        // Outside range
        if (l > end || r < start) {
            return Integer.MAX_VALUE;
        }
        // inside range
        if (l <= start && end <= r) {
            return segmentTree[node];
        }
        // partial inside range
        int mid = (start + end) / 2;
        int p1 = query(2 * node, start, mid, l, r);
        int p2 = query(2 * node + 1, mid + 1, end, l, r);
        return Math.min(p1, p2);
    }
}
