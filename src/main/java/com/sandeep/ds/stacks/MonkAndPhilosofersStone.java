package com.sandeep.ds.stacks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by smahanty on 2/4/17.
 */
public class MonkAndPhilosofersStone {

    public static void main(String[] args) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        long startTime = System.currentTimeMillis();
        String[] harry = br.readLine().split(" ");
        Stack<Integer> monk = new Stack<>();

        String[] tokens = br.readLine().split(" ");

        int q = Integer.parseInt(tokens[0]);
        int x = Integer.parseInt(tokens[1]);

        int sum = 0;
        int cnt = 0;
        int size = 0;

        for (int i = 0; i < q; i++) {
            String op = br.readLine();
            int num = 0;
            if (op.equals("Harry")) {
                num = Integer.parseInt(harry[cnt++]);
                sum += num;
                monk.push(num);
                size++;
            } else {
                num = monk.pop();
                sum -= num;
                size--;
            }
            if (sum == x) {
                break;
            }
        }

        int result = (sum == x) ? size : -1;
        System.out.println(result);
        long endTime = System.currentTimeMillis();
        System.out.println("Time: " + (endTime - startTime));
    }

    static int parseInt(String input) {
        int base = 1;
        int num = 0;

        for (int i = input.length() - 1; i >= 0; i--) {
            num += (input.charAt(i) - '0') * base;
            base *= 10;
        }
        return num;
    }
}
