package com.sandeep.ds.stacks;

import java.util.Stack;

public class BasicCalculator {
    public static void main(String[] args) {
        BasicCalculator obj = new BasicCalculator();
        System.out.println("(1+(4+5+2)-3)+(6+8): " + obj.calculate("(1+(4+5+2)-3)+(6+8)"));
        System.out.println("2147483647: " + obj.calculate("2147483647"));
        System.out.println(" 2-1 + 2 : " + obj.calculate(" 2-1 + 2 "));
        System.out.println("1+5-4 : " + obj.calculate("1+5-4"));
        System.out.println("(1) : "+obj.calculate("(1)"));
    }

    public int calculate(String s) {
        Stack<Integer> tokens = new Stack<>();
        Stack<String> operations = new Stack<>();

        for (int ch = s.length() - 1; ch >= 0; ch--) {
            String c = String.valueOf(s.charAt(ch));
            if (c.equals(")") || c.equals("+") || c.equals("-")) {
                operations.push(c);
            } else if (c.equals("(")) {
                while (!operations.isEmpty() && !operations.peek().equals(")")) {
                    tokens.push(performOp(operations.pop(), tokens.pop(), tokens.pop()));
                }
                if (!operations.isEmpty()) {
                    operations.pop();
                }
            } else if (!c.equals(" ")) {
                String str = "";
                while (ch >= 0 &&( s.charAt(ch) >= '0' && s.charAt(ch) <= '9')) {
                    str = s.charAt(ch--) + str;
                }
                ch++;
                tokens.push(Integer.parseInt(str));
            }
        }
        System.out.println("Ops: " + operations);
        System.out.println("Tokens: " + tokens);

        while (!operations.isEmpty()) {
            tokens.push(performOp(operations.pop(), tokens.pop(), tokens.pop()));
        }
        return tokens.peek();
    }

    public int performOp(String op, int op1, int op2) {
        switch (op) {
            case "+":
                return op1 + op2;
            default:
                return op1 - op2;
        }
    }
}
