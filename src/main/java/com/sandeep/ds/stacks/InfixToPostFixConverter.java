package com.sandeep.ds.stacks;

import lombok.Data;
import org.testng.Assert;

import java.beans.Expression;
import java.util.HashMap;
import java.util.Stack;

public class InfixToPostFixConverter {
    public static void main(String[] args) {
        System.out.println(getInFix("ab*c+"));
        System.out.println(getPostFix("a+b*(c^d-e)^(f+g*h)-i"));
        System.out.println(getInFix(getPostFix("a+b*(c^d-e)^(f+g*h)-i")));
        Assert.assertEquals(getInFix(getPostFix("a+b*(c^d-e)^(f+g*h)-i")), "a+b*(c^d-e)^(f+g*h)-i");
    }


    public static String getPostFix(String infix) {
        if (infix == null || infix.length() < 1) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        Stack<Character> operandsStack = new Stack<>();

        for (int i = 0; i < infix.length(); i++) {
            char c = infix.charAt(i);
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
            } else if (c == '(') {
                operandsStack.push(c);
            } else if (c == ')') {
                while (!operandsStack.isEmpty() && operandsStack.peek() != '(') {
                    sb.append(operandsStack.pop());
                }
                if (!operandsStack.isEmpty()) {
                    operandsStack.pop();
                }
            } else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '^') {
                while (!operandsStack.isEmpty() && getPrecedence(c) <= getPrecedence(operandsStack.peek())) {
                    sb.append(operandsStack.pop());
                }
                operandsStack.push(c);
            }
        }

        while (!operandsStack.isEmpty()) {
            sb.append(operandsStack.pop());
        }
        return sb.toString();
    }

    //abcd^e-fgh*+^*+i-
    //((a+(b*(((c^d)-e)^(f+(g*h)))))-i)
    public static String getInFix(String postFix) {
        if (postFix == null || postFix.length() < 1) {
            return null;
        }

        Stack<Exp> operandsStack = new Stack<>();

        for (int i = 0; i < postFix.length(); i++) {
            char c = postFix.charAt(i);

            if (Character.isLetterOrDigit(c)) {
                operandsStack.push(new Exp(String.valueOf(c), ""));
            } else if (c == '+' || c == '-') {
                String first = operandsStack.pop().expression;
                String second = operandsStack.pop().expression;
                operandsStack.push(new Exp(second + c + first, String.valueOf(c)));
            } else if (c == '*' || c == '/' || c == '^') {
                String first = getCorrectedExpression(operandsStack.pop());
                String second = getCorrectedExpression(operandsStack.pop());
                operandsStack.push(new Exp(second + c + first, String.valueOf(c)));
            }
            //System.out.println("Stack: " + operandsStack);
        }
        return operandsStack.peek().expression;
    }

    static String getCorrectedExpression(Exp s) {
        String result = s.expression;
        if (s.op.equalsIgnoreCase("+") || s.op.equalsIgnoreCase("-")) {
            result = "(" + result + ")";
        }
        //System.out.println("op: " + result + " for string: " + s);
        return result;

    }

    static int getPrecedence(char ch) {
        switch (ch) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            case '^':
                return 3;
            default:
                return -1;
        }
    }

    @Data
    static class Exp {
        String expression;
        String op;

        public Exp(String expression, String op) {
            this.expression = expression;
            this.op = op;
        }
    }
}
