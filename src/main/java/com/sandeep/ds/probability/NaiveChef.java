package com.sandeep.ds.probability;

import java.util.HashMap;
import java.util.Scanner;

public class NaiveChef {
    /**
     * Problem Statement at : (remove JUNE18B)
     * https://www.codechef.com/JUNE18B/problems/NAICHEF
     */

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCases = sc.nextInt();

        for (int testCase = 0; testCase < testCases; testCase++) {
            int numElements = sc.nextInt();
            int firstNum = sc.nextInt();
            int secondNum = sc.nextInt();

            HashMap<Integer, Integer> probabilityMap = new HashMap<>();

            int[] elements = new int[numElements];

            for (int i = 0; i < numElements; i++) {
                elements[i] = sc.nextInt();
                updateProbabilityMap(probabilityMap, elements[i]);
            }
            //System.out.println(probabilityMap);

            double probabilityOfFirstNum = probabilityMap.containsKey(firstNum) ? (double) probabilityMap.get(firstNum)/numElements: 0;
            double probabilityOfSecondNum = probabilityMap.containsKey(secondNum) ? (double) probabilityMap.get(secondNum)/numElements: 0;

            System.out.println(probabilityOfFirstNum * probabilityOfSecondNum);
        }
    }

    private static void updateProbabilityMap(HashMap<Integer, Integer> map, int num) {
        if (map.containsKey(num)) {
            map.put(num, map.get(num) + 1);
        } else {
            map.put(num, 1);
        }
    }
}
