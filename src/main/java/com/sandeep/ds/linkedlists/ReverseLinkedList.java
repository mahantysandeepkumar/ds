package com.sandeep.ds.linkedlists;

/**
 * Created by smahanty on 9/22/16.
 */
public class ReverseLinkedList {

    public Node reverse(Node head) {

        if (head == null || head.getNext() == null) {
            return head;
        }

        Node prev = null;
        Node curr = head;
        Node next = curr.getNext();

        while (curr != null) {
            curr.setNext(prev);
            prev = curr;
            curr = next;
            if (next != null)
                next = next.getNext();
        }

        return prev;
    }
}
