package com.sandeep.ds.linkedlists;

/**
 * Created by smahanty on 11/10/16.
 */
public class MergeSortedLists {

    public static void main(String[] args) {
        Node<Integer> node = new Node<>(1);
        Node<Integer> node2 = new Node<>(2);
        Node<Integer> node3 = new Node<>(4);
        Node<Integer> node4 = new Node<>(6);
        Node<Integer> node5 = new Node<>(2);
        Node<Integer> node6 = new Node<>(3);

        node5.setNext(node6);
        node3.setNext(node4);
        node2.setNext(node3);
        node.setNext(node2);
        System.out.println(getMergedList(node, node5));
    }

    public static Node getMergedList(Node<Integer> list1, Node<Integer> list2) {
        Node finalList;
        if (list1 == null) {
            return list2;
        }

        if (list2 == null) {
            return list1;
        }

        if (list1.getData() < list2.getData()) {
            finalList = list1;
            finalList.setNext(getMergedList(list1.getNext(), list2));
        } else {
            finalList = list2;
            finalList.setNext(getMergedList(list1, list2.getNext()));
        }

        return finalList;
    }
}
