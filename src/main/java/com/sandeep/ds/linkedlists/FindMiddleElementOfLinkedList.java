package com.sandeep.ds.linkedlists;

/**
 * Created by smahanty on 11/15/16.
 */
public class FindMiddleElementOfLinkedList {

    public int findMiddleElement(SingleLinkedList list) {
        if (list.size() == 0) {
            return -1;
        }

        Node<Integer> root = list.getRoot();
        Node slow = root;
        Node fast = root.getNext();

        while (fast != null && fast.getNext() != null) {
            slow = slow.getNext();
            fast = slow.getNext().getNext();
        }

        return (Integer) (slow.getData());
    }
}
