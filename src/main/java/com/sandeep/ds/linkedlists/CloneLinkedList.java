package com.sandeep.ds.linkedlists;

/**
 * Created by mahantys on 08/07/17.
 */
class LNode {
    private int data;
    private LNode next;
    private LNode random;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public LNode getNext() {
        return next;
    }

    public void setNext(LNode next) {
        this.next = next;
    }

    public LNode getRandom() {
        return random;
    }

    public void setRandom(LNode random) {
        this.random = random;
    }

    public LNode() {

    }

    public LNode(int data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LNode{" +
                "data=" + data +
                ", next=" + next +
                ", random=" + random +
                '}';
    }
}

public class CloneLinkedList {

    public LNode clone(LNode root) {
        LNode node = traverse(root);
        return node;
    }

    private LNode traverse(LNode root) {
        if (root == null) {
            return null;
        }
        LNode node = new LNode(root.getData());
        LNode next = traverse(root.getNext());
        node.setNext(next);
        node.setRandom(root.getRandom());
        return node;
    }

    public static void main(String[] args) {
        CloneLinkedList obj = new CloneLinkedList();
        LNode root = new LNode(1);
        LNode root1 = new LNode(2);
        LNode root2 = new LNode(3);
        LNode root3 = new LNode(4);
        root.setNext(root1);
        root.setRandom(root2);
        root1.setNext(root2);
        root1.setRandom(root3);
        root2.setNext(root3);
        root2.setRandom(root1);
        root3.setRandom(root2);
        System.out.println("Original: ");
        print(root);
        LNode n = obj.clone(root);
        System.out.println("\nCloned: ");
        print(n);
        System.out.println(" Original == Cloned : " + (n == root));
    }

    private static void print(LNode root) {
        LNode temp = root;

        while (temp != null) {
            System.out.print(temp.getData() + " - " + temp.getRandom().getData() + ", ");
            temp = temp.getNext();
        }
    }

}
