package com.sandeep.ds.linkedlists;

import java.util.LinkedList;

public class RemoveLastNthNode {
    static Node<Integer> result = new Node<>();

    public static void main(String[] args) {
        SingleLinkedList<Integer> list = new SingleLinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);


        find(list.getRoot(), result, 2);
        System.out.println(result);
    }

    public static int find(Node<Integer> node, Node<Integer> retNode, int dest) {
        if (node != null && node.getNext() == null) {
            return 1;
        }
        int pos = find(node.getNext(), retNode, dest);
        if (pos + 1 == dest) {
            result = node;
        }
        return pos + 1;
    }

}
