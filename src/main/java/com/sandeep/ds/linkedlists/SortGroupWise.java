package com.sandeep.ds.linkedlists;

public class SortGroupWise {

    public static void main(String[] args) {
        Node root = new Node(4);
        Node first = new Node(1);
        Node seco = new Node(8);
        Node th = new Node(2);
        Node four = new Node(1);
        Node fiv = new Node(6);
        first.setNext(seco);
        seco.setNext(th);
        th.setNext(four);
        four.setNext(fiv);
        root.setNext(first);
        System.out.print("Original: ");
        print(root);
        Node sorted = sort(3, root);
        System.out.print("Sorted: ");
        print(sorted);
    }

    public static void print(Node root) {
        Node temp = root;
        while (temp != null) {
            System.out.print(temp.getData());
            if (temp.getNext() != null) {
                System.out.print(" -> ");
            }
            temp = temp.getNext();
        }
        System.out.println();
    }

    public static Node sort(int k, Node<Integer> root) {

        return null;
    }
}
