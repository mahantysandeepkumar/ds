package com.sandeep.ds.linkedlists;


import java.util.Stack;

/**
 * Created by smahanty on 9/20/16.
 */


public class RemoveDuplicateFromSortedList {

    static Node removeDuplicates(Node head) {
        //11->11->11->21->43->43->60
        if (head == null) {
            return null;
        }

        Node temp = head.getNext();
        Node prev = head;

        while (temp != null) {
            if (prev.getData() == temp.getData()) {
                prev.setNext(temp.getNext());
            } else {
                prev = temp;
            }
            temp = temp.getNext();
        }

        return head;
    }

    public static void main(String[] args) {
        Node root = new Node(11);
        Node one = new Node(11);
        Node two = new Node(11);
        Node three = new Node(21);
        Node four = new Node(43);
        Node five = new Node(43);
        Node six = new Node(60);
        root.setNext(one);
        one.setNext(two);
        two.setNext(three);
        three.setNext(four);
        four.setNext(five);
        five.setNext(six);
        printNodes(root);
        Node altered = removeDuplicates(root);
        printNodes(altered);
    }

    static void printNodes(Node root) {
        Node temp = root;
        Stack<Integer> s1 = new Stack<Integer>();

        while (temp != null) {
            System.out.print(temp.getData() + " -> ");
            temp = temp.getNext();
        }
        System.out.println("");
    }
    /// 1 2 3 4 5
    /// 5 4 3 2 1
    ///
    /// 5 4 3 2
}
