package com.sandeep.ds.linkedlists;

import lombok.Data;

/**
 * Created by smahanty on 11/10/16.
 */
@Data
class Node<E> {
    private E data;
    private Node next;

    public Node(E d) {
        data = d;
        next = null;
    }

    public Node() {

    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
