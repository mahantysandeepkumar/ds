package com.sandeep.ds.linkedlists;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class MergeSortLikedList {

    private Node root;

    @Data
    static class Node {
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }

        public Node(int data, Node next) {
            this(data);
            this.next = next;
        }
    }

    public Node sort(Node root) {
        return mergeSort(root);
    }

    public Node mergeSort(Node root) {
        if (root == null || root.next == null) {
            return root;
        }
        Node middleNode = getMiddleNode(root);
        //System.out.println(middleNode.data);

        Node nextToMiddleNode = middleNode.next;

        middleNode.next = null;

        Node left = mergeSort(root);
        Node right = mergeSort(nextToMiddleNode);
        Node merge = merge(left, right);

        return merge;
    }

    public Node merge(Node root1, Node root2) {
        Node dummy;
        if (root1 == null) {
            return root2;
        }
        if (root2 == null) {
            return root1;
        }
        if (root1.data >= root2.data) {
            dummy = root2;
            dummy.next = merge(root1, root2.next);
        } else {
            dummy = root1;
            dummy.next = merge(root1.next, root2);
        }
        return dummy;
    }

    public Node getMiddleNode(Node root) {
        if (root == null || root.next == null) {
            return root;
        }

        Node fast = root.next;
        Node slow = root;

        while (fast != null) {
            fast = fast.next;
            if (fast != null) {
                slow = slow.next;
                fast = fast.next;
            }
        }
        return slow;
    }

    public static void main(String[] args) {
        Node node = new Node(6);
        Node node2 = new Node(4, node);
        Node node3 = new Node(8, node2);
        Node node4 = new Node(2, node3);
        Node node5 = new Node(7, node4);
        MergeSortLikedList obj = new MergeSortLikedList();
        System.out.println(obj.sort(node5));
    }
}
