package com.sandeep.ds.linkedlists;

import java.util.*;

/**
 * Created by smahanty on 11/10/16.
 */

/**
 * Simple implementation of a Singly LinkedList with an iterator.
 * This list will follow the order the elements are inserted and is not synchronized.
 * For now modCount is not being taken into consideration hence it is not a fail fast iterator.
 */
public class SingleLinkedList<E> implements List<E> {
    private Node<E> root;
    private int size;
    private Node lastNode;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new Itr();
    }

    public Node<E> getRoot() {
        return root;
    }

    public void setRoot(Node<E> root) {
        this.root = root;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        Node<E> node = new Node<>(e);
        if (root == null) {
            root = node;
        }
        if (lastNode != null) {
            lastNode.setNext(node);
        }
        lastNode = node;
        this.size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) {
        return null;
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Node temp = root;
        while (temp != null) {
            if (temp.getNext() != null) {
                sb.append(temp.getData() + ",");
            } else {
                sb.append(temp.getData());
            }
            temp = temp.getNext();
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SingleLinkedList) {
            if (((SingleLinkedList) obj).size() == this.size()) {
                return true;
            }
        }
        return false;
    }

    private class Itr implements Iterator<E> {
        private Node<E> cursor = root;
        private Node<E> lastRet = null;

        @Override
        public boolean hasNext() {
            return cursor != null;
        }

        @Override
        public E next() {
            // TODO: need to write an iterator for this
            Node<E> temp = cursor;
            if (temp == null) {
                throw new NoSuchElementException();
            }
            cursor = temp.getNext();
            lastRet = temp;
            return (E) temp.getData();
        }

        @Override
        public void remove() {

        }
    }
}
