package com.sandeep.ds.graph;

import java.util.*;

/**
 * Created by smahanty on 3/16/17.
 */
public class DetectingCycles {
    public boolean hasCycle(Graph graph) {
        List<List<Integer>> adjacencyList = graph.getAdjacencyList();
        BitSet visited = new BitSet(graph.getAdjacencyList().size());

        for (int i = 0; i < adjacencyList.size(); i++) {
            if (!visited.get(i)) {
                if (DFS(graph, i, visited, -1)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Whenever we encounter a visited node we check whether it is a parent or not.
     * Since in an undirected graph we can visit parent back from child. So if the current
     * node is visited and not the parent then the graph must have a cycle.
     *
     * @param graph the graph
     * @param start current node
     * @param visited BitSet holding all the visited nodes
     * @param parent the parent node
     * @return true if the graph has a cycle
     */
    public boolean DFS(Graph graph, int start, BitSet visited, int parent) {

        List<Integer> list = graph.getAdjacencyList().get(start);
        Iterator<Integer> itr = list.iterator();
        visited.set(start, true);
        while (itr.hasNext()) {
            int item = itr.next();

            if (!visited.get(item)) {
                if (DFS(graph, item, visited, start)) {
                    return true;
                }
            } else if (item != parent) {
                return true;
            }
        }
        return false;
    }
}
