package com.sandeep.ds.graph;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class WeightedGraph {

    @Getter
    private List<List<Node>> adjacencyList;
    private int SIZE;
    private BitSet visited;
    @Getter
    private Type graphType;
    @Getter
    private int vertices;

    public void addEdge(int start, int end, int weight) {
        List<Node> sList = adjacencyList.get(start);
        sList.add(new Node(end, weight));

        if (graphType == Type.UNDIRECTED) {
            List<Node> eList = adjacencyList.get(end);
            eList.add(new Node(start, weight));
        }
    }

    public WeightedGraph(int size, Type type) {
        this.adjacencyList = new ArrayList<>();
        this.SIZE = size;
        this.graphType = type;
        this.vertices = size;
        for (int i = 0; i < SIZE; i++) {
            List<Node> alist = new ArrayList<>();
            adjacencyList.add(alist);
        }
    }


    @Getter
    @Setter
    @AllArgsConstructor
    class Node {
        private int name;
        private int weight;
    }

    public enum Type {
        DIRECTED, UNDIRECTED
    }
}
