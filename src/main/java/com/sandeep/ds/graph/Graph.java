package com.sandeep.ds.graph;

import lombok.Getter;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by smahanty on 3/16/17.
 */
public class Graph {

    @Getter
    private List<List<Integer>> adjacencyList;
    private int SIZE;
    private BitSet visited;
    @Getter
    private Type graphType;
    @Getter
    private int vertices;

    public void addEdge(int start, int end) {
        List<Integer> sList = adjacencyList.get(start);
        sList.add(end);

        if (graphType == Type.UNDIRECTED) {
            List<Integer> eList = adjacencyList.get(end);
            eList.add(start);
        }
    }

    public void printList() {

        for (int row = 0; row < adjacencyList.size(); row++) {
            System.out.print(row + " :-> ");

            for (Integer col : adjacencyList.get(row)) {
                System.out.print(col + " -> ");
            }
            System.out.println();
        }
    }

    public void printBFS(int start) {
        String bfsString = BFS(start);
        System.out.println(bfsString);
    }
    public void printDFS(int start) {
        String dfsString = DFS(start);
        System.out.println(dfsString);
    }

    public String BFS(int start) {
        StringBuilder stringBuilder = new StringBuilder();
        LinkedList<Integer> list = new LinkedList<>();
        list.add(start);

        while (!list.isEmpty()) {
            int element = list.removeFirst();
            if (!visited.get(element)) {
                stringBuilder.append(element + ",");
                list.addAll(adjacencyList.get(element));
                visited.set(element, true);
            }
        }
        return stringBuilder.toString();
    }

    public String DFS(int start) {
        StringBuilder stringBuilder = new StringBuilder();
        LinkedList<Integer> list = new LinkedList<>();
        list.addFirst(start);

        while (!list.isEmpty()) {
            int element = list.removeFirst();
            if (!visited.get(element)) {
                stringBuilder.append(element + ",");
                list.addAll(0, adjacencyList.get(element));
                visited.set(element, true);
            }
        }
        return stringBuilder.toString();
    }

    public Graph(int size, Type type) {
        this.SIZE = size;
        this.vertices = size;
        this.visited = new BitSet(SIZE);
        this.adjacencyList = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            List<Integer> alist = new ArrayList<>();
            adjacencyList.add(alist);
        }
        this.graphType = type;
    }

    public static void main(String[] args) {
        Graph graph = new Graph(6, Type.UNDIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(0, 4);
        graph.addEdge(0, 5);
        graph.addEdge(1, 2);
        graph.addEdge(1, 4);
        graph.addEdge(2, 3);
        graph.addEdge(3, 4);
        graph.printList();
        graph.printDFS(0);
    }

    public enum Type {
        DIRECTED, UNDIRECTED
    }
}
