package com.sandeep.ds.graph;

import java.util.ArrayList;
import java.util.Scanner;

public class RoadsAndLibraries {
    /* * Problem Statement can be found at : https://www.hackerrank.com/challenges/torque-and-development/problem
     */

    static int[][] graph;
    static boolean[] visited;
    static int connected = 0;
    static int total = 0;


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int numCases = in.nextInt();

        for (int caseCount = 0; caseCount < numCases; caseCount++) {
            connected = 0;

            int numCity = in.nextInt();
            int numRoad = in.nextInt();
            long costLibrary = in.nextLong();
            long costRoad = in.nextLong();


            visited = new boolean[numCity];
            graph = new int[numCity][numCity];

            for (int road = 0; road < numRoad; road++) {
                int city_1 = in.nextInt() - 1;
                int city_2 = in.nextInt() - 1;
                graph[city_1][city_2] = 1;
                graph[city_2][city_1] = 1;
            }

            if (costLibrary < costRoad) {
                System.out.println(numCity * costLibrary);
            } else {
                dfsUtil();
                //System.out.println("total: " + total);
                System.out.println(costLibrary + (costRoad * connected) + (numCity - connected - 1) * costLibrary);
            }
        }
    }

    static void dfsUtil() {
        for (int node = 0; node < graph.length; node++) {
            if (!visited[node]) {
                visited[node] = true;
                dfs(node);
                //System.out.println("Connected: " + connected);
                //total += connected;
                //connected -=1;
            }
        }
    }

    static void dfs(int node) {
        int[] adjacency = graph[node];
        for (int city = 0; city < adjacency.length; city++) {
            if (!visited[city] && adjacency[city] != 0) {
                visited[city] = true;
                connected++;
                //System.out.println("Node: "+node+" next node: "+city);
                dfs(city);
            }
        }
    }
}
