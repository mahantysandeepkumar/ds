package com.sandeep.ds.graph;

import java.util.*;

public class FindArticulatePoints {
    private Graph graph;
    private BitSet visited;
    private int time = 0;

    public int[] findArticulatePoints(Graph graph) {
        this.graph = graph;
        this.visited = new BitSet(graph.getVertices());

        int[] points = new int[graph.getVertices()];
        int[] discoveryTime = new int[graph.getVertices()];
        int[] low = new int[graph.getVertices()];
        int[] parent = new int[graph.getVertices()];

        Arrays.fill(parent, -1);

        for (int i = 0; i < graph.getAdjacencyList().size(); i++) {
            if (!visited.get(i)) {
                findAP(i, discoveryTime, low, parent, points);
            }
        }
        return points;
    }

    private void findAP(int vertex, int[] discoveryTime, int[] low, int[] parent, int[] pointsList) {
        visited.set(vertex);

        // count children (for the case of root vertex)
        int child = 0;

        // Set the current discovery time
        discoveryTime[vertex] = low[vertex] = ++time;

        Iterator<Integer> iterator = graph.getAdjacencyList().get(vertex).iterator();

        while (iterator.hasNext()) {
            int v = iterator.next();
            /*
                If it is the first time visiting the node then add it as a child to current vertex
                and then recurse for its dfs tree.
             */
            if (!visited.get(v)) {
                child++;
                parent[v] = vertex;

                // recurse to calculate low and disc times for all connected vertices
                findAP(v, discoveryTime, low, parent, pointsList);

                // for finding if any back edge exists. this time would be less than discovery time
                low[vertex] = Math.min(low[vertex], low[v]);

                // If it is a root and has more than 1 child then it is an articulation point
                if (parent[vertex] == -1 && child > 1) {
                    pointsList[vertex] = 1;
                }

                /* if it is not root and with lowest earliest time for any child in the subtree rooted at v
                    if the time of discovery is less than the earliest time of tree rooted at v then no back edge exists
                    and hence it is an articulation point. Visualize the dfs tree of the graph and the time of
                    discovery is increasing as we go down from ay parent to child of that tree.
                 */
                if (parent[vertex] != -1 && low[vertex] >= discoveryTime[vertex]) {
                    pointsList[vertex] = 1;
                }

            }
            // Ignoring the path we took to reach here (v == parent[vertex])
            else if (v != parent[vertex]) {
                low[vertex] = Math.min(low[vertex], low[v]);
            }
        }

    }

}
