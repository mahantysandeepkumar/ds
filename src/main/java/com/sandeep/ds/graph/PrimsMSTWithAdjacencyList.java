package com.sandeep.ds.graph;

import java.util.Iterator;

public class PrimsMSTWithAdjacencyList {

    public void findMST(WeightedGraph graph) {
        //to keep  track of minimum distance to any of the discovered vertices
        int[] key = new int[graph.getVertices()];
        // This will hold the mst vertices (Edge (index - > value at the index))
        int[] parent = new int[graph.getVertices()];
        // The vertices which are not yet part of the mst
        boolean[] mstSet = new boolean[graph.getVertices()];

        init(key, mstSet, parent);

        for (int i = 0; i < graph.getVertices() - 1; i++) {
            // find the min distance vertex
            int u = getMinDistanceVertex(key, mstSet);
            // mark it as discovered
            mstSet[u] = true;

            // update the distance table
            Iterator<WeightedGraph.Node> iterator = graph.getAdjacencyList().get(u).iterator();

            while (iterator.hasNext()) {
                // if not visited yet and the distance is greater than
                WeightedGraph.Node node = iterator.next();
                if (!mstSet[node.getName()] && node.getWeight() < key[node.getName()]) {
                    parent[node.getName()] = u;
                    key[node.getName()] = node.getWeight();
                }
            }
        }
        printMST(parent, graph);
    }

    public void init(int[] key, boolean[] mstSet, int[] parent) {
        for (int i = 0; i < key.length; i++) {
            key[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
        }
        key[0] = 0;
        parent[0] = -1;
    }

    void printMST(int[] parent, WeightedGraph graph) {
        System.out.println("Edge   Weight");
        for (int i = 1; i < parent.length; i++) {
            System.out.println(parent[i] + " - " + i + "  " + graph.getAdjacencyList().get(i).get(parent[i]).getWeight());
        }
    }

    public static void main(String[] args) {
        WeightedGraph graph = new WeightedGraph(5, WeightedGraph.Type.UNDIRECTED);
        graph.addEdge(0,1,2);
        graph.addEdge(0,3,6);
        graph.addEdge(1,2,3);
        graph.addEdge(2,4,7);
        graph.addEdge(1,4,5);
        graph.addEdge(3,4,9);
        graph.addEdge(1,3,8);
        PrimsMSTWithAdjacencyList obj = new PrimsMSTWithAdjacencyList();
        obj.findMST(graph);
    }

    public int getMinDistanceVertex(int[] key, boolean[] mstSet) {
        int minValue = Integer.MAX_VALUE, minIndex = -1;
        // If not discovered yet, find the min distance
        for (int i = 0; i < key.length; i++) {
            if (!mstSet[i] && key[i] < minValue) {
                minValue = key[i];
                minIndex = i;
            }
        }
        return minIndex;
    }
}
