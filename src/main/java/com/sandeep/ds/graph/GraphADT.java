package com.sandeep.ds.graph;

import java.util.List;

public interface GraphADT {
    int numVertices();

    List<Integer> vertices();

    int numEdges();
}
