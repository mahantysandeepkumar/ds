package com.sandeep.ds.graph;

import java.util.*;

/**
 * Created by smahanty on 3/19/17.
 */
public class TopologicalSort {

    public String topologicalSort(Graph graph) {
        StringBuilder str = new StringBuilder();
        List<List<Integer>> adjacencyList = graph.getAdjacencyList();
        BitSet visited = new BitSet(graph.getAdjacencyList().size());
        Deque<Integer> stack = new LinkedList<>();

        for (int i = 0; i < adjacencyList.size(); i++) {
            if (!visited.get(i)) {
                DFS(graph, i, visited, stack);
            }
        }

        for (Integer item : stack) {
            str.append(item + " ");
        }
        return str.toString();
    }

    public void DFS(Graph graph, int start, BitSet visited, Deque<Integer> stack) {

        List<Integer> list = graph.getAdjacencyList().get(start);
        Iterator<Integer> itr = list.iterator();
        visited.set(start, true);
        while (itr.hasNext()) {
            int item = itr.next();

            if (!visited.get(item)) {
                DFS(graph, item, visited, stack);
            }
        }
        stack.push(start);
    }
}
