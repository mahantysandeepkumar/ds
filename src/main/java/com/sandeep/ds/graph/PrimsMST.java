package com.sandeep.ds.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by mahantys on 08/07/17.
 */
public class PrimsMST {

    private int getMin(int[] keys, boolean[] mstSet) {
        int minIndex = -1;
        int minValue = Integer.MAX_VALUE;

        for (int i = 0; i < keys.length; i++) {
            if (mstSet[i] == false && keys[i] < minValue) {
                minValue = keys[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

    public void printMST(int[][] graph) {
        boolean[] mstSet = new boolean[graph.length];
        int[] parent = new int[graph.length];
        int[] keys = new int[graph.length];

        for (int i = 0; i < keys.length; i++) {
            keys[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
        }

        keys[0] = 0;
        parent[0] = -1;

        for (int count = 0; count < parent.length - 1; count++) {
            int index = getMin(keys, mstSet);
            mstSet[index] = true;
            int[] adjacency = graph[index];
            for (int i = 0; i < adjacency.length; i++) {
                if (adjacency[i] > 0 && !mstSet[i] && adjacency[i] < keys[i]) {
                    parent[i] = index;
                    keys[i] = adjacency[i];
                }
            }
        }
        printMST(parent, graph);
    }

    void printMST(int[] parent, int[][] graph) {
        System.out.println("Edge   Weight");
        for (int i = 1; i < parent.length; i++) {
            System.out.println(parent[i] + " - " + i + "  " + graph[i][parent[i]]);
        }
    }

    public static void main(String[] args) {
        PrimsMST mst = new PrimsMST();

        mst.printMST(new int[][]{{0, 2, 0, 6, 0},
                {2, 0, 3, 8, 5},
                {0, 3, 0, 0, 7},
                {6, 8, 0, 0, 9},
                {0, 5, 7, 9, 0},
        });

    }
}
