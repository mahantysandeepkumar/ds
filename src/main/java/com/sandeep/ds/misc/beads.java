package com.sandeep.ds.misc;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by smahanty on 11/3/16.
 */
public class beads {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader("beads.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("beads.out")));
        int n = Integer.parseInt(br.readLine());
        String beadsString = br.readLine();
        int result = findMaxBeads(beadsString);
        out.println(result);
        out.println();
        out.close();
    }
//wwwbbrwrbrbrrbrbrwrwwrbwrwrrb
    public static int findMaxBeads(String input) {
        int max = Integer.MIN_VALUE;
        ArrayList<Integer> counts = new ArrayList<>();
        for (int i = 0; i < input.length(); i++) {
            int count = 0;
            int firstCount = 0;
            int secondCount = 0;

            char token = input.charAt(i);
            while(input.charAt(i)==token) {
                i++;
                firstCount++;
            }

            char tokenSecond = input.charAt(i);
            while (input.charAt(i)== token || input.charAt(i) =='w') {
                i++;
                secondCount++;
            }
        }
        return max;
    }
}
