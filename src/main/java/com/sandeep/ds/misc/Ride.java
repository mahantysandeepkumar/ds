package com.sandeep.ds.misc;

import java.io.*;

/**
 * Created by smahanty on 9/26/16.
 */
public class Ride {
    private static final int MOD = 47;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("ride.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ride.out")));

        String comet = br.readLine();
        String group = br.readLine();

        long cometSum = 1;
        for (int i = 0; i < comet.length(); i++) {
            cometSum *= comet.charAt(i) - 'A' + 1;
        }
        long groupSum = 1;
        for (int i = 0; i < group.length(); i++) {
            groupSum *= group.charAt(i) - 'A' + 1;
        }

        if (cometSum % MOD == groupSum % MOD) {
            out.println("GO");
        } else {
            out.println("STAY");
        }

        out.close();

    }
}
