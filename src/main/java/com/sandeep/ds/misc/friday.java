package com.sandeep.ds.misc;

import java.io.*;

/**
 * Created by smahanty on 9/26/16.
 */
public class friday {

    private static int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    private static int isLeapYear(int year) {
        if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
            return 1;
        }
        return 0;
    }

    private static int length(int month, int year) {
        if (month == 1) {
            return months[month] + isLeapYear(year);
        } else {
            return months[month];
        }
    }

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader("friday.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("friday.out")));

        int NP = Integer.parseInt(br.readLine());
        int[] daysCount = new int[7];

        int startOfMonthDay = 0;

        for (int year = 1900; year < 1900 + NP; year++) {
            for (int month = 0; month < 12; month++) {
                daysCount[startOfMonthDay]++;
                startOfMonthDay = ((startOfMonthDay + length(month, year)) % 7);
            }
        }

        for (int i = 0; i < 7; i++) {
            if (i == 6) {
                out.print(daysCount[i]);
            } else {
                out.print(daysCount[i] + " ");
            }
        }
        out.println();
        out.close();
    }
}
