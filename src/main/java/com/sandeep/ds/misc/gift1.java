package com.sandeep.ds.misc;

/*
ID: sandeep57
LANG: JAVA
TASK: gift1
*/

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by smahanty on 9/26/16.
 */
public class gift1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("gift1.in"));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("gift1.out")));

        int NP = Integer.parseInt(br.readLine());
        HashMap<String, Long> friendList = new LinkedHashMap<String, Long>();

        for (int i = 0; i < NP; i++) {
            friendList.put(br.readLine(), 0l);
        }

        for (int i = 0; i < NP; i++) {
            String sender = br.readLine();
            String[] tokens = br.readLine().split(" ");

            long amount = Long.parseLong(tokens[0]);
            int friendCount = Integer.parseInt(tokens[1]);
            long amountPerFriend = 0;

            if (amount != 0) {
                amountPerFriend = (amount - (amount % friendCount)) / friendCount;
            }

            friendList.put(sender, friendList.get(sender) - amount + (amount == 0 ? 0 : amount % friendCount));

            for (int j = 0; j < friendCount; j++) {
                String friend = br.readLine();
                friendList.put(friend, friendList.get(friend) + amountPerFriend);
            }
        }

        Iterator itr = friendList.entrySet().iterator();

        while (itr.hasNext())

        {
            Map.Entry<String, Long> entry = (Map.Entry<String, Long>) itr.next();
            out.println(entry.getKey() + " " + entry.getValue());
        }
        out.close();
    }
}
