package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 8/23/16.
 */
public class ExtremeAlternateNodes {

    public void printNodes(Node root) {
        Node left = root.getLeft();
        Node right = root.getRight();
        System.out.print(root.getData() + ",");
        // Depth
        int count = 1;

        // Traverse till at least one of left or right sub trees is completely traversed
        while (left != null && right != null) {
            if (count % 2 == 0) {
                System.out.print(right.getData() + ",");
            } else {
                System.out.print(left.getData() + ",");
            }
            right = right.getRight();
            left = left.getLeft();
            count++;
        }

        // If right sub tree is not completely traversed
        while (right != null) {
            if (count % 2 == 0) {
                System.out.print(right.getData() + ",");
            }
            right = right.getRight();
            count++;
        }
        // If left sub tree is not completely traversed
        while (left != null) {
            if (count % 2 != 0) {
                System.out.print(left.getData() + ",");
            }
            left = left.getLeft();
            count++;
        }
    }


    // null,12 : 3
    //5 4 8
    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree(new int[]{5, 6, 7, 8, 9, 10, 11, 12, 4, 3, 2, 1});
        ExtremeAlternateNodes obj = new ExtremeAlternateNodes();
        obj.printNodes(bst.getRoot());
    }
    /**
     *       5 0
     *      4 6 1
     *     2    8 2
     *           12 3
     *     5 4 8
     */
}
