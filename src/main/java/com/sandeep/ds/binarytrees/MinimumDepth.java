package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 7/21/16.
 */
public class MinimumDepth {

    /* Time: O(n) Space: O(1) */
    public int getDepth(Node root) {
        if (root == null) {
            return 0;
        }

        int left = getDepth(root.getLeft());
        int right = getDepth(root.getRight());

        if (left != 0 && right != 0) {
            return Math.min(left, right) + 1;
        } else {
            return Math.max(left, right) + 1;
        }
    }

}
