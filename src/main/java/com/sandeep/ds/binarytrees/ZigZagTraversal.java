package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * Created by smahanty on 8/23/16.
 */
public class ZigZagTraversal {
    public static void printZigZagTraversal(Node root) {
        boolean flag = true;
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);
        /**
         * 5
         * 4  8
         * 3   7  9
         * 2   6    10
         * 1
         *
         * 5 8 4
         * 9 7 3
         * @param args t
         */
        while (!queue.isEmpty()) {
            int count = queue.size();

            while (count > 0) {
                Node node = queue.remove();
                System.out.print(node.getData()+",");
                if (flag) {
                    if (node.getLeft() != null) {
                        queue.add(node.getLeft());
                    }
                    if (node.getRight() != null) {
                        queue.add(node.getRight());
                    }
                } else {
                    if (node.getRight() != null) {
                        queue.add(node.getRight());
                    }
                    if (node.getLeft() != null) {
                        queue.add(node.getLeft());
                    }
                }
                count--;
            }
            if (flag) {
                flag = false;
            } else {
                flag = true;
            }
        }
    }


    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree(new int[]{5, 4, 3, 2, 1, 8, 7, 6, 9, 10});
        printZigZagTraversal(tree.getRoot());
    }
}
