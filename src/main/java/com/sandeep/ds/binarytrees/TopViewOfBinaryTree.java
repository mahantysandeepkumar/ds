package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

/**
 * Created by smahanty on 8/2/16.
 */

class NodeToDistance {

    private Node node;
    private int distance;

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public NodeToDistance(Node node, int distance) {
        this.node = node;
        this.distance = distance;
    }
}

public class TopViewOfBinaryTree {

    public String getTopView(Node root) {
        StringBuilder str = new StringBuilder();
        Set<Integer> hash = new HashSet<>();
        Queue<NodeToDistance> queue = new ArrayDeque<>();
        queue.add(new NodeToDistance(root, 0));

        while (!queue.isEmpty()) {
            NodeToDistance temp = queue.remove();
            Node node = temp.getNode();
            int distance = temp.getDistance();

            if (!hash.contains(distance)) {
                str.append(node.getData() + ", ");
            }

            if (node.getLeft() != null) {
                queue.add(new NodeToDistance(node.getLeft(), distance - 1));
            }

            if (node.getRight() != null) {
                queue.add(new NodeToDistance(node.getRight(), distance + 1));
            }
        }
        return str.toString();
    }
}
