package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by smahanty on 4/5/17.
 */
public class MirrorTree {

    public Node getMirror(Node original) {
        Node result = mirror(original, new Node());
        return result;
    }

    /* Recursive solution */
    public Node mirror(Node orig, Node mirror) {
        if (orig == null) {
            return null;
        }
        mirror.setRight(mirror(orig.getLeft(), new Node()));
        mirror.setData(orig.getData());
        mirror.setLeft(mirror(orig.getRight(), new Node()));
        return mirror;
    }
}
