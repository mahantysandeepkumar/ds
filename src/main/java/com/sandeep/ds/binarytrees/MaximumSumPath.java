package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 7/22/16.
 */
public class MaximumSumPath {

    /*
        Given a binary tree, find the maximum path sum. The path may start and end at any node in the tree.
        Example:
        Input: Root of below tree
               1
              / \
             2   -3
        Output: 3

        Example:
        Input: Root of below tree
               10
              / \
             2  10
            / \  \
           20 1  -25
                  / \
                 3  4
        Output: 42 (20+2+10+10)
     */

    /* Time: O(n) Space: O(1) */
    public int getMaximum(Node root) {
        if (root == null) {
            return Integer.MIN_VALUE;
        }

        int max = root.getData();
        int left = getMaximum(root.getLeft());
        int right = getMaximum(root.getRight());

        if (left != Integer.MIN_VALUE) {
            max = Math.max(max, max + left);
        }

        if (right != Integer.MIN_VALUE) {
            max = Math.max(max, max + right);
        }
        return max;
    }
}
