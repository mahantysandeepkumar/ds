package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by mahantys on 07/07/17.
 */
public class CloneBinaryTree {
    public Node clone(Node root) {
        if (root == null) {
            return null;
        }

        return inOrder(root);
    }

    private Node inOrder(Node root) {
        if (root == null) {
            return null;
        }
        Node newRoot = new Node();
        newRoot.setLeft(inOrder(root.getLeft()));
        newRoot.setData(root.getData());
        newRoot.setRight(inOrder(root.getRight()));
        return newRoot;
    }
}
