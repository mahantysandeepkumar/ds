package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 8/2/16.
 */
public class CheckIfFullBinaryTree {

    /* Time: O(n) Space: O(1) */

    public boolean isBinaryTree(Node root) {

        if (root == null) {
            return true;
        }

        if (root.getLeft() == null && root.getRight() == null) {
            return true;
        }

        if (root.getLeft() != null && root.getRight() != null) {
            return isBinaryTree(root.getLeft()) && isBinaryTree(root.getRight());
        }

        return false;
    }
}
