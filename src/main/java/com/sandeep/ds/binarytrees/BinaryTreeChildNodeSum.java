package com.sandeep.ds.binarytrees;

import lombok.AllArgsConstructor;
import lombok.Data;

public class BinaryTreeChildNodeSum {

    TreeNode root;

    public static void main(String[] args) {
        TreeNode l21 = new TreeNode(1, null, null);
        TreeNode l22 = new TreeNode(1, null, null);
        TreeNode r21 = new TreeNode(3, null, null);
        TreeNode r22 = new TreeNode(1, null, null);
        TreeNode l1 = new TreeNode(2, l21, l22);
        TreeNode r1 = new TreeNode(4, r21, r22);
        TreeNode root = new TreeNode(6, l1, r1);
        System.out.println("Before: " + root);
        BinaryTreeChildNodeSum obj = new BinaryTreeChildNodeSum(root);
        addUp(root);
        System.out.println("After: " + root);
    }

    public static int addUp(TreeNode node) {
        if (node == null) {
            return 0;
        }
        if (node.left != null) {
            node.data += addUp(node.left);
        }
        if (node.right != null) {
            node.data += addUp(node.right);
        }
        return node.data;
    }

    @Data
    static class TreeNode {
        private int data;
        private TreeNode left;
        private TreeNode right;

        public TreeNode(int data, TreeNode left, TreeNode right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }
    }

    public BinaryTreeChildNodeSum(TreeNode root) {
        this.root = root;
    }
}
