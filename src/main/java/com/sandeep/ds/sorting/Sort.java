package com.sandeep.ds.sorting;

public interface Sort {
    int[] sort(int[] arr);
}
