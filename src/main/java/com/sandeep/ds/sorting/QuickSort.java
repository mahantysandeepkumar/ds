package com.sandeep.ds.sorting;

/**
 * Created by smahanty on 4/7/17.
 */
public class QuickSort implements Sort{

    public int[] sort(int[] arr) {
        quickSort(arr, 0, arr.length - 1);
        return arr;
    }

    public void quickSort(int[] arr, int start, int end) {
        int pivot = end;
        int low = start;
        int high = end;

        while (low < high) {
            while (arr[low] < arr[pivot]) {
                low++;
            }
            while (arr[high] > arr[pivot]) {
                high++;
            }
            if (low <= high) {
                swap(arr, low, high);
                low++;
                high--;
            }
        }
        if (low < end) {
            quickSort(arr, low, end);
        }
        if (high > start) {
            quickSort(arr, start, high);
        }

    }

    public void swap(int[] arr, int one, int two) {
        int temp = arr[one];
        arr[one] = arr[two];
        arr[two] = temp;
    }
}

