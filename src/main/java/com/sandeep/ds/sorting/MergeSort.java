package com.sandeep.ds.sorting;

import java.util.Arrays;

public class MergeSort implements Sort{
    public static void main(String[] args) {
        MergeSort obj = new MergeSort();
        //System.out.println(Arrays.toString(obj.sort(new int[]{4, 3, 1, 6, 2, 5})));
        System.out.println(Arrays.toString(obj.sort(new int[]{11, 3, 5, 11, 7, 1})));
    }

    public int[] sort(int[] arr) {
        mergeSort(arr, 0, arr.length - 1);
        return arr;
    }

    public void mergeSort(int[] arr, int start, int end) {
        if (start >= end) {
            return;
        }
        int mid = (start + end) / 2;
        mergeSort(arr, start, mid);
        mergeSort(arr, mid + 1, end);
        merge(arr, start, end, mid);
    }

    private void merge(int[] arr, int start, int end, int mid) {
        int len = end - start + 1;
        int[] temp = new int[len];

        int i = start;
        int j = mid + 1;
        int k = 0;
        for (int l = start; l <= end; l++) {
            if (i > mid) {
                temp[k++] = arr[j++];
            } else if (j > end) {
                temp[k++] = arr[i++];
            } else if (arr[i] < arr[j]) {
                temp[k++] = arr[i++];
            } else {
                temp[k++] = arr[j++];
            }
        }

        for (int l = 0; l < len; l++) {
            arr[start++] = temp[l];
        }
        System.out.println("Op: "+Arrays.toString(arr));
    }
}
