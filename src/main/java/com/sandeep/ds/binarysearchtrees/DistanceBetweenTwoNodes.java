package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by mahantys on 07/07/17.
 */
public class DistanceBetweenTwoNodes {

    public int getDistance(Node root, int a, int b) {
        if (root == null) {
            return -1;
        }
        Node temp = root;
        Node lca = new LowestCommonAncestor().findLowestCommonAncestorUsingMap(temp, a, b);
        int left = getDistance(lca, a);
        int right = getDistance(lca, b);
        return ((left >= 0 && right >= 0) ? (left + right) : -1);
    }

    private int getDistance(Node start, int a) {
        int distance = 0;
        Node temp = start;
        while (temp != null) {
            if (temp.getData() == a) {
                break;
            }
            if (temp.getData() > a) {
                temp = temp.getLeft();
            } else if (temp.getData() < a) {
                temp = temp.getRight();
            }
            distance++;
        }
        if (temp == null) {
            return -1;
        }
        return distance;
    }
}
