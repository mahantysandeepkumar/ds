package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.LinkedList;

/**
 * Created by mahantys on 08/07/17.
 */
public class SwappedNodes {
    /**
     * Two nodes of a BST are swapped.
     */


    public void rectifyBST(Node root) {
        if (root == null) {
            return;
        }

        LinkedList<Node> inOrder = new LinkedList<>();

        inOrder(root, inOrder);
        int left = 0;
        int right = inOrder.size() - 1;

        while (inOrder.get(left).getData() < inOrder.get(left + 1).getData()) {
            left++;
        }

        while (inOrder.get(right).getData() > inOrder.get(right - 1).getData()) {
            right++;
        }

        Node temp = inOrder.get(left);
        inOrder.set(left, inOrder.get(right));
        inOrder.set(right, temp);
        System.out.println(inOrder);
    }

    void inOrder(Node node, LinkedList<Node> list) {
        if (node == null) {
            return;
        }
        inOrder(node.getLeft(), list);
        list.add(node);
        inOrder(node.getRight(), list);
    }

    public static void main(String[] args) {
        SwappedNodes obj = new SwappedNodes();
        BinarySearchTree bst = new BinarySearchTree(new int[]{10,5,8,2,20});
        Node temp = bst.getRoot();
        Node parent = null;
        while(temp!= null) {
            if(temp.getData()==5) {
                parent = temp;
                break;
            }
            if(temp.getData() > 5) {
                temp = temp.getLeft();
            }else{
                temp = temp.getRight();
            }
        }

        temp = parent.getRight();
        parent.setRight(bst.getRoot().getRight());
        bst.getRoot().setRight(temp);
        System.out.println(" before swap : ");
        bst.printInOrder(bst.getRoot());
        System.out.println(" after swap : ");
        obj.rectifyBST(bst.getRoot());

    }

}
