package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 7/11/16.
 */
public class InorderSuccessor {

    /*
        In Binary Tree, Inorder successor of a node is the next node in Inorder traversal of the Binary Tree.
        Inorder Successor is NULL for the last node in Inoorder traversal.
        In Binary Search Tree, Inorder Successor of an input node can also be defined as the node with the smallest
        key greater than the key of input node. So, it is sometimes important to find next node in sorted order.
     */
    public int findInorderSuccessor(Node root, int key) {
        Node temp = root;
        Node prev = root;
        while (temp != null) {

            if (temp.getData() == key) {
                if (temp.getRight() != null) {
                    prev = temp.getRight();
                } else {
                    break;
                }
            }

            if (temp.getData() > key) {
                prev = temp;
                temp = temp.getLeft();
            } else {
                temp = temp.getRight();
            }
        }
        return prev.getData();
    }

}
