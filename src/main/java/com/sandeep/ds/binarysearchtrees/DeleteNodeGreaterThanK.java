package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by mahantys on 07/07/17.
 */
public class DeleteNodeGreaterThanK {

    public Node deleteGreaterThanK(Node root, int k) {
        Node temp = root;

        while (temp != null) {
            if (temp.getData() == k) {
                root = temp;
                root.setRight(null);
                break;
            }

            if (temp.getData() > k) {
                temp = temp.getLeft();
            } else {
                temp = temp.getRight();
            }
        }
        return root;
    }

}
