package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.ArrayList;

/**
 * Created by smahanty on 7/8/16.
 */


public class CheckIfBinaryTree {

    /*
        A binary search tree (BST) is a node based binary tree data structure which has the following properties.
        • The left subtree of a node contains only nodes with keys less than the node’s key.
        • The right subtree of a node contains only nodes with keys greater than the node’s key.
        • Both the left and right subtrees must also be binary search trees.

        From the above properties it naturally follows that:
        • Each node (item in the tree) has a distinct key.

     */

    public boolean checkIfBinaryTree(Node root) {
        Node temp = root;
        ArrayList<Integer> list = new ArrayList<>();
/*
        inOrder(temp, list);

        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i) > list.get(i + 1)) {
                return false;
            }
        }
        return true; */
        this.prev = null;
        return isBST(root);
    }

    public void inOrder(Node root, ArrayList<Integer> list) {
        if (root == null) {
            return;
        }
        inOrder(root.getLeft(), list);
        list.add(root.getData());
        inOrder(root.getRight(), list);
    }

    Node prev = null;

    public boolean isBST(Node root) {
        if (root != null) {
            if (!isBST(root.getLeft())) {
                return false;
            }

            if (this.prev != null && root.getData() <= this.prev.getData()) {
                return false;
            }
            this.prev = root;
            return isBST(root.getRight());

        }
        return true;
    }
}
