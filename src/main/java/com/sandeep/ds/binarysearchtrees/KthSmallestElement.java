package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 7/11/16.
 */
public class KthSmallestElement {

    /*
        Given root of binary search tree and K as input, find K-th smallest element in BST.
     */
    private int count = 0;
    private int data = 0;

    public int kthSmallestElement(Node root, int k) {
        count = 0;
        data = 0;
        kthSmallest(root, k);
        return data;
    }

    /* Time: O(n) Space:O(1) */
    public void kthSmallest(Node root, int k) {
        if (root == null) {
            return;
        }

        kthSmallest(root.getLeft(), k);

        count++;

        if (count == k) {
            data = root.getData();
        }

        kthSmallest(root.getRight(), k);
    }
}
