package com.sandeep.ds.binarysearchtrees.model;

import java.util.Arrays;

public class BinarySearchTree {
    private Node root;
    private int size;

    public int getSize() {
        return size;
    }

    public Node getRoot() {
        return root;
    }

    public void insert(int data) {
        root = addNodeR(root, data);
        size++;
    }

    public Node addNodeR(Node node, int data) {
        if (node == null) {
            node = new Node(data);
            return node;
        }
        if (node.getData() > data) {
            node.setLeft(addNodeR(node.getLeft(), data));
        } else {
            node.setRight(addNodeR(node.getRight(), data));
        }
        return node;
    }

    public void addNode(int data) {

        if (root == null) {
            root = new Node(data);
            return;
        }

        Node temp = root;
        Node parent = root;

        while (temp != null) {
            parent = temp;
            if (data > temp.getData()) {
                temp = temp.getRight();
            } else {
                temp = temp.getLeft();
            }
        }

        if (data > parent.getData()) {
            parent.setRight(new Node(data));
        } else {
            parent.setLeft(new Node(data));
        }

    }

    public Node findNode(int data) {
        if (root == null) {
            return null;
        }

        Node temp = root;
        while (temp != null) {
            if (temp.getData() == data) {
                return temp;
            }
            if (data > temp.getData()) {
                temp = temp.getRight();
            } else {
                temp = temp.getLeft();
            }
        }

        return null;
    }

    public void printInOrder(Node root) {
        if (root == null) {
            return;
        }
        printInOrder(root.getLeft());
        System.out.print(root.getData() + ", ");
        printInOrder(root.getRight());
    }

    public void printPreOrder(Node root) {
        if (root == null) {
            return;
        }
        System.out.print(root.getData() + ", ");
        printPreOrder(root.getLeft());
        printPreOrder(root.getRight());
    }

    public void printPostOrder(Node root) {
        if (root == null) {
            return;
        }

        printPostOrder(root.getLeft());
        printPostOrder(root.getRight());
        System.out.print(root.getData() + ", ");
    }

    public BinarySearchTree() {

    }

    public BinarySearchTree(int[] elements) {
        for (int i = 0; i < elements.length; i++) {
            insert(elements[i]);
        }
    }

    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree();
        tree.insert(6);
        tree.insert(5);
        tree.insert(4);
        tree.insert(7);
        tree.insert(9);
        tree.insert(2);
        tree.printInOrder(tree.root);
        Node node = tree.findNode(2);
        System.out.println("Searched: " + 2 + " Found: " + node.getData());
    }
}
