package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 7/11/16.
 */
public class PrintRange {

    /*
        Given two values k1 and k2 (where k1 < k2) and a root pointer to a Binary Search Tree.
        Print all the keys of tree in range k1 to k2. i.e. print all x such that k1<=x<=k2 and
        x is a key of given BST. Print all the keys in increasing order.
     */

    public String printRange(Node root, int k1, int k2) {
        StringBuilder result = new StringBuilder();
        getRange(root, k1, k2, result);
        return result.toString();
    }

    /* Time: O(n) Space: O(1) */
    public void getRange(Node root, int k1, int k2, StringBuilder result) {
        if (root == null) {
            return;
        }
        getRange(root.getLeft(), k1, k2, result);
        if (root.getData() >= k1 && root.getData() <= k2) {
            result.append(root.getData() + ",");
        }
        getRange(root.getRight(), k1, k2, result);
    }

}
