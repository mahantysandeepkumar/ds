package com.sandeep.ds.binarysearchtrees;

/**
 * Created by smahanty on 7/11/16.
 */
public class PrintSortedArray {

    /*
        Given an array that stores a complete Binary Search Tree, write a function that
        efficiently prints the given array in ascending order.

        For example:
        given an array [4, 2, 5, 1, 3], the function should print 1, 2, 3, 4, 5
    */
    public String printArray(int[] tree) {
        StringBuilder result = new StringBuilder();
        printInorder(tree, 0, tree.length - 1, result);
        return result.toString();
    }

    void printInorder(int[] tree, int start, int end, StringBuilder str) {
        if (start > end) {
            return;
        }

        printInorder(tree, start * 2 + 1, end, str);
        if (tree[start] != -1) {
            str.append(tree[start] + ",");
        }
        printInorder(tree, start * 2 + 2, end, str);
    }
}
