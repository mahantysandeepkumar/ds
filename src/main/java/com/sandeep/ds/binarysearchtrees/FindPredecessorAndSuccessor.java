package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.Deque;
import java.util.LinkedList;

/**
 * Created by smahanty on 7/8/16.
 */
public class FindPredecessorAndSuccessor {
    public Node findPredecessor(Node root, int key) {
        Node temp = root;
        while (temp != null) {
            if (temp.getLeft() != null) {
                if (temp.getLeft().getData() == key) {
                    return temp;
                }
            }
            if (temp.getRight() != null) {
                if (temp.getRight().getData() == key) {
                    return temp;
                }
            }
            if (temp.getData() > key) {
                temp = temp.getLeft();
            } else {
                temp = temp.getRight();
            }
        }
        return null;
    }


    public static void main(String[] args) {
        FindPredecessorAndSuccessor obj = new FindPredecessorAndSuccessor();
        BinarySearchTree bst = new BinarySearchTree(new int[]{4, 3, 6, 7, 2, 1, 8});
        Node node = obj.findPredecessor(bst.getRoot(), 8);
        System.out.println("Predecessor: " + node.getData());
    }
}
