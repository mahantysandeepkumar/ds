package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.HashMap;

/**
 * Created by smahanty on 7/8/16.
 */
public class LowestCommonAncestor {

    /*
        Let T be a rooted tree. The lowest common ancestor between two nodes n1 and n2 is
        defined as the lowest node in T that has both n1 and n2 as descendants (where we
        allow a node to be a descendant of itself).
     */

    /* Time: O(n) Space: O(n) */
    public Node findLowestCommonAncestorUsingMap(Node root, int key1, int key2) {
        HashMap<Node, Integer> map = new HashMap<>();
        Node temp = root;
        Node result = null;
        while (temp != null) {
            map.put(temp, 1);
            if (temp.getData() == key1) {
                break;
            }
            if (temp.getData() > key1) {
                temp = temp.getLeft();
            } else {
                temp = temp.getRight();
            }
        }

        temp = root;
        while (temp != null) {
            if (map.containsKey(temp)) {
                result = temp;
            }
            if (temp.getData() == key2) {
                break;
            }
            if (temp.getData() > key2) {
                temp = temp.getLeft();
            } else {
                temp = temp.getRight();
            }
        }
        return result;
    }

    /* Time: O(n) Space: O(1) */
    public Node findLowestCommonAncestorWithoutExtraSpace(Node root, int key1, int key2) {
        Node result = null;

        Node temp = root;

        while (temp != null) {
            if (temp.getData() > key1 && temp.getData() > key2) {
                /* If both keys exist in left subtree */
                temp = temp.getLeft();
            } else if (temp.getData() < key1 && temp.getData() < key2) {
                /* If both keys exist in right subtree */
                temp = temp.getRight();
            } else {
                /* If both keys exist in different subtrees then this is the node which lies between key1 and key2 */
                result = temp;
                break;
            }
        }
        return result;
    }
}
