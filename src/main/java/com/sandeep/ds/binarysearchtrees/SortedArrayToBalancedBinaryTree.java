package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 7/12/16.
 */
public class SortedArrayToBalancedBinaryTree {

    public Node createBSTfromArray(int[] elements) {
        Node root = construct(0, elements.length - 1, elements);
        return root;
    }

    public Node construct(int start, int end, int[] elements) {
        if (start > end) {
            return null;
        }

        int mid = (end + start) / 2;
        Node node = new Node(elements[mid]);

        node.setLeft(construct(start, mid - 1, elements));
        node.setRight(construct(mid + 1, end, elements));

        return node;
    }
}
