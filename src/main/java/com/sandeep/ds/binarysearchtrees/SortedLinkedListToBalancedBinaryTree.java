package com.sandeep.ds.binarysearchtrees;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by smahanty on 4/4/17.
 */
public class SortedLinkedListToBalancedBinaryTree {
    class TNode {
        TNode left;
        TNode right;
        int data;

        public TNode(int data) {
            this.data = data;
        }
    }

    private Iterator<Integer> itr;

    public TNode getBalancedBinaryTree(LinkedList<Integer> ll) {
        int n = ll.size();
        itr = ll.iterator();
        System.out.println("List: " + ll);
        TNode root = binaryTreeUtil(n);
        System.out.print("Tree (Pre): ");
        preOrder(root);
        System.out.println();
        System.out.print("Tree (In): ");
        inOrder(root);
        return root;
    }

    private TNode binaryTreeUtil(int n) {
        if (n <= 0) {
            return null;
        }

        TNode left = binaryTreeUtil(n / 2);

        TNode root = new TNode(itr.next());

        root.left = left;

        root.right = binaryTreeUtil(n - (n / 2) - 1);

        return root;
    }

    private void preOrder(TNode root) {
        if (root == null) {
            return;
        }

        System.out.print(root.data + ", ");
        preOrder(root.left);
        preOrder(root.right);
    }

    private void inOrder(TNode root) {
        if (root == null) {
            return;
        }

        inOrder(root.left);
        System.out.print(root.data + ", ");
        inOrder(root.right);
    }

    public static void main(String[] args) {
        LinkedList<Integer> ll = new LinkedList<>();
        ll.add(1);
        ll.add(2);
        ll.add(3);
        ll.add(4);
        ll.add(5);
        ll.add(6);
        ll.add(7);

        SortedLinkedListToBalancedBinaryTree obj = new SortedLinkedListToBalancedBinaryTree();
        obj.getBalancedBinaryTree(ll);
    }
}
