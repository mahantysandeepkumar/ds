package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.Node;

/**
 * Created by smahanty on 7/7/16.
 */
public class MinimumElement {

    /*
    This is quite simple. Just traverse the node from root to left recursively until left is NULL.
    The node whose left is NULL is the node with minimum value.
     */

    /* Time:
    *  Average case: O(log n)
    *  Best case :  O(1) Example: right skew tree formed from sorted elements
    *  Worst case :  O(n) Example: left skew tree formed from reverse sorted elements
    *  Space: O(1)
    * */
    public int findMinimum(Node root) {
        Node temp = root;
        int result = 0;
        while (temp != null) {
            result = temp.getData();
            temp = temp.getLeft();
        }
        return result;
    }
}
