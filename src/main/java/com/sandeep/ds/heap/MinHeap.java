package com.sandeep.ds.heap;

import org.apache.commons.math.stat.descriptive.rank.Min;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class MinHeap {
    int[] data;
    private int size;
    private int length;

    public static void main(String[] args) {
        int[] arr = new int[]{1, 3, 2, 2, 3, 1,1,1};
        MinHeap heap = new MinHeap(arr);
        System.out.println(Arrays.toString(heap.heapSort()));
    }

    private void buildHeap() {
        for (int i = size / 2; i >= 1; i--) {
            heapify(i);
        }
    }

    private int[] heapSort() {
        int[] result = new int[size];

        for (int i = 0; i < result.length; i++) {
            result[i] = getMin();
        }

        return result;
    }

    private int peekMin() {
        if (size < 1) {
            throw new NoSuchElementException("Heap is empty");
        }
        return data[1];
    }

    private int getMin() {
        if (size < 1) {
            throw new NoSuchElementException("Heap is empty");
        }
        int result = peekMin();
        data[1] = data[size];
        size--;
        heapify(1);
        return result;
    }

    private void heapify(int index) {
        int left = index * 2;
        int right = index * 2 + 1;

        int min = index;

        if (left <= size && data[min] > data[left]) {
            min = left;
        }
        if (right <= size && data[min] > data[right]) {
            min = right;
        }

        if (min != index) {
            swap(index, min);
            heapify(min);
        }
    }

    private void swap(int left, int right) {
        int temp = data[left];
        data[left] = data[right];
        data[right] = temp;
    }

    public MinHeap(int[] elements) {
        this.size = elements.length;
        this.length = size + 1;
        this.data = new int[length];
        for (int i = 0; i < elements.length; i++) {
            data[i + 1] = elements[i];
        }
        buildHeap();
        System.out.println(Arrays.toString(data));
    }
}
