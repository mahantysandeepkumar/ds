package com.sandeep.ds.heap;

import java.util.Arrays;
import java.util.List;

public class KthSmallest {
    public static void main(String[] args) {
        KthSmallest sol = new KthSmallest();
        System.out.println(sol.kthsmallest(Arrays.asList(new Integer[]{2, 1, 4, 3, 2}), 4));
    }

    public int kthsmallest(final List<Integer> A, int B) {
        Heap heap = new Heap(A);
        int count = 0;
        int kthSmallestElement = -1;
        while (count < B && count < A.size()) {
            kthSmallestElement = heap.get();
            count++;
        }
        return kthSmallestElement;
    }

    class Heap {
        private int[] data;
        private int size;

        private void minHeapify(int i) {
            int left = 2 * i;
            int right = 2 * i + 1;
            int smallest = i;

            if (left <= size && data[left] < data[i]) {
                smallest = left;
            }

            if (right <= size && data[right] < data[smallest]) {
                smallest = right;
            }

            if (smallest != i) {
                swap(i, smallest);
                minHeapify(smallest);
            }
        }

        //Min Heap
        private void buildHeap() {
            for (int i = data.length / 2; i >= 1; i--) {
                minHeapify(i);
            }
        }

        public void swap(int i, int j) {
            int temp = data[i];
            data[i] = data[j];
            data[j] = temp;
        }

        public int get() {
            if (size > 0) {
                int max = data[1];
                data[1] = data[size];
                size--;
                minHeapify(1);
                return max;
            }
            return -1;
        }

        public Heap(List<Integer> data) {
            this.data = new int[data.size() + 1];
            this.size = data.size();
            for (int i = 0; i < data.size(); i++) {
                this.data[i + 1] = data.get(i);
            }
            buildHeap();
        }
    }
}
