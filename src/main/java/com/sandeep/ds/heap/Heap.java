package com.sandeep.ds.heap;

import java.util.Arrays;

/**
 * Created by smahanty on 4/13/17.
 */
public class Heap {
    private int[] data;
    private int size;
    private HeapType type;

    public enum HeapType {
        MIN_HEAP, MAX_HEAP
    }

    // Assuming max heap
    private void maxHeapify(int i) {
        int left = 2 * i;
        int right = 2 * i + 1;

        int largest = i;

        if (left <= size && data[left] > data[i]) {
            largest = left;
        }

        if (right <= size && data[right] > data[largest]) {
            largest = right;
        }

        if (largest != i) {
            swap(i, largest);
            maxHeapify(largest);
        }
    }

    private void minHeapify(int i) {
        int left = 2 * i;
        int right = 2 * i + 1;
        int smallest = i;

        if (left <= size && data[left] < data[i]) {
            smallest = left;
        }

        if (right <= size && data[right] < data[smallest]) {
            smallest = right;
        }

        if (smallest != i) {
            swap(i, smallest);
            minHeapify(smallest);
        }
    }

    //Max Heap
    private void buildHeap() {
        for (int i = data.length / 2; i >= 1; i--) {
            if (type.equals(HeapType.MAX_HEAP)) {
                maxHeapify(i);
            } else {
                minHeapify(i);
            }
        }
    }

    public void swap(int i, int j) {
        int temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

    public int[] toArray() {
        int[] results = new int[data.length - 1];
        int i = 0;
        while (size > 0) {
            results[i++] = get();
        }
        return results;
    }

    public int get() {
        if (size > 0) {
            int max = data[1];
            data[1] = data[size];
            size--;
            if (type.equals(HeapType.MAX_HEAP)) {
                maxHeapify(1);
            } else {
                minHeapify(1);
            }
            return max;
        }
        return -1;
    }

    public int getSize() {
        return size;
    }

    public Heap(int[] data) {
        this(data, HeapType.MAX_HEAP);
    }

    public Heap(int[] data, HeapType heapType) {
        this.data = new int[data.length + 1];
        this.size = data.length;
        this.type = heapType;
        System.arraycopy(data, 0, this.data, 1, data.length);
        buildHeap();
    }

    public static void main(String[] args) {
        Heap heap = new Heap(new int[]{1, 2, 3, 4, 5, 6});
        System.out.println("Max: " + heap);
        while (heap.getSize() > 0) {
            System.out.print(heap.get() + ", ");
        }

        Heap anotherHeap = new Heap(new int[]{6, 5, 4, 3, 2, 1}, HeapType.MIN_HEAP);
        System.out.println("\nMin: " + anotherHeap);
        while (anotherHeap.getSize() > 0) {
            System.out.print(anotherHeap.get() + ", ");
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(data);
    }
}
