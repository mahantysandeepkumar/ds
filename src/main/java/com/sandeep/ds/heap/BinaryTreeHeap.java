package com.sandeep.ds.heap;

import com.sandeep.ds.binarysearchtrees.model.Node;
import com.sandeep.ds.graph.Graph;

/**
 * Created by mahantys on 08/07/17.
 */
public class BinaryTreeHeap {
    private int size;
    private Node root;
    private Heap.HeapType type;

    private void heapify(Node node) {

    }

    public void add(int element) {
        if (root == null) {
            root = new Node(Integer.MAX_VALUE);
            root.setLeft(new Node(element));
            return;
        }
    }

    public BinaryTreeHeap() {
        this(Heap.HeapType.MAX_HEAP);
    }

    public BinaryTreeHeap(Heap.HeapType type) {
        this.type = type;
    }
}
