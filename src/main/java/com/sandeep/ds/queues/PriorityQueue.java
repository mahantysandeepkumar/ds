package com.sandeep.ds.queues;


public interface PriorityQueue<K, V> {
    /**
     * Insert an entry with key {@code key} and value {@code value}
     *
     * @param key
     * @param value
     */
    Entry<K, V> insert(K key, V value) throws IllegalArgumentException;

    /**
     * Finds and returns the entry with minimum key. Note: a priority queue can have
     * many entries with equivalent keys. So min() and removeMin() may return
     * arbitrary entry
     *
     * @return
     */
    Entry<K, V> min();

    /**
     * Removes and returns the entry with minimum key. Note: a priority queue can have
     * many entries with equivalent keys. So min() and removeMin() may return
     * arbitrary entry
     *
     * @return
     */
    Entry<K, V> removeMin();

    /**
     * Returns the number of entries in the priority queue
     *
     * @return
     */
    int size();

    /**
     * Returns whether the priority queue is empty
     *
     * @return
     */
    boolean isEmpty();

    interface Entry<K, V> {
        K getKey();

        V getValue();
    }
}
