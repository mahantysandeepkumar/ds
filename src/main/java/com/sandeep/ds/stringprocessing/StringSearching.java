package com.sandeep.ds.stringprocessing;

/**
 * Created by smahanty on 10/28/16.
 */
public class StringSearching {
    public int findByBruteForce(String text, String pattern) {
        int n = text.length();
        int m = pattern.length();
        int j = 0;

        if (m > n) {
            return -1;
        }

        for (int i = 0; i < n - m; i++) {
            while (j < m && text.charAt(j + i) == pattern.charAt(j)) {
                j++;
            }
            if (j == m) {
                return i;
            }
        }
        return -1;
    }

    public int findByBoyreMooreMethod(String text, String pattern) {
        int n = text.length();
        int m = pattern.length();
        int[] last = buildLastTable(pattern);
        int i = m - 1;
        int j = m - 1;
        if (m > n) {
            return -1;
        }

        do {
            if (pattern.charAt(j) == text.charAt(i)) {
                if (j == 0) {
                    return i;
                } else {
                    i--;
                    j--;
                }
            } else {
                i = i + m - Math.min(j, 1 + last[text.charAt(i) - 'a']);
                j = m - 1;
            }

        } while (i <= n - 1);
        return -1;
    }

    private int[] buildLastTable(String pattern) {
        int[] table = new int[26];

        for (int i = 0; i < pattern.length(); i++) {
            table[i] = -1;
        }

        for (int i = 0; i < pattern.length(); i++) {
            table[pattern.charAt(i) - 'a'] = i;
        }

        return table;
    }
}
