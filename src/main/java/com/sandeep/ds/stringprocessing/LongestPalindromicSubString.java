package com.sandeep.ds.stringprocessing;

/**
 * Created by mahantys on 08/07/17.
 */
public class LongestPalindromicSubString {

    public String findPalindromicSubString(String input) {
        int len = input.length();
        int max = 1;
        int start = 0;
        for (int i = 1; i < len; i++) {
            //Even palindromes
            int low = i - 1;
            int high = i;

            while (low >= 0 && high < len && input.charAt(low) == input.charAt(high)) {
                if (max < high - low + 1) {
                    max = high - low + 1;
                    start = low;
                }
                low--;
                high++;
            }

            // Odd palindromes
            low = i - 1;
            high = i + 1;

            while (low >= 0 && high < len && input.charAt(low) == input.charAt(high)) {
                if (max < high - low + 1) {
                    max = high - low + 1;
                    start = low;
                }
                low--;
                high++;
            }

        }
        return input.substring(start, start + max);
    }

}
