package com.sandeep.ds.arrays;

public class LargestContiguousSumSubArray {

	public int findLargestContiguousSubArraySum(int[] arr) {
		int max_so_far = arr[0];
		int curr_max = arr[0];

		for (int i = 1; i < arr.length; i++) {
			curr_max = Math.max(arr[i], curr_max + arr[i]);
			max_so_far = Math.max(max_so_far, curr_max);
		}
		return max_so_far;
	}

	public static void main(String[] args) {
		LargestContiguousSumSubArray obj = new LargestContiguousSumSubArray();
		int arr[] = { 1, 2, -4, 5, 6, 7, 8 };
		int sum = obj.findLargestContiguousSubArraySum(arr);
		System.out.println("Sum: " + sum);
	}
}
