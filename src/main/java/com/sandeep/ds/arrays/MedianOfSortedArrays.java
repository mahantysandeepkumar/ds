package com.sandeep.ds.arrays;

public class MedianOfSortedArrays {

    public double findMedian(int[] a, int[] b) {
        int i = 0, j = 0, count = 0;
        int totalLength = a.length + b.length;
        double median = 0;

        while (i < a.length && j < b.length && count < totalLength / 2) {
            if (a[i] < b[j]) {
                median = a[i];
                i++;
            } else {
                median = b[j];
                j++;
            }
            count++;
        }

        if (totalLength % 2 == 0) {
            if (a[i] < a[j]) {
                median += a[i];
            } else {
                median += b[j];
            }
            return median / 2;
        } else {
            return (a[i] < a[j]) ? a[i] : b[j];
        }
    }

}
