package com.sandeep.ds.arrays;

public class NumberOccuringOddNumberOfTimes {

	/*
	 * Given an array of positive integers. All numbers occur even number of
	 * times except one number which occurs odd number of times. Find the number
	 * in O(n) time & constant space.
	 * 
	 * Example: I/P = [1, 2, 3, 2, 3, 1, 3] O/P = 3
	 */

	/*
	 * XOR Method: Time: O(n) Space: O(1)
	 */
	public int findOddTimesOccurringELementByXOR(int[] arr) {

		int num = arr[0];

		for (int i = 1; i < arr.length; i++) {
			num = num ^ arr[i];
		}

		return num;
	}

}
