package com.sandeep.ds.arrays;

import com.sandeep.ds.graph.FindArticulatePoints;

import java.util.*;

public class FindDuplicate {
    public static void main(String[] args) {
        FindDuplicate obj = new FindDuplicate();
        System.out.println(obj.findDuplicate(Arrays.asList(new Integer[]{3})));
    }

    private int findDuplicate(List<Integer> arrayList) {
        HashSet<Integer> set = new HashSet<>();

        for (Integer element: arrayList) {
            if (set.contains(element)){
                return element;
            }
            set.add(element);
        }
        return -1;
    }
}
