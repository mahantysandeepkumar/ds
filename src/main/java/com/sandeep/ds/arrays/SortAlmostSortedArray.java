package com.sandeep.ds.arrays;

/**
 * Created by mahantys on 07/07/17.
 */
public class SortAlmostSortedArray {
    public int[] sortAlmostSortedArray(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Please pass any array");
        }

        if (array.length == 1) {
            return array;
        }

        int left = 0;
        int right = array.length - 1;

        while (array[left] < array[left + 1]) {
            left++;
        }

        while (array[right] > array[right - 1]) {
            right--;
        }

        swap(array, left, right);
        return array;
    }

    private void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
