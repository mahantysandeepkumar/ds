package com.sandeep.ds.arrays;

/**
 * Created by smahanty on 11/9/16.
 */
public class PathFinding {

    public int findPath(int[][] matrix) {
        int steps = -1;

        findPath(matrix, 2, 3);
        steps = matrix[0][0];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        return steps > 0 ? steps : -1;
    }

    public void findPath(int[][] matrix, int i, int j) {
        if (i < 0 || i >= matrix.length) {
            return;
        }

        if (j < 0 || j >= matrix[0].length) {
            return;
        }

        if (matrix[i][j] != -2) {
            return;
        }

        matrix[i][j] = findSmallestValueGreaterThanZero(matrix, i, j);

        findPath(matrix, i - 1, j);
        findPath(matrix, i + 1, j);
        findPath(matrix, i, j - 1);
        findPath(matrix, i, j + 1);
    }


    public int findSmallestValueGreaterThanZero(int[][] matrix, int i, int j) {
        int num1 = Integer.MAX_VALUE;
        int num2 = Integer.MAX_VALUE;
        int num3 = Integer.MAX_VALUE;
        int num4 = Integer.MAX_VALUE;

        if (isUnderBounds(i - 1, j, matrix) && matrix[i - 1][j] >= 0) {
            num1 = matrix[i - 1][j];
        }

        if (isUnderBounds(i + 1, j, matrix) && matrix[i + 1][j] >= 0) {
            num2 = matrix[i + 1][j];
        }

        if (isUnderBounds(i, j - 1, matrix) && matrix[i][j - 1] >= 0) {
            num3 = matrix[i][j - 1];
        }

        if (isUnderBounds(i, j + 1, matrix) && matrix[i][j + 1] >= 0) {
            num4 = matrix[i][j + 1];
        }
        //System.out.println("Min value for i: " + i + "   j:" + j + " vl: " + num1 + ":" + num2 + ":" + num3 + ":" + num4);
        int min = Math.min(Math.min(num1, num2), Math.min(num3, num4));

        if (min == Integer.MAX_VALUE) {
            return 0;
        } else {
            return min + 1;
        }
    }

    public boolean isUnderBounds(int i, int j, int[][] matrix) {
        boolean insideHorizontalBounds = true;
        boolean insideVerticalBounds = true;

        if (i < 0 || i >= matrix.length) {
            insideVerticalBounds = false;
        }

        if (j < 0 || j >= matrix[0].length) {
            insideHorizontalBounds = false;
        }

        return insideHorizontalBounds && insideVerticalBounds;
    }
}
