package com.sandeep.ds.arrays;

public class MissingNumber {

	/*
	 * You are given a list of n-1 integers and these integers are in the range
	 * of 1 to n. There are no duplicates in list. One of the integers is
	 * missing in the list. Write an efficient code to find the missing integer
	 * Example: I/P [1, 2, 4, ,6, 3, 7, 8] O/P 5
	 * 
	 */

	/* Find by sum : Time: O(n) Space: O(1) */
	public int findMissingElementBySumMethod(int[] arr, int x) {

		int sum = (x * (x + 1)) / 2;

		for (int i = 0; i < arr.length; i++) {
			sum -= arr[i];
		}
		return sum;
	}

	/* Find by XOR : Time: O(n) Space: O(1) */
	public int findMissingElementByXorMethod(int[] arr, int x) {

		int x1 = 0;
		int x2 = 0;

		for (int i = 1; i <= x; i++) {
			x1 ^= i;
		}

		for (int i = 0; i < arr.length; i++) {
			x2 ^= arr[i];
		}

		return x1 ^ x2;
	}

}
