package com.sandeep.ds.arrays;

public class MajorityElement {
	/*
	 * A majority element in a array is an element which occurs more than n/2
	 * times.
	 */
	public boolean doesMajorityElementExists(int[] arr, int op) {
		switch (op) {
		case 1:
			return majorityElementMooresVotingAlgorithm(arr);
		case 2:
			return majorityElementBruteForce(arr);
		default:
			return false;
		}
	}

	/*
	 * Brute Force Method : Space: O(1) Time:O(n^2)
	 * 
	 */
	private boolean majorityElementBruteForce(int[] arr) {
		int length = arr.length;
		int count = 0;

		for (int i = 0; i < length; i++) {
			count = 0;
			for (int j = 0; j < length; j++) {
				if (arr[i] == arr[j]) {
					count++;
				}
			}

			if (count > length / 2) {
				return true;
			}
		}

		return false;
	}

	/*
	 * Moore's Voting Algorithm 1. Find the most frequently occurring element 2.
	 * Check if that element is the majority element Space: O(1) Time: O(n)
	 */
	private boolean majorityElementMooresVotingAlgorithm(int[] arr) {
		int count = 1;
		int index = 0;

		/* Find most frequently occurring element */
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] == arr[index]) {
				count++;
			} else {
				count--;
			}
			if (count == 0) {
				index = i;
				count = 1;
			}
		}

		/* Check if the MOE is a majority element */
		count = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[index] == arr[i]) {
				count++;
			}
			if (count > arr.length / 2) {
				return true;
			}
		}

		return false;
	}

}
