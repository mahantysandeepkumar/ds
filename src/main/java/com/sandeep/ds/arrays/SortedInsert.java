package com.sandeep.ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class SortedInsert {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        list.add(5);
        list.add(6);
        SortedInsert obj = new SortedInsert();
        System.out.println(obj.searchInsert(
                Arrays.asList(new Integer[]{17, 30, 32, 69, 94, 96, 106, 118, 127, 159, 169, 170, 178, 183, 209, 238, 242, 247, 253, 261, 265, 279, 288, 302, 305, 316, 352, 357, 374, 376, 392, 402, 410, 421, 439, 442, 444, 446, 454, 458, 464, 467, 468, 498, 500, 513, 523, 541, 545, 556, 575, 608, 616, 629, 631, 635, 669, 674, 682, 686, 693, 695, 719, 733, 754, 755, 756, 778, 802, 822, 824, 828, 835, 847, 848, 862, 864, 878, 883, 885, 904, 908, 928, 934}), 104));
        System.out.println(obj.searchInsert(list, 5));
        System.out.println(obj.searchInsert(list, 2));
        System.out.println(obj.searchInsert(list, 7));
        System.out.println(obj.searchInsert(list, 0));
    }

    public int searchInsert(List<Integer> a, int b) {
        int length = a.size();

        if (a.size() == 1) {
            if (b > a.get(0)) {
                return 1;
            } else {
                return 0;
            }
        }
        if (b < a.get(0)) {
            return 0;
        } else if (b > a.get(length - 1)) {
            return a.size();
        }
        // binary search
        int left = 0;
        int right = length - 1;
        while (left < right) {
            int mid = (left + right) / 2;
            if (a.get(mid) == b) {
                return mid;
            }
            if (b > a.get(mid)) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }

}
