package com.sandeep.ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MergeSortedLists {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(-5);
        ArrayList<Integer> b = new ArrayList<>();
        b.add(-5);
        merge(a, b);
    }

    public static void merge(List<Integer> a, ArrayList<Integer> b) {
        List<Integer> result = new ArrayList<>();
        int len = a.size() + b.size();
        int lenA = a.size();
        int lenB = b.size();
        int count = 0;
        int i = 0;
        int j = 0;
        while (i < lenA && j < lenB) {
            if (a.get(i) < b.get(j)) {
                result.add(a.get(i++));
            } else {
                result.add(b.get(j++));
            }
        }
        HashMap<Integer,Integer> map;
        while (i < lenA) {
            result.add(a.get(i++));
        }

        while (j < lenB) {
            result.add(b.get(j++));
        }
        a.clear();
        for (int integer: result) {
            a.add(integer);
        }
        System.out.println(a);
    }
}
