package com.sandeep.ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class AddOne {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(9);
        list.add(9);
        AddOne obj = new AddOne();
        System.out.println(obj.plusOne(list));
    }

    public ArrayList<Integer> plusOne(ArrayList<Integer> A) {
        int len = A.size();
        // removing leading zeros
        for (int  i = 0; i < len; i++){
            if (A.get(i) == 0) {
                A.remove(i);
            }else {
                break;
            }
        }

        int carry = 0;
        int add = 1;
        for (int i = A.size() - 1; i >= 0; i--) {
            int element = A.get(i);
            int sum = element + add + carry;
            if (sum > 9) {
                A.set(i, (sum % 10));
                carry = 1;
            } else {
                A.set(i, sum);
                carry = 0;
            }
            add = 0;
        }

        ArrayList<Integer> list = new ArrayList<>();

        if (carry != 0) {
            list.add(carry);
        }

        for (int j = 0; j < A.size(); j++) {
            list.add(A.get(j));
        }

        return list;
    }
}
