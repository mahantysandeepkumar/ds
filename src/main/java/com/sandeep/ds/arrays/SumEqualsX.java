package com.sandeep.ds.arrays;

import java.util.HashMap;

public class SumEqualsX {

	/*
	 * Write a program that, given an array A[] of n numbers and another number
	 * x, determines whether or not there exist two elements in S whose sum is
	 * exactly x.
	 */
	public boolean doesSumExists(int[] arr, int x, int op) {
		switch (op) {
		case 1:
			return doesSumExistsOp1(arr, x);
		case 2:
			return doesSumExistsOp2(arr, x);
		default:
			return false;
		}
	}

	private boolean doesSumExistsOp2(int[] arr, int x) {
		boolean[] map = new boolean[10000];

		for (int i = 0; i < arr.length; i++) {
			int value = x - arr[i];

			if (value >= 0 && map[value]) {
				System.out.println("Numbers are : " + arr[i] + " , " + value);
				return true;
			}

			map[arr[i]] = true;
		}

		return false;
	}

	private boolean doesSumExistsOp1(int[] arr, int x) {
		HashMap<Integer, Integer> map = new HashMap<>();

		for (int i = 0; i < arr.length; i++) {
			map.put(arr[i], (x - arr[i]));
			if (map.get(map.get(arr[i])) != null) {
				return true;
			}
		}

		return false;
	}
}
