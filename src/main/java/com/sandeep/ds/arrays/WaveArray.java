package com.sandeep.ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WaveArray {
    public static void main(String[] args) {
        WaveArray obj = new WaveArray();
        System.out.println(obj.getWaveArray(Arrays.asList(new Integer[]{3, 2, 1})));
    }

    private List<Integer> getWaveArray(List<Integer> arrayList) {
        Collections.sort(arrayList);
        for (int i = 1; i < arrayList.size(); i += 2) {
            int temp = arrayList.get(i);
            arrayList.set(i, arrayList.get(i - 1));
            arrayList.set(i - 1, temp);
        }
        return arrayList;
    }
}
