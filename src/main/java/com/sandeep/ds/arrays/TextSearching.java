package com.sandeep.ds.arrays;

/**
 * Created by smahanty on 9/19/16.
 */
public class TextSearching {

    public int findPatternWithBruteForce(String s, String pattern) {
        int index = -1;

        for (int i = 0; i < s.length(); i++) {
            int count = 0;
            for (int j = 0; j < pattern.length(); j++) {
                if (s.charAt(i + j) != pattern.charAt(j)) {
                    break;
                }
                count++;
            }
            if (count == pattern.length()) {
                index = i;
                break;
            }
        }
        return index;
    }
}
