package com.sandeep.ds.arrays;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class PascalsTriangle {

    public static void main(String[] args) {
        PascalsTriangle obj = new PascalsTriangle();
        System.out.println(obj.getTriangle(4));
    }

    private ArrayList<ArrayList<Integer>> getTriangle(int rows) {
        ArrayList<ArrayList<Integer>> pascalTriangle = new ArrayList<>();
        ArrayList<Integer> row = new ArrayList<>();
        row.add(1);
        pascalTriangle.add(row);
        for (int i = 1; i < rows; i++) {
            ArrayList<Integer> newRow = new ArrayList<>();
            newRow.add(1);
            ArrayList<Integer> prevRow = pascalTriangle.get(i - 1);
            for (int j = 1; j <= i; j++) {
                if (j >= prevRow.size()) {
                    newRow.add(1);
                } else {
                    newRow.add(prevRow.get(j - 1) + prevRow.get(j));
                }
            }
            pascalTriangle.add(newRow);
        }

        return pascalTriangle;
    }
}
