package com.sandeep.ds;

import java.util.HashMap;
import java.util.Scanner;

public class BinaryShuffle {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testCases = sc.nextInt();

        for (int testCase = 0; testCase < testCases; testCase++) {
            int firstNum = sc.nextInt();
            int secondNum = sc.nextInt();

            int count = 0;
            int div = secondNum / firstNum;
            int temp = firstNum;
            while (temp <= secondNum) {
                int sh = getClosest(div);
                //System.out.println("Sh: "+ sh);
                int next = (temp << sh) + 1;
                //System.out.println("next: "+ next);
                if (next <= secondNum) {
                    temp = next;
                }

                temp++;
                count++;
                div = (secondNum - temp) / 2;
            }

            System.out.println(count);
        }
    }

    private static int getClosest(int num) {
        int count = 0;

        while (num >= 2) {
            num = num >> 1;
            count++;
        }
        return count;
    }
}
