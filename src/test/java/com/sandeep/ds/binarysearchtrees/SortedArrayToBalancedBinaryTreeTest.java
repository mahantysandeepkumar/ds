package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/12/16.
 */
public class SortedArrayToBalancedBinaryTreeTest extends TestBase {

    SortedArrayToBalancedBinaryTree obj = new SortedArrayToBalancedBinaryTree();

    @Test
    public void genericTest() {
        int[] elements = {1, 2, 3, 4, 5, 6, 7};
        Node node = obj.createBSTfromArray(elements);
        printPreOrder(node);

    }

    public void printPreOrder(Node root) {

        if (root == null) {
            return;
        }
        printPreOrder(root.getLeft());
        printPreOrder(root.getRight());
        System.out.println(root.getData() + ", ");
    }
}
