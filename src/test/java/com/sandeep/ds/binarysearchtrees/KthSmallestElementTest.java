package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/11/16.
 */
public class KthSmallestElementTest extends TestBase {

    KthSmallestElement obj = new KthSmallestElement();

    @Test
    public void genericPositiveTest() {
        int[] elements = {6, 4, 5, 3, 8, 9};
        BinarySearchTree bst = new BinarySearchTree(elements);
        int result = obj.kthSmallestElement(bst.getRoot(), 3);

        Assert.assertTrue(result == 5, "The " + 3 + "th element should be " + 5 + " and found : " + result);
    }

    @Test
    public void leftSkewTreeTest() {
        int[] elements = {9, 8, 7, 6, 5, 4};
        BinarySearchTree bst = new BinarySearchTree(elements);
        int result = obj.kthSmallestElement(bst.getRoot(), 3);

        Assert.assertTrue(result == 6, "The " + 3 + "th element should be " + 6 + " and found : " + result);
    }

    @Test
    public void rightSkewTreeTest() {
        int[] elements = {1, 2, 3, 4, 5, 6};
        BinarySearchTree bst = new BinarySearchTree(elements);
        int result = obj.kthSmallestElement(bst.getRoot(), 3);

        Assert.assertTrue(result == 3, "The " + 3 + "th element should be " + 3 + " and found : " + result);
    }
}
