package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/8/16.
 */
public class CheckIfBinaryTreeTest {

    CheckIfBinaryTree obj = new CheckIfBinaryTree();

    @Test
    public void genericPositiveTest() {
        int[] elements = {4, 5, 3, 2, 1, 7};
        BinarySearchTree bst = new BinarySearchTree(elements);
        boolean result = obj.checkIfBinaryTree(bst.getRoot());

        Assert.assertTrue(result, "The tree is a binary search tree");
    }

    @Test
    public void genericNegativeTest() {
        int[] elements = {4, 5, 3, 2, 1, 7};

        Node root = new Node(3);
        Node left = new Node(2);
        Node right = new Node(5);
        root.setLeft(left);
        root.setRight(right);
        left.setLeft(new Node(1));
        left.setRight(new Node(4));

        boolean result = obj.checkIfBinaryTree(root);

        Assert.assertFalse(result, "The tree is not a binary search tree");
    }

}
