package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/8/16.
 */
public class LowestCommonAncestorTest extends TestBase {

    LowestCommonAncestor obj = new LowestCommonAncestor();

    @Test
    public void genericPositiveTest() {
        int[] arr = {6, 4, 3, 5, 9, 8, 7, 10};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorUsingMap(bst.getRoot(), 3, 5);
        Assert.assertTrue(result.getData() == 4, "Answer should be 4 ");
    }

    @Test
    public void differentSubtreeTest() {
        int[] arr = {6, 4, 3, 5, 9, 8, 7, 10};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorUsingMap(bst.getRoot(), 3, 8);
        Assert.assertTrue(result.getData() == 6, "Answer should be 6 ");
    }

    @Test
    public void leftSkewTreeTest() {
        int[] arr = {9, 8, 7, 6, 5, 4, 3, 2};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorUsingMap(bst.getRoot(), 3, 8);
        Assert.assertTrue(result.getData() == 8, "Answer should be 8  but got: " + result.getData());
    }

    @Test
    public void rightSkewTreeTest() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorUsingMap(bst.getRoot(), 3, 8);
        Assert.assertTrue(result.getData() == 3, "Answer should be 3  but got: " + result.getData());
    }

    @Test
    public void genericPositiveTestMethod2() {
        int[] arr = {6, 4, 3, 5, 9, 8, 7, 10};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorWithoutExtraSpace(bst.getRoot(), 3, 5);
        Assert.assertTrue(result.getData() == 4, "Answer should be 4 ");
    }

    @Test
    public void differentSubtreeTestMethod2() {
        int[] arr = {6, 4, 3, 5, 9, 8, 7, 10};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorWithoutExtraSpace(bst.getRoot(), 3, 8);
        Assert.assertTrue(result.getData() == 6, "Answer should be 6 ");
    }

    @Test
    public void leftSkewTreeTestMethod2() {
        int[] arr = {9, 8, 7, 6, 5, 4, 3, 2};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorWithoutExtraSpace(bst.getRoot(), 3, 8);
        Assert.assertTrue(result.getData() == 8, "Answer should be 8  but got: " + result.getData());
    }

    @Test
    public void rightSkewTreeTestMethod2() {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node result = obj.findLowestCommonAncestorWithoutExtraSpace(bst.getRoot(), 3, 8);
        Assert.assertTrue(result.getData() == 3, "Answer should be 3  but got: " + result.getData());
    }

}
