package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by mahantys on 07/07/17.
 */
public class DeleteNodeGreaterThanKTest {
    DeleteNodeGreaterThanK obj = new DeleteNodeGreaterThanK();


    @DataProvider(name = "positiveTestProvider")
    private Object[][] positiveTestDataProvider() {
        return new Object[][]{
                {new int[]{5, 4, 3, 2, 1, 0}, 6},
                {new int[]{5, 4, 3, 2, 1, 0}, 4},
                {new int[]{1}, 1},
                {new int[]{1, 2, 3, 4, 5, 6}, 1},
                {new int[]{1, 4, 3, 2, 1, 0}, 3},
                {new int[]{1, 2, 3, 4, 5, 6}, 3}

        };
    }

    @Test(dataProvider = "positiveTestProvider")
    public void deleteNodePositiveTest(int[] arr, int k) {
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node root = obj.deleteGreaterThanK(bst.getRoot(), k);
        Assert.assertTrue(verify(root, k));
    }

    boolean verify(Node root, int k) {
        Node temp = root;

        while (temp != null) {
            if (temp.getData() > k) {
                return false;
            }
            temp = temp.getRight();
        }
        return true;
    }
}
