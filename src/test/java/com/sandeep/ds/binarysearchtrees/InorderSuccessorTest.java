package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/11/16.
 */
public class InorderSuccessorTest extends TestBase {
    InorderSuccessor obj = new InorderSuccessor();

    @Test
    public void genericPositiveTest(){
        int[] elements = {20,8,22,4,12,10,14};
        BinarySearchTree bst = new BinarySearchTree(elements);
        int result = obj.findInorderSuccessor(bst.getRoot(),8);
        Assert.assertTrue(result == 10,"Result should be same");
    }

    @Test
    public void genericPositiveTest2(){
        int[] elements = {20,8,22,4,12,10,14};
        BinarySearchTree bst = new BinarySearchTree(elements);
        int result = obj.findInorderSuccessor(bst.getRoot(),14);
        Assert.assertTrue(result == 20,"Result should be same");
    }
}
