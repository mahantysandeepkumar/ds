package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/11/16.
 */
public class PrintRangeTest extends TestBase {
    PrintRange obj = new PrintRange();

    @Test
    public void rangeCheckTest() {
        int[] elements = {6, 4, 5, 3, 7, 8, 9};
        BinarySearchTree bst = new BinarySearchTree(elements);
        String result = obj.printRange(bst.getRoot(), 4, 7);
        String expected = "4,5,6,7,";
        Assert.assertEquals(result, expected, "The results should match");
    }

    @Test
    public void rangeUnbalancedTreeTest() {
        int[] elements = {20, 8, 4, 12, 22};
        BinarySearchTree bst = new BinarySearchTree(elements);
        String result = obj.printRange(bst.getRoot(), 10, 22);
        String expected = "12,20,22,";
        Assert.assertEquals(result, expected, "The results should match");
    }

    @Test
    public void leftSkewTreeTest() {
        int[] elements = {9, 8, 7, 6, 5, 4};
        BinarySearchTree bst = new BinarySearchTree(elements);
        String result = obj.printRange(bst.getRoot(), 1, 9);
        String expected = "4,5,6,7,8,9,";
        Assert.assertEquals(result, expected, "The results should match");
    }

    @Test
    public void rightSkewTreeTest() {
        int[] elements = {1, 2, 3, 4, 5, 6};
        BinarySearchTree bst = new BinarySearchTree(elements);
        String result = obj.printRange(bst.getRoot(), 4, 7);
        String expected = "4,5,6,";
        Assert.assertEquals(result, expected, "The results should match");
    }
}
