package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/7/16.
 */
public class MinimumElementTest extends TestBase {
    MinimumElement obj = new MinimumElement();

    @Test
    public void genericPositiveTest() {
        int[] treeElements = {4, 3, 6, 7, 2, 1, 8};
        BinarySearchTree bst = new BinarySearchTree(treeElements);
        int result = obj.findMinimum(bst.getRoot());
        Assert.assertTrue(result == 1, "Result should be 1");
    }

    @Test
    public void negativeElementsTest() {
        int[] treeElements = {-4, -3, -6, -7, -2, -1, -8};
        BinarySearchTree bst = new BinarySearchTree(treeElements);
        int result = obj.findMinimum(bst.getRoot());
        Assert.assertTrue(result == -8, "Result should be -8");
    }
}
