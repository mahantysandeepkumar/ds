package com.sandeep.ds.binarysearchtrees;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/11/16
 */
public class PrintSortedArrayTest extends TestBase {

    PrintSortedArray obj = new PrintSortedArray();

    @Test
    public void genericPositiveTest() {
        int[] elements = {4, 2, 5, 1, 3};

        String expected = "1,2,3,4,5,";
        String actual = obj.printArray(elements);

        Assert.assertEquals(actual, expected, "Results should match");
    }

    @Test
    public void alreadySortedTest() {
        int[] elements = {1, -1, 2, -1, -1, -1, 3, -1, -1, -1, -1, -1, -1, -1, 4};
        String expected = "1,2,3,4,";
        String actual = obj.printArray(elements);

        Assert.assertEquals(actual, expected, "Results should match");
    }

    @Test
    public void oneElementTest() {
        int[] elements = {6};
        String expected = "6,";
        String actual = obj.printArray(elements);

        Assert.assertEquals(actual, expected, "Results should match");
    }
}
