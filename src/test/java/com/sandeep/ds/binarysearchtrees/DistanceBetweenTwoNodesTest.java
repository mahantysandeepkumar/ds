package com.sandeep.ds.binarysearchtrees;

import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by mahantys on 07/07/17.
 */
public class DistanceBetweenTwoNodesTest {
    DistanceBetweenTwoNodes obj = new DistanceBetweenTwoNodes();

    @Test
    public void distancePositiveTest() {
        BinarySearchTree bst = new BinarySearchTree(new int[]{5, 3, 4, 2, 7, 9});
        int distance = obj.getDistance(bst.getRoot(), 3, 9);
        Assert.assertTrue(distance == 3, " Expected 3 but got " + distance);
    }

    @Test
    public void distanceLeftSkewTreeTest() {
        BinarySearchTree bst = new BinarySearchTree(new int[]{9, 8, 7, 6, 5, 4});
        int distance = obj.getDistance(bst.getRoot(), 8, 9);
        Assert.assertTrue(distance == 1, " Expected 1 but got " + distance);
    }

    @Test
    public void distanceRightSkewTreeOutOfRangeTest() {
        BinarySearchTree bst = new BinarySearchTree(new int[]{1, 2, 3, 4, 5, 6});
        int distance = obj.getDistance(bst.getRoot(), 8, 9);
        Assert.assertTrue(distance == -1, " Expected 0 but got " + distance);
    }

    @Test
    public void distanceRightSkewTreeTest() {
        BinarySearchTree bst = new BinarySearchTree(new int[]{1, 2, 3, 4, 5, 6});
        int distance = obj.getDistance(bst.getRoot(), 4, 6);
        Assert.assertTrue(distance == 2, " Expected 0 but got " + distance);
    }
}
