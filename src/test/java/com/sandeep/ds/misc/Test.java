package com.sandeep.ds.misc;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by smahanty on 7/12/16.
 */
public class Test {
    static WebDriver driver;

    static void init() {
        System.setProperty("webdriver.chrome.driver", "/Users/smahanty/Downloads/chromedriver");
        driver = new ChromeDriver();
    }

    public static void main(String[] args) throws Exception {
        init();
        login();
        ArrayList<String> list = readMovies();
        ArrayList<String> records = scrape(list);
        write(records);
    }

    public static void login() {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get("https://movielens.org/login");
        driver.manage().window().maximize();
        driver.findElement(By.id("inputEmail")).sendKeys("sandeepkumarmahanty@gmail.com");
        driver.findElement(By.id("inputPassword")).sendKeys("India_2014");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        WebElement element = driver.findElement(By.xpath("//button"));
        element.submit();
    }

    public static ArrayList<String> readMovies() throws Exception {

        ArrayList<String> list = new ArrayList<>();
        InputStream is = Test.class.getClassLoader().getResourceAsStream("u.item");
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));

        String line = br.readLine();
        while (line != null) {
            String[] tokens = line.split("~");
            list.add(tokens[1]);
            line = br.readLine();
        }
        br.close();
        is.close();
        System.out.println("Data set: " + list.size());
        return list;
    }

    public static ArrayList<String> scrape(ArrayList<String> input) throws Exception {
        ArrayList<String> records = new ArrayList<>();
        int i = 1;
        for (String name : input) {
            String url = "";
            String searchText = name;

            if (name.indexOf("(") != -1) {
                searchText = name.substring(0, name.indexOf('('));
            }

            driver.findElement(By.id("omnisearch-typeahead")).sendKeys(searchText);
            List<WebElement> elements = driver.findElements(By.xpath("//button"));
            elements.get(1).click();
            WebDriverWait wait = new WebDriverWait(driver, 2);
            try {
                List<WebElement> results = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div/a/img")));

                if (results.size() > 0) {
                    url = results.get(0).getAttribute("src");
                }

                String result = name + "~" + url + "\n";
                System.out.print("Wrote (line :" + i++ + ": " + result);
                records.add(result);
                Thread.sleep(100);
            } catch (Exception e) {
                System.out.println("Element not found moving on");
                continue;
            }
        }
        return records;

    }

    public static void write(ArrayList<String> list) throws Exception {
        File file = new File("result.txt");
        FileWriter writer = new FileWriter(file);
        for (String result : list) {
            writer.write(result);
        }
        writer.close();
    }



}
