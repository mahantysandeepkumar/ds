package com.sandeep.ds.misc;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by smahanty on 7/15/16.
 */
public class RecommendationTest {
    static Gson gson = new Gson();

    public static enum Genres {
        UNKNOWN(0), ACTION(1), ADVENTURE(2), ANIMATION(3), CHILDRENS(4), COMEDY(5), CRIME(6), DOCUMENTARY(7), DRAMA(
                8), FANTASY(9), FILMNOIR(10), HORROR(
                11), MUSICAL(12), MYSTERY(13), ROMANCE(14), SCIFI(15), THRILLER(16), WAR(17), WESTERN(18);

        private final int genreCode;

        Genres(int genreCode) {
            this.genreCode = genreCode;
        }

        public int getGenreCode() {
            return this.genreCode;
        }

        public String getCodeDescription() {
            switch (this.genreCode) {
                case 0:
                    return "unknown";
                case 1:
                    return "Action";
                case 2:
                    return "Adventure";
                case 3:
                    return "Animation";
                case 4:
                    return "Children's";
                case 5:
                    return "Comedy";
                case 6:
                    return "Crime";
                case 7:
                    return "Documentary";
                case 8:
                    return "Drama";
                case 9:
                    return "Fantasy";
                case 10:
                    return "Film-Noir";
                case 11:
                    return "Horror";
                case 12:
                    return "Musical";
                case 13:
                    return "Mystery";
                case 14:
                    return "Romance";
                case 15:
                    return "Sci-Fi";
                case 16:
                    return "Thriller";
                case 17:
                    return "War";
                case 18:
                    return "Western";
            }
            return "";
        }
    }

    public static void main(String[] args) throws Exception {

        for (Genres genres : Genres.values()) {
            System.out.println("Code: " + genres.getGenreCode() + "   Value: " + genres.getCodeDescription());
        }

        MapModel dataModel = new MapModel("testdata.txt");
        ContentBasedSimilarity similarity = new ContentBasedSimilarity(dataModel);
        List<Long> recommendations = recommend(dataModel, similarity, 3);

        System.out.println("Recommended for you: ");

        for (Long itemId : recommendations) {
            Movie movie = dataModel.itemMovieMap.get(itemId);
            System.out.println(gson.toJson(movie));
        }
    }

    public static List<Long> recommend(MapModel dataModel, ContentBasedSimilarity similarity, int howMany) {
        List<Long> recommendations = new ArrayList<>();
        HashMap<Long, Double> itemSimilarityMap = findSimilarItems(dataModel, similarity);

        for (Map.Entry<Long, Double> entry : itemSimilarityMap.entrySet()) {
            if (entry.getValue() >= 0.7) {
                recommendations.add(entry.getKey());
            }
        }

        return recommendations;
    }

    public static HashMap<Long, Double> findSimilarItems(MapModel dataModel, ContentBasedSimilarity similarity) {
        HashMap<Long, Double> itemSimilarityMap = new HashMap<>();
        Set<Long> preferredItems = dataModel.itemPreferenceMap.keySet();

        Iterator<Long> iterator = preferredItems.iterator();

        while (iterator.hasNext()) {
            Long prefItem = iterator.next();
            // for each preferred item we have similar items list
            Map<Long, Double> similarItemsMap = similarity.itemSimilarities(prefItem, dataModel.toItemArray());
            //System.out.println("Similar items for : " + ((Movie) dataModel.itemMovieMap.get(prefItem)).getName());

            for (Map.Entry<Long, Double> entry : similarItemsMap.entrySet()) {
                if (entry.getValue() >= 0.7) {
                    itemSimilarityMap.put(entry.getKey(), entry.getValue());
                    Movie movie = dataModel.itemMovieMap.get(entry.getKey());
                }
            }
        }

        return itemSimilarityMap;
    }
}

/* 1. Find similar elements
            Similarity
            1-> Same Tag +0.7
            2-> Same preference +0.3
            3-> Same year +0.09
            4-> Same director +0.2
            5-> Same publisher +0.2
    2. Get top N similar elements
 */

class ContentBasedSimilarity {
    private MapModel dataModel;

    public ContentBasedSimilarity(MapModel dataModel) {
        this.dataModel = dataModel;
    }


    public double itemSimilarity(long itemID1, long itemID2) {
        Double similarity = 0.0;
        double normalize = 0.1;
        Movie movie1 = dataModel.itemMovieMap.get(itemID1);
        Movie movie2 = dataModel.itemMovieMap.get(itemID2);
        double pref1 = dataModel.itemPreferenceMap.get(itemID1);
        Double pref2 = dataModel.itemPreferenceMap.get(itemID2);

        if (movie1.getTag() == movie2.getTag()) {
            similarity += 0.7;
        }
        if (pref2 != null) {
            if (pref1 == pref2) {
                similarity += 0.3;
            } else if (Math.abs(pref1 - pref2) <= normalize) {
                similarity += 0.15;
            }
        }
        return similarity;
    }

    public Map<Long, Double> itemSimilarities(long itemID1, long[] itemID2s) {
        Map<Long, Double> itemSimilarityMap = new HashMap<>();
        int length = itemID2s.length;

        for (int i = 0; i < length; i++) {
            itemSimilarityMap.put(itemID2s[i], itemSimilarity(itemID1, itemID2s[i]));
        }

        return itemSimilarityMap;
    }

}


class MapModel {

    HashMap<Long, Double> itemPreferenceMap;
    HashMap<Long, Movie> itemMovieMap;

    public long[] toItemArray() {

        int count = 0;

        long[] movies = new long[itemMovieMap.keySet().size()];

        Iterator<Long> iterator = itemMovieMap.keySet().iterator();

        while (iterator.hasNext()) {
            movies[count++] = iterator.next();
        }

        return movies;
    }

    public MapModel(String fileName) {
        this.itemPreferenceMap = new HashMap<>();
        this.itemMovieMap = new HashMap<>();
        populateMap();
        try {
            InputStream is = Test.class.getClassLoader().getResourceAsStream(fileName);
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));

            String line = br.readLine();
            while (line != null) {
                String[] tokens = line.split(",");
                itemPreferenceMap.put(Long.parseLong(tokens[0]), Double.parseDouble(tokens[1]));
                line = br.readLine();
            }
            br.close();
            is.close();
        } catch (Exception ex) {
            System.out.println("Failed to create data model because of: " + ex.getMessage());
        }
    }

    private void populateMap() {
        itemMovieMap.put(1L, new Movie(1, 1, "How to train your Dragon"));
        itemMovieMap.put(2L, new Movie(2, 1, "How to train your Dragon 2"));
        itemMovieMap.put(3L, new Movie(3, 1, "Big Hero 6"));
        itemMovieMap.put(4L, new Movie(4, 1, "Inside Out"));
        itemMovieMap.put(5L, new Movie(5, 1, "Minions"));
        itemMovieMap.put(6L, new Movie(6, 1, "Finding Nemo"));
        itemMovieMap.put(7L, new Movie(7, 1, "Finding Dory"));

        itemMovieMap.put(8L, new Movie(8, 2, "Aliens"));
        itemMovieMap.put(9L, new Movie(9, 2, "Independence Day"));
        itemMovieMap.put(10L, new Movie(10, 2, "Star Trek"));
        itemMovieMap.put(11L, new Movie(11, 2, "Star Trek: Into darkness"));
        itemMovieMap.put(12L, new Movie(12, 2, "Star Wars: Attack of clones"));
        itemMovieMap.put(13L, new Movie(13, 2, "Predator"));
        itemMovieMap.put(14L, new Movie(14, 2, "Alien Vs Predator"));
        itemMovieMap.put(15L, new Movie(15, 2, "Jurrasic World"));

        itemMovieMap.put(16L, new Movie(16, 3, "Man from U.N.C.L.E"));
        itemMovieMap.put(17L, new Movie(17, 3, "Kingsman"));
        itemMovieMap.put(18L, new Movie(18, 3, "Die another day"));
        itemMovieMap.put(19L, new Movie(19, 3, "Casino Royale"));
        itemMovieMap.put(20L, new Movie(20, 3, "Sherlock Holmes"));
        itemMovieMap.put(21L, new Movie(21, 3, "Sherlock Holmes: A game of shadows"));
        itemMovieMap.put(22L, new Movie(22, 3, "Elementary"));
        itemMovieMap.put(23L, new Movie(23, 3, "The judge"));

        itemMovieMap.put(24L, new Movie(24, 4, "Iron Man"));
        itemMovieMap.put(25L, new Movie(25, 4, "Iron Man 2"));
        itemMovieMap.put(26L, new Movie(26, 4, "Thor"));
        itemMovieMap.put(27L, new Movie(27, 4, "Captain America: The first avenger"));
        itemMovieMap.put(28L, new Movie(28, 4, "Marvel's Avengers"));
        itemMovieMap.put(29L, new Movie(29, 4, "The Dark knight"));
        itemMovieMap.put(30L, new Movie(39, 4, "Batman Begins"));
        itemMovieMap.put(31L, new Movie(31, 4, "Dark knight returns"));
    }
}


class Movie {
    private String name;
    private long id;
    private int tag;
    private int year;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public Movie(long id, int tag, String name) {
        this.id = id;
        this.tag = tag;
        this.name = name;
    }
}

