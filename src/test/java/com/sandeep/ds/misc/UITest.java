package com.sandeep.ds.misc;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by smahanty on 7/22/16.
 */
public class UITest {

    private String WAR_PATH;
    private String DEPLOY_ENV;
    private String RESULT;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Deploy war");
        frame.setSize(550, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new UITest().getLayout();
        frame.add(panel);
        frame.setVisible(true);

    }

    public JPanel getLayout() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JLabel formTitle = new JLabel("War deployer");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 40;      //make this component tall
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(formTitle, c);

        final JLabel selectedFile = new JLabel("File chosen: None");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        panel.add(selectedFile, c);

        final JButton fileChooser = new JButton("Choose");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 3;
        c.gridy = 1;
        fileChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "War & Jar Files", "war", "jar");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(fileChooser);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    WAR_PATH = chooser.getSelectedFile().getAbsolutePath();
                    selectedFile.setText("File chosen: " + chooser.getSelectedFile().getName());
                }
            }
        });
        panel.add(fileChooser, c);

        final JLabel environmentTitle = new JLabel("Environment:");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 0;
        c.gridy = 2;
        panel.add(environmentTitle, c);

        final JComboBox<String> environment = new JComboBox<>(new String[]{"QA", "SQA", "Staging"});
        environment.setSelectedIndex(2);
        environment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox<String> comboBox = (JComboBox<String>) e.getSource();
                DEPLOY_ENV = (String) comboBox.getSelectedItem();
            }
        });
        c.weightx = 0.5;
        c.gridx = 3;
        c.gridy = 2;
        panel.add(environment, c);

        final JTextArea displayProgress = new JTextArea(RESULT);

        JButton deployButton = new JButton("Deploy");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 10;      //make this component tall
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 3;
        deployButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                displayProgress.setText(displayProgress.getText() + "\n" + RunTimeExecutor.executeCommand("scp -S gwsh " + WAR_PATH + " root@192.168.24.41:/root/"));
            }
        });
        panel.add(deployButton, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 40;      //make this component tall
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 4;
        panel.add(displayProgress, c);
        return panel;
    }
}

class RunTimeExecutor {

    public static String executeCommand(String command) {
        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();
    }
}
