package com.sandeep.ds.dynamicprogramming;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class RangeSumQueryTest {


    @DataProvider(name = "rangeQueries")
    public Object[][] rangeQueryDataProvider() {
        return new Object[][]{
                {new int[]{0, 2}, 1},
                {new int[]{2, 5}, -1},
                {new int[]{0, 5}, -3},
                {new int[]{2, 3}, -2}
        };
    }

    @Test(dataProvider = "rangeQueries")
    public void sampleTest(int[] range, int expectedOutput) {
        int[] input = {-2, 0, 3, -5, 2, -1};

        int actualOutput = new RangeSumQuery(input).sumRange(range[0], range[1]);

        Assert.assertEquals(actualOutput, expectedOutput);
    }
}
