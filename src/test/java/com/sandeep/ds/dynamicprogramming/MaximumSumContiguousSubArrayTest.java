package com.sandeep.ds.dynamicprogramming;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MaximumSumContiguousSubArrayTest {

    MaximumSumContiguousSubArray obj = new MaximumSumContiguousSubArray();

    @DataProvider(name = "dataProvider")
    public Object[][] rangeQueryDataProvider() {
        return new Object[][]{
                {new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}, 6},
        };
    }

    @Test(dataProvider = "dataProvider")
    public void sampleTest(int[] nums, int expectedOutput) {
        int actualOutput = obj.maxSubArray(nums);
        Assert.assertEquals(actualOutput, expectedOutput);
    }

}
