package com.sandeep.ds.dynamicprogramming;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class HouseRobberTest {
    HouseRobber obj = new HouseRobber();

    @DataProvider(name = "dataProvider")
    public Object[][] rangeQueryDataProvider() {
        return new Object[][]{
                {new int[]{2, 1, 1, 2}, 4},
        };
    }

    @Test(dataProvider = "dataProvider", enabled = false)
    public void sampleTest(int[] nums, int expectedOutput) {
        int actualOutput = obj.rob(nums);
        Assert.assertEquals(actualOutput, expectedOutput);
    }
}
