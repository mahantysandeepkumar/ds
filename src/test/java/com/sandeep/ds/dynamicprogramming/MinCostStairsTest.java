package com.sandeep.ds.dynamicprogramming;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MinCostStairsTest {

    MinCostStairs obj = new MinCostStairs();

    @Test
    public void sampleTest() {
        int[] input = {10,15,20};
        int expectedOutput = 15;
        int actualOutput = obj.minCostClimbingStairs(input);

        Assert.assertEquals(actualOutput, expectedOutput);
    }

    @Test
    public void sampleTestTwo() {
        int[] input = {1, 100, 1, 1, 1, 100, 1, 1, 100, 1};
        int expectedOutput = 6;
        int actualOutput = obj.minCostClimbingStairs(input);

        Assert.assertEquals(actualOutput, expectedOutput);
    }
}
