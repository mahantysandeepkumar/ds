package com.sandeep.ds.dynamicprogramming;

import java.util.HashMap;
import java.util.Scanner;

public class AliensAtTrain {
    static HashMap<Integer, Integer> stationToDistanceMap = new HashMap<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int cases = 0; cases < t; cases++) {
            int numStations = sc.nextInt();
            int maxPeople = sc.nextInt();

            int[] people = new int[numStations];
            for (int i = 0; i < numStations; i++) {
                people[i] = sc.nextInt();
            }
            int max = findCount(people, maxPeople);
            System.out.println(max + " " + stationToDistanceMap.get(max));
        }
    }

    static int findCount(int[] arr, int maxPeople) {
        return dp(arr, 0, maxPeople);
    }

    static int dp(int[] arr, int i, int maxPeople) {
        if (i > arr.length - 1) {
            return Integer.MAX_VALUE;
        }

        if (maxPeople < 0) {
            return Integer.MAX_VALUE;
        }

        if (maxPeople - arr[i] < 0) {
            stationToDistanceMap.put(maxPeople, i);
            return maxPeople;
        }

        if (maxPeople - arr[i] == 0) {
            stationToDistanceMap.put(0, i);
            return maxPeople;
        }

        int distWith = dp(arr, i + 1, maxPeople - arr[i]);
        int distWithout = dp(arr, i + 1, maxPeople);

        if (distWith < distWithout) {
            stationToDistanceMap.put(distWith, i);
        } else {
            stationToDistanceMap.put(distWithout, i);
        }
        return Math.min(distWith, distWithout);
    }
}
