package com.sandeep.ds.linkedlists;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Iterator;

/**
 * Created by smahanty on 11/15/16.
 */
public class LinkedListTest {

    @Test
    public void basicTest() {
        SingleLinkedList<Integer> testList = new SingleLinkedList<>();

        for (int i = 0; i < 5; i++) {
            testList.add(i + 1);
        }

        String expResult = "[1,2,3,4,5]";
        String actualResult = testList.toString();
        Assert.assertTrue(expResult.equals(actualResult), " Expected : " + expResult + " but got : " + actualResult);
    }


    @Test
    public void basicIteratorTest() {
        SingleLinkedList<Integer> testList = new SingleLinkedList<>();
        String expResult = "[1,2,3,4,5,]";
        String actualResult = "[";

        for (int i = 0; i < 5; i++) {
            testList.add(i + 1);
        }

        Iterator<Integer> itr = testList.iterator();

        while (itr.hasNext()) {
            actualResult += itr.next() + ",";
        }

        actualResult += "]";

        Assert.assertTrue(expResult.equals(actualResult), " Expected : " + expResult + " but got : " + actualResult);
    }
}
