package com.sandeep.ds.linkedlists;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 11/15/16.
 */
public class FindMiddleElementOfLinkedListTest {
    FindMiddleElementOfLinkedList obj = new FindMiddleElementOfLinkedList();

    @Test
    public void oneElementTest() {
        SingleLinkedList<Integer> testList = new SingleLinkedList<>();

        for (int i = 0; i < 1; i++) {
            testList.add(i + 1);
        }

        int expResult = 1;
        int actualResult = obj.findMiddleElement(testList);
        Assert.assertTrue(expResult == actualResult, " Expected : " + expResult + " but got : " + actualResult);
    }

    @Test
    public void twoElementTest() {
        SingleLinkedList<Integer> testList = new SingleLinkedList<>();

        for (int i = 0; i < 2; i++) {
            testList.add(i + 1);
        }

        int expResult = 1;
        int actualResult = obj.findMiddleElement(testList);
        Assert.assertTrue(expResult == actualResult, " Expected : " + expResult + " but got : " + actualResult);
    }

    @Test
    public void evenElementsTest() {
        SingleLinkedList<Integer> testList = new SingleLinkedList<>();

        for (int i = 0; i < 4; i++) {
            testList.add(i + 1);
        }

        int expResult = 2;
        int actualResult = obj.findMiddleElement(testList);
        Assert.assertTrue(expResult == actualResult, " Expected : " + expResult + " but got : " + actualResult);
    }

    @Test
    public void oddElementsTest() {
        SingleLinkedList<Integer> testList = new SingleLinkedList<>();

        for (int i = 0; i < 5; i++) {
            testList.add(i + 1);
        }

        int expResult = 3;
        int actualResult = obj.findMiddleElement(testList);
        Assert.assertTrue(expResult == actualResult, " Expected : " + expResult + " but got : " + actualResult);
    }
}
