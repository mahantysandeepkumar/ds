package com.sandeep.ds.linkedlists;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 11/10/16.
 */
public class ReverseLinkedListTest {
    private ReverseLinkedList obj = new ReverseLinkedList();

    @Test
    public void sampleTest() {
        Node root = new Node(1);
        Node secondNode = new Node(2);
        Node thirdNode = new Node(3);
        Node fourthNode = new Node(4);

        thirdNode.setNext(fourthNode);
        secondNode.setNext(thirdNode);
        root.setNext(secondNode);

        Node reversedList = obj.reverse(root);
        checkResult("4,3,2,1,", reversedList);
    }

    @Test
    public void singleNodeTest() {
        Node root = new Node(1);

        Node reversedList = obj.reverse(root);
        checkResult("1,", reversedList);
    }

    @Test
    public void nullListTest() {
        Node root = null;

        Node reversedList = obj.reverse(root);
        Assert.assertNull(reversedList, "Node should be null");
    }

    public void checkResult(String expString, Node root) {
        StringBuilder br = new StringBuilder();
        Node temp = root;
        while (temp != null) {
            br.append(temp.getData() + ",");
            temp = temp.getNext();
        }
        Assert.assertTrue(br.toString().equals(expString), "Expected : " + expString + " but got: " + br.toString());
    }
}
