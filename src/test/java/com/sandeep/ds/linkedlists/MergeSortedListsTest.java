package com.sandeep.ds.linkedlists;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 11/10/16.
 */
public class MergeSortedListsTest {
    private MergeSortedLists obj = new MergeSortedLists();

    //@Test
    public void oneListOneElementTest() {
        SingleLinkedList<Integer> listOne = new SingleLinkedList<>();
        SingleLinkedList<Integer> listTwo = new SingleLinkedList<>();
        listOne.add(2);
        listOne.add(4);
        listOne.add(6);

        Node root = obj.getMergedList(listOne.getRoot(), listTwo.getRoot());
        checkResult("2,4,6,", root);
    }

    //@Test
    public void oneListMultipleElementsTest() {
        SingleLinkedList<Integer> listOne = new SingleLinkedList<>();
        SingleLinkedList<Integer> listTwo = new SingleLinkedList<>();
        listOne.add(2);

        Node root = obj.getMergedList(listOne.getRoot(), listTwo.getRoot());
        checkResult("2,", root);
    }

    //@Test
    public void equalLengthListsTest() {
        SingleLinkedList<Integer> listOne = new SingleLinkedList<>();
        SingleLinkedList<Integer> listTwo = new SingleLinkedList<>();
        listOne.add(2);
        listOne.add(4);
        listOne.add(6);
        listTwo.add(1);
        listTwo.add(3);
        listTwo.add(5);

        Node root = obj.getMergedList(listOne.getRoot(), listTwo.getRoot());
        checkResult("1,2,3,4,5,6,", root);
    }

    //@Test
    public void equalLengthListsWithDuplicatesTest() {
        SingleLinkedList<Integer> listOne = new SingleLinkedList<>();
        SingleLinkedList<Integer> listTwo = new SingleLinkedList<>();
        listOne.add(2);
        listOne.add(4);
        listOne.add(6);
        listTwo.add(1);
        listTwo.add(3);
        listTwo.add(3);

        Node root = obj.getMergedList(listOne.getRoot(), listTwo.getRoot());
        checkResult("1,2,3,3,4,6,", root);
    }

    //@Test
    public void diffLengthListsTest() {
        SingleLinkedList<Integer> listOne = new SingleLinkedList<>();
        SingleLinkedList<Integer> listTwo = new SingleLinkedList<>();
        listOne.add(2);
        listOne.add(4);
        listOne.add(6);
        listTwo.add(1);
        listTwo.add(3);

        Node root = obj.getMergedList(listOne.getRoot(), listTwo.getRoot());
        checkResult("1,2,3,4,6,", root);
    }

    public void checkResult(String expString, Node root) {
        StringBuilder br = new StringBuilder();
        Node temp = root;
        while (temp != null) {
            br.append(temp.getData() + ",");
            temp = temp.getNext();
        }
        Assert.assertTrue(br.toString().equals(expString), "Expected : " + expString + " but got: " + br.toString());
    }
}
