package com.sandeep.ds.heap;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

/**
 * Created by mahantys on 08/07/17.
 */
public class BinaryTreeHeapTest {

    @DataProvider(name = "maxHeapProvider")
    private Object[][] maxHeapDataProvider() {
        return new Object[][]{
                {new int[]{1, 2, 3, 4, 5, 6}, "[6, 5, 4, 3, 2, 1]"},
                {new int[]{6, 2, 3, 2, 5, 6}, "[6, 6, 5, 3, 2, 2]"},
                {new int[]{1}, "[1]"}
        };
    }

    @DataProvider(name = "minHeapProvider")
    private Object[][] minHeapDataProvider() {
        return new Object[][]{
                {new int[]{6, 5, 4, 3, 2, 1}, "[1, 2, 3, 4, 5, 6]"},
                {new int[]{6, 2, 3, 2, 5, 6}, "[2, 2, 3, 5, 6, 6]"},
                {new int[]{1}, "[1]"}
        };
    }

    @Test(dataProvider = "maxHeapProvider")
    public void maxHeapTests(int[] input, String expectedOutput) {
        Heap maxHeap = new Heap(input);
        Assert.assertTrue(Arrays.toString(maxHeap.toArray()).equals(expectedOutput), " Expected " + expectedOutput + " but got " + maxHeap);
    }

    @Test(dataProvider = "minHeapProvider")
    public void minHeapTests(int[] input, String expectedOutput) {
        Heap minHeap = new Heap(input, Heap.HeapType.MIN_HEAP);
        Assert.assertTrue(Arrays.toString(minHeap.toArray()).equals(expectedOutput), " Expected " + expectedOutput + " but got " + minHeap);
    }
}
