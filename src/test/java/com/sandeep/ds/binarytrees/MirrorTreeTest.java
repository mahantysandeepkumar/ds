package com.sandeep.ds.binarytrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.annotations.Test;


/**
 * Created by smahanty on 4/5/17.
 */
public class MirrorTreeTest extends TestBase {

    MirrorTree obj = new MirrorTree();

    @Test
    public void positiveTest() {
        Node root = new Node(5);
        Node leftChild = new Node(7);
        Node rightChild = new Node(10);
        Node lLeftChild = new Node(2);
        Node lRightChild = new Node(1);
        Node rRightChild = new Node(21);
        Node rLeftChild = new Node(12);


        leftChild.setRight(lRightChild);
        leftChild.setLeft(lLeftChild);

        rightChild.setRight(rRightChild);
        rightChild.setLeft(rLeftChild);

        root.setLeft(leftChild);
        root.setRight(rightChild);
        inOrder(root);

        Node mirror = obj.getMirror(root);
        System.out.println("\nMirror: ");
        inOrder(mirror);
        System.out.println("\n");
    }


    @Test
    public void positiveUnbalancedTreeTest() {
        Node root = new Node(5);
        Node leftChild = new Node(7);
        Node rightChild = new Node(10);
        Node lLeftChild = new Node(2);
        Node rRightChild = new Node(21);
        Node rLeftChild = new Node(12);

        leftChild.setLeft(lLeftChild);

        rightChild.setRight(rRightChild);
        rightChild.setLeft(rLeftChild);

        root.setLeft(leftChild);
        root.setRight(rightChild);
        inOrder(root);

        Node mirror = obj.getMirror(root);
        System.out.println("\nMirror: ");
        inOrder(mirror);
        System.out.println("\n");
    }

    public void inOrder(Node root) {
        if (root == null) {
            return;
        }

        inOrder(root.getLeft());
        System.out.print(root.getData() + ", ");
        inOrder(root.getRight());
    }

}
