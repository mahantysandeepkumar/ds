package com.sandeep.ds.binarytrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 8/2/16.
 */
public class CheckIfFullBinaryTreeTest{

    CheckIfFullBinaryTree obj = new CheckIfFullBinaryTree();

    @Test
    public void genericTest() {
        Node root = new Node(5);
        root.setLeft(new Node(12));
        root.setRight(new Node(6));
        root.getLeft().setLeft(new Node(1));
        root.getLeft().setRight(new Node(9));

        Assert.assertTrue(obj.isBinaryTree(root), " The tree is a full binary tree.");
    }

    @Test
    public void nullRootTest() {
        Node root = null;

        Assert.assertTrue(obj.isBinaryTree(root), " The tree is a full binary tree.");
    }

    @Test
    public void onlyRootTest() {
        Node root = new Node(10);

        Assert.assertTrue(obj.isBinaryTree(root), " The tree is a full binary tree.");
    }

    @Test
    public void genericNegativeTest() {
        Node root = new Node(5);
        root.setLeft(new Node(12));
        root.setRight(new Node(6));
        root.getLeft().setLeft(new Node(1));

        Assert.assertFalse(obj.isBinaryTree(root), " The tree is not a full binary tree.");
    }

    @Test
    public void leftSkewTreeTest() {
        Node root = new Node(5);
        root.setLeft(new Node(12));
        root.getLeft().setLeft(new Node(6));

        Assert.assertFalse(obj.isBinaryTree(root), " The tree is not a full binary tree.");
    }

    @Test
    public void rightSkewTreeTest() {
        Node root = new Node(5);
        root.setRight(new Node(12));
        root.getRight().setRight(new Node(6));

        Assert.assertFalse(obj.isBinaryTree(root), " The tree is not a full binary tree.");
    }
}
