package com.sandeep.ds.binarytrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.Node;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

/**
 * Created by smahanty on 8/8/16.
 */
public class SerializeAndDeSerializeBinaryTree extends TestBase {
    public Object[] serialize(Node root) {
        Queue<Node> queue = new ArrayDeque<>();
        queue.add(root);
        ArrayList<Integer> list = new ArrayList<>();
        list.add(root.getData());
        while (!queue.isEmpty()) {
            Node node = queue.remove();
            if (node.getLeft() != null) {
                queue.add(node.getLeft());
                list.add(node.getLeft().getData());
            }
            if (node.getRight() != null) {
                queue.add(node.getRight());
                list.add(node.getRight().getData());
            }
        }
        return  list.toArray();
    }

    public static void main(String[] args) {
        SerializeAndDeSerializeBinaryTree obj = new SerializeAndDeSerializeBinaryTree();
        Node node = new Node(10);
        node.setLeft(new Node(5));
        node.setRight(new Node(3));
        node.getLeft().setLeft(new Node(2));
        node.getLeft().setRight(new Node(1));
        Object[] arr = obj.serialize(node);

        for (Object i : arr) {
            System.out.print((int)i + ", ");
        }
        System.out.println("");
    }
}
