package com.sandeep.ds.binarytrees;

import com.sandeep.ds.binarysearchtrees.model.BinarySearchTree;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by mahantys on 07/07/17.
 */
public class CloneBinaryTreeTest {
    CloneBinaryTree obj = new CloneBinaryTree();

    @DataProvider(name = "positiveTestProvider")
    private Object[][] positiveTestDataProvider() {
        return new Object[][]{
                {new int[]{5, 4, 3, 2, 1, 0}, 6},
                {new int[]{5, 4, 3, 2, 1, 0}, 4},
                {new int[]{1}, 1},
                {new int[]{1, 2, 3, 4, 5, 6}, 1},
                {new int[]{1, 4, 3, 2, 1, 0}, 3}

        };
    }

    @Test(dataProvider = "positiveTestProvider")
    public void cloneTest(int[] arr, int x) {
        BinarySearchTree bst = new BinarySearchTree(arr);
        Node clonedRoot = obj.clone(bst.getRoot());
        Assert.assertTrue(verify(bst.getRoot(), clonedRoot));
    }

    public boolean verify(Node root, Node root2) {
        if (root == null && root2 == null) {
            return true;
        }
        return root.getData() == root2.getData() && verify(root.getLeft(), root2.getLeft()) && verify(root.getRight(), root2.getRight());
    }
}
