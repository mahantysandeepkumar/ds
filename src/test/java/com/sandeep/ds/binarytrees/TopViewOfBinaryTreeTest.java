package com.sandeep.ds.binarytrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 8/2/16.
 */
public class TopViewOfBinaryTreeTest extends TestBase {
    TopViewOfBinaryTree obj = new TopViewOfBinaryTree();

    @Test
    public void genericTest() {
        Node root = new Node(10);
        root.setLeft(new Node(5));
        root.setRight(new Node(6));
        root.getRight().setRight(new Node(7));
        root.getRight().setLeft(new Node(8));
        root.getRight().getRight().setRight(new Node(9));

        String actual = obj.getTopView(root);
        Assert.assertTrue(check(actual, new int[]{5, 10, 6, 7, 9}));
    }

    @Test
    public void singleNodeTest() {
        Node root = new Node(10);

        String actual = obj.getTopView(root);
        Assert.assertTrue(check(actual, new int[]{10}));
    }

    @Test
    public void leftSkewTreeTest() {
        Node root = new Node(10);
        root.setLeft(new Node(5));
        root.getLeft().setLeft(new Node(7));
        root.getLeft().getLeft().setLeft(new Node(9));
        String actual = obj.getTopView(root);

        Assert.assertTrue(check(actual, new int[]{10, 5, 7, 9}));
    }

    public boolean check(String actual, int[] expected) {
        boolean isCorrect = true;

        for (int i = 0; i < expected.length; i++) {
            if (!actual.contains(String.valueOf(expected[i]))) {
                isCorrect = false;
                break;
            }
        }
        return isCorrect;
    }
}
