package com.sandeep.ds.binarytrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/22/16.
 */
public class MaximumSumPathTest extends TestBase {
    MaximumSumPath obj = new MaximumSumPath();

    @Test
    public void testOne() {
        MaximumSumPath obj = new MaximumSumPath();
        Node root = new Node(3);
        Assert.assertTrue(3 == obj.getMaximum(root), "Maximum sum should be 1");
    }

    @Test
    public void testTwo() {
        MaximumSumPath obj = new MaximumSumPath();
        Node root = new Node(1);
        root.setLeft(new Node(2));
        root.setRight(new Node(3));
        Assert.assertTrue(6 == obj.getMaximum(root), "Maximum sum should be 6");
    }

    @Test
    public void testThree() {
        MaximumSumPath obj = new MaximumSumPath();
        Node root = new Node(1);
        root.setLeft(new Node(2));
        root.setRight(new Node(-3));
        Assert.assertTrue(3 == obj.getMaximum(root), "Maximum sum should be 3");
    }

    @Test
    public void testFour() {
        MaximumSumPath obj = new MaximumSumPath();
        Node root = new Node(-1);
        root.setLeft(new Node(-2));
        root.setRight(new Node(-3));
        Assert.assertTrue(-1 == obj.getMaximum(root), "Maximum sum should be -1");
    }

    @Test
    public void testFive() {
        MaximumSumPath obj = new MaximumSumPath();
        Node root = new Node(-1);
        Assert.assertTrue(-1 == obj.getMaximum(root), "Maximum sum should be -1");
    }
}
