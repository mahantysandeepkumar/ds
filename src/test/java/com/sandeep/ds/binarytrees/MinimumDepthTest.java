package com.sandeep.ds.binarytrees;

import com.sandeep.base.TestBase;
import com.sandeep.ds.binarysearchtrees.model.Node;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 7/22/16.
 */
public class MinimumDepthTest extends TestBase {
    MinimumDepth obj = new MinimumDepth();

    @Test
    public void testOne() {
        Node root = new Node(1);
        root.setLeft(new Node(2));
        root.setRight(new Node(3));
        root.getLeft().setLeft(new Node(4));
        root.getLeft().setRight(new Node(5));
        Assert.assertTrue(2 == obj.getDepth(root), "Minimum should be 2");
    }

    @Test
    public void testTwo() {
        Node root = new Node(1);
        root.setLeft(new Node(3));
        Assert.assertTrue(2 == obj.getDepth(root), "Minimum should be 2");
    }

    @Test
    public void testSix() {
        Assert.assertTrue(0 == obj.getDepth(null), "Minimum should be 2");
    }

    @Test
    public void testThree() {
        Node root = new Node(1);
        Assert.assertTrue(1 == obj.getDepth(root), "Minimum should be 2");
    }

    @Test
    public void testFour() {
        Node root = new Node(1);
        root.setLeft(new Node(2));
        root.getLeft().setLeft(new Node(3));
        Assert.assertTrue(3 == obj.getDepth(root), "Minimum should be 3");
    }

    @Test
    public void testFive() {
        Node root = new Node(1);
        root.setRight(new Node(2));
        root.getRight().setRight(new Node(3));
        Assert.assertTrue(3 == obj.getDepth(root), "Minimum should be 3");
    }
}
