package com.sandeep.ds.arrays;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NumberOccuringOddNumberOfTimesTest extends TestBase {
	NumberOccuringOddNumberOfTimes obj = new NumberOccuringOddNumberOfTimes();

	@Test
	public void genericPositiveTest() {
		int[] arr = { 3, 3, 4, 2, 4, 4, 2, 4, 4 };
		int result = obj.findOddTimesOccurringELementByXOR(arr);
		Assert.assertTrue(result == 4, "The result should be true");
	}

	@Test
	public void genericNegativeTest() {
		int[] arr = { 3, 3, 4, 2, 4, 4, 2, 4 };
		int result = obj.findOddTimesOccurringELementByXOR(arr);
		Assert.assertTrue(result == 0, "The result should not be true");
	}
}
