package com.sandeep.ds.arrays;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 11/9/16.
 */
public class PathFindingTest extends TestBase {

    PathFinding obj = new PathFinding();

    @Test
    public void sampleTest() {
        int[][] matrix = new int[][]{
                {-2, -1, -2, -2, -2},
                {-2, -1, -2, -2, -2},
                {-2, -1, -2, -2, -2},
                {-2, -1, -2, -2, -2}
        };

        int pathLength = obj.findPath(matrix);

        Assert.assertTrue(pathLength == -1, "The length of path should be 5 but found " + pathLength);
    }

    @Test
    void noPathTest(){

    }
}
