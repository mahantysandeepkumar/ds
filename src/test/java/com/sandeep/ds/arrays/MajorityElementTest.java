package com.sandeep.ds.arrays;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MajorityElementTest extends TestBase {
	MajorityElement obj = new MajorityElement();

	@Test
	public void genericPositiveTest() {
		int[] arr = { 3, 3, 4, 2, 4, 4, 2, 4, 4 };
		boolean result = obj.doesMajorityElementExists(arr, 2);
		Assert.assertTrue(result, "The result should be true");
	}

	@Test
	public void genericNegativeTest() {
		int[] arr = { 3, 3, 4, 2, 4, 4, 2, 4 };
		boolean result = obj.doesMajorityElementExists(arr, 2);
		Assert.assertFalse(result, "The result should not be true");
	}
	@Test
	public void genericPositiveMooresTest() {
		int[] arr = { 3, 3, 4, 2, 4, 4, 2, 4, 4 };
		boolean result = obj.doesMajorityElementExists(arr, 1);
		Assert.assertTrue(result, "The result should be true");
	}

	@Test
	public void genericNegativeMooresTest() {
		int[] arr = { 3, 3, 4, 2, 4, 4, 2, 4 };
		boolean result = obj.doesMajorityElementExists(arr, 1);
		Assert.assertFalse(result, "The result should not be true");
	}
}
