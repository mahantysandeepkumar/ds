package com.sandeep.ds.arrays;

import com.sandeep.base.TestBase;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 9/19/16.
 */
public class TextSearchingTest extends TestBase {
    TextSearching obj = new TextSearching();

    @Test()
    public void bruteForceTest(){
        long startTime = System.currentTimeMillis();
        String s = "asajshjghjhajhsgjhgahjsgjhgashjgjhasgSandeep";
        int index = obj.findPatternWithBruteForce(s,"Sandeep");
        long endTime = System.currentTimeMillis();

        System.out.println(" Patten found at index : "+index+" in "+(endTime-startTime)+" ms");
    }
}
