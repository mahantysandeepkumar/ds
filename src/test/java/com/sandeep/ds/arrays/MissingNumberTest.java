package com.sandeep.ds.arrays;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MissingNumberTest extends TestBase {
	MissingNumber obj = new MissingNumber();

	@Test
	public void genericPositiveTest() {
		int arr[] = { 1, 2, 3, 5, 6, 7, 8 };
		int result = obj.findMissingElementByXorMethod(arr, 8);
		Assert.assertTrue(result == 4, "The result should be true");
	}

	@Test
	public void genericNegativeTest() {
		int arr[] = { 1, 2, 4, 3, 6, 7, 8 };
		int result = obj.findMissingElementByXorMethod(arr, 8);
		Assert.assertTrue(result == 5, "The result should not be true");
	}

	@Test
	public void genericPositiveTestBySum() {
		int arr[] = { 1, 2, 3, 5, 6, 7, 8 };
		int result = obj.findMissingElementBySumMethod(arr, 8);
		Assert.assertTrue(result == 4, "The result should be true");
	}

	@Test
	public void genericNegativeTestBySum() {
		int arr[] = { 1, 2, 4, 3, 6, 7, 8 };
		int result = obj.findMissingElementBySumMethod(arr, 8);
		Assert.assertTrue(result == 5, "The result should not be true");
	}
}
