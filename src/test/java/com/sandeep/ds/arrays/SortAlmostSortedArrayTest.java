package com.sandeep.ds.arrays;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;

/**
 * Created by mahantys on 07/07/17.
 */
public class SortAlmostSortedArrayTest {
    SortAlmostSortedArray obj = new SortAlmostSortedArray();

    @DataProvider
    public Object[][] dataProvider() {
        return new Object[][]{
                {new int[]{1, 2, 6, 5, 3}, "[1, 2, 3, 5, 6]"},
                {new int[]{2, 1}, "[1, 2]"},
                {new int[]{1}, "[1]"},
                {new int[]{10, 20, 60, 40, 50, 30}, "[10, 20, 30, 40, 50, 60]"},
                {new int[]{1, 5, 3}, "[1, 3, 5]"}
        };
    }

    @Test(dataProvider = "dataProvider")
    public void almostSortedTest(int[] arr, String expectedResult) {
        int[] sorted = obj.sortAlmostSortedArray(arr);
        Assert.assertTrue(Arrays.toString(sorted).equals(expectedResult), " Expected " + expectedResult + " but got " + Arrays.toString(sorted));
    }
}
