package com.sandeep.ds.arrays;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 9/9/16.
 */
public class OddOnFrontTest extends TestBase {
    OddOnFront obj = new OddOnFront();

    @Test()
    public void worstCase() {
        int[] input = {2, 3, 4, 5, 6, 1, 2};
        int[] output = obj.rearrange(input);

        Assert.assertFalse(isValid(output), " not passed");
    }

    /* 246833323222 */
    private boolean isValid(int[] arr) {
        boolean isEven = false;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                if (!isEven) {
                    isEven = true;
                }
            } else {
                if (isEven) {
                    return false;
                }
            }
        }
        return true;
    }
}
