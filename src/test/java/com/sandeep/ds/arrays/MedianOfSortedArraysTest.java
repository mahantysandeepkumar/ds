package com.sandeep.ds.arrays;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MedianOfSortedArraysTest {

    MedianOfSortedArrays obj = new MedianOfSortedArrays();

    @Test(enabled = false)
    public void sameLengthArrayTest() {
        int arrA[] = new int[]{1, 2};
        int arrB[] = new int[]{3};

        double expected = 2;
        double actual = obj.findMedian(arrA, arrB);
        Assert.assertEquals(expected, actual, " Expected: {" + expected + "} but actual: {" + actual + "}");
    }

    @Test(enabled = false)
    public void diffLengthArrayTest() {
        int arrA[] = new int[]{1, 2};
        int arrB[] = new int[]{3, 4};

        double expected = 2.5;
        double actual = obj.findMedian(arrA, arrB);
        Assert.assertEquals(expected, actual, " Expected: {" + expected + "} but actual: {" + actual + "}");
    }

    @Test(enabled = false)
    public void diffLengthMixedElementsArrayTest() {
        int arrA[] = new int[]{1, 3, 7};
        int arrB[] = new int[]{2, 4, 5, 9};

        double expected = 4;
        double actual = obj.findMedian(arrA, arrB);
        Assert.assertEquals(expected, actual, " Expected: {" + expected + "} but actual: {" + actual + "}");
    }
}
