package com.sandeep.ds.arrays;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SumEqualsXTest extends TestBase {
	
	SumEqualsX obj = new SumEqualsX();
	
	@Test
	public void genericPositiveTest(){
		int[] arr = {6,3,5,2,7};
		int x = 5;
		boolean result = obj.doesSumExists(arr, x, 1);
		Assert.assertTrue(result, "The result should be true");
	}
	
	@Test
	public void genericNegativeTest(){
		int[] arr = {6,3,5,3,7};
		int x = 5;
		boolean result = obj.doesSumExists(arr, x, 1);
		Assert.assertFalse(result, "The result should not be true");
	}
	@Test
	public void genericPositiveTestOp2(){
		int[] arr = {6,3,5,2,7};
		int x = 5;
		boolean result = obj.doesSumExists(arr, x, 2);
		Assert.assertTrue(result, "The result should be true");
	}
	
	@Test
	public void genericNegativeTestOp2(){
		int[] arr = {6,3,5,3,7};
		int x = 5;
		boolean result = obj.doesSumExists(arr, x, 2);
		Assert.assertFalse(result, "The result should not be true");
	}

}
