package com.sandeep.ds.graph;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

public class FindArticulatePointsTest {

    FindArticulatePoints obj = new FindArticulatePoints();

    @Test
    public void checkWithOneArticulatePoint() {
        Graph graph = new Graph(3, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        int[] expectedResult = {0,1,0};
        int[] actualResult = obj.findArticulatePoints(graph);
        Assert.assertTrue(verifyResults(actualResult, expectedResult), "This graph has articulate points : " + actualResult);
    }

    @Test
    public void checkWithNoArticulatePoint() {
        Graph graph = new Graph(3, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);
        graph.addEdge(0, 2);
        int[] expectedResult = {0,0,0};
        int[] actualResult = obj.findArticulatePoints(graph);
        Assert.assertTrue(verifyResults(actualResult, expectedResult), "This graph does not have articulate points");
    }

    private boolean verifyResults(int[] actual, int[] expected) {
        return Arrays.equals(actual, expected);
    }
}
