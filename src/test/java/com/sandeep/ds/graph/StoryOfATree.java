package com.sandeep.ds.graph;

import com.sandeep.base.TestBase;

import java.util.*;

/**
 * Created by smahanty on 3/17/17.
 */
public class StoryOfATree {

    LinkedList<Integer>[] adjacencyList;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        for (int i = 0; i < t; i++) {
            int vertexCount = sc.nextInt();
            Graph graph = new Graph(vertexCount, Graph.Type.UNDIRECTED);
            for (int j = 0; j < vertexCount - 1; j++) {
                int start = sc.nextInt();
                int end = sc.nextInt();
                graph.addEdge(start, end);
            }

            int guess = sc.nextInt();
            int minScore = sc.nextInt();
            HashMap<Integer, Integer> guessMap = new HashMap<>();
            for (int j = 0; j < guess; j++) {
                int parent = sc.nextInt();
                int child = sc.nextInt();
                guessMap.put(child, parent);
            }
            int positive = hasCycle(graph, guessMap);
        }
    }


    public static int hasCycle(Graph graph, HashMap<Integer, Integer> guessMap) {
        List<List<Integer>> adjacencyList = graph.getAdjacencyList();
        BitSet visited = new BitSet(graph.getAdjacencyList().size());
        int result = 0;
        for (int i = 0; i < adjacencyList.size(); i++) {
            if (!visited.get(i)) {
                if (DFS(graph, i, visited, -1, guessMap)) {
                    result++;
                }
            }
        }
        return result;
    }

    public static boolean DFS(Graph graph, int start, BitSet visited, int parent, HashMap<Integer, Integer> guessMap) {

        List<Integer> list = graph.getAdjacencyList().get(start);
        Iterator<Integer> itr = list.iterator();
        visited.set(start, true);
        while (itr.hasNext()) {
            int item = itr.next();

            if (!visited.get(item)) {
                if (DFS(graph, item, visited, start, guessMap)) {
                    return true;
                }
            } else if (guessMap.get(item) == parent) {
                return true;
            }
        }
        return false;
    }
}
