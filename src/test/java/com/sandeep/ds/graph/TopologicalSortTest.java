package com.sandeep.ds.graph;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 3/19/17.
 */
public class TopologicalSortTest {
    TopologicalSort obj = new TopologicalSort();

    @Test
    public void sampleTest() {
        String expectedResult = "5 4 2 3 1 0 ";
        Graph graph = new Graph(6, Graph.Type.DIRECTED);
        graph.addEdge(5, 2);
        graph.addEdge(5, 0);
        graph.addEdge(4, 0);
        graph.addEdge(4, 1);
        graph.addEdge(2, 3);
        graph.addEdge(3, 1);

        String actualResult = obj.topologicalSort(graph);

        Assert.assertEquals(actualResult, expectedResult, " Expected : " + expectedResult + " actual: " + actualResult);
    }
}
