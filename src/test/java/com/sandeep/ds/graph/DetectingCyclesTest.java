package com.sandeep.ds.graph;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 3/16/17.
 */
public class DetectingCyclesTest {
    DetectingCycles obj = new DetectingCycles();

    @Test
    public void directedDetectCyclePositiveTest() {
        Graph graph = new Graph(3, Graph.Type.DIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(1, 1);
        graph.addEdge(0, 2);

        boolean actualResult = obj.hasCycle(graph);
        Assert.assertTrue(actualResult, "This graph has cycle");
    }

    @Test
    public void directedDetectCycleNegativeTest() {
        Graph graph = new Graph(3, Graph.Type.DIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);

        boolean actualResult = obj.hasCycle(graph);
        Assert.assertFalse(actualResult, "This graph does not have cycle");
    }

    @Test
    public void detectCycleNegativeTest() {
        Graph graph = new Graph(3, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(1, 2);

        boolean actualResult = obj.hasCycle(graph);
        Assert.assertFalse(actualResult, "This graph does not have cycle");
    }


    @Test
    public void directedDetectCycleNotConnectedGraphNegativeTest() {
        Graph graph = new Graph(4, Graph.Type.DIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(2, 3);

        boolean actualResult = obj.hasCycle(graph);
        Assert.assertFalse(actualResult, "This graph does not have cycle");
    }

    @Test
    public void directedDetectCycleNotConnectedGraphPositiveTest() {
        Graph graph = new Graph(4, Graph.Type.DIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);
        boolean actualResult = obj.hasCycle(graph);
        Assert.assertTrue(actualResult, "This graph has cycle");
    }

    @Test
    public void unDirectedDetectCycleSelfConnectedGraphPositiveTest() {
        Graph graph = new Graph(1, Graph.Type.DIRECTED);
        graph.addEdge(0, 0);
        boolean actualResult = obj.hasCycle(graph);
        Assert.assertTrue(actualResult, "This graph has cycle");
    }
}
