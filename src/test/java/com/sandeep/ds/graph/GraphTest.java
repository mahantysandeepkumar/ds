package com.sandeep.ds.graph;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 3/16/17.
 */
public class GraphTest {

    @Test
    public void directedBreadthFirstSearchTest() {
        Graph graph = new Graph(4, Graph.Type.DIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);
        String expectedResult = "2,0,3,1,";
        String actualResult = graph.BFS(2);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void directedBreadthFirstSearchTestWithSingleVertexCycle() {
        Graph graph = new Graph(1, Graph.Type.DIRECTED);
        graph.addEdge(0, 0);
        String expectedResult = "0,";
        String actualResult = graph.BFS(0);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void directedBreadthFirstSearchTestWithTwoVertexCycles() {
        Graph graph = new Graph(2, Graph.Type.DIRECTED);
        graph.addEdge(0, 0);
        graph.addEdge(1, 1);
        graph.addEdge(0, 1);
        String expectedResult = "0,1,";
        String actualResult = graph.BFS(0);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void directedBreadthFirstSearchTestWithTwoIsolatedVertex() {
        Graph graph = new Graph(2, Graph.Type.DIRECTED);
        graph.addEdge(0, 0);
        graph.addEdge(1, 1);
        String expectedResult = "0,";
        String actualResult = graph.BFS(0);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void directedBreadthFirstSearchTestWithNonDirectConnectedVertex() {
        Graph graph = new Graph(2, Graph.Type.DIRECTED);
        graph.addEdge(0, 0);
        graph.addEdge(0, 1);
        String expectedResult = "1,";
        String actualResult = graph.BFS(1);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void directedBreadthFirstSearchTestWithTwoIsolatedVertexNegative() {
        Graph graph = new Graph(2, Graph.Type.DIRECTED);
        graph.addEdge(0, 0);
        graph.addEdge(1, 1);
        String expectedResult = "1,";
        String actualResult = graph.BFS(1);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    public void breadthFirstSearchTest() {
        Graph graph = new Graph(4, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);
        String expectedResult = "2,0,3,1,";
        String actualResult = graph.BFS(2);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void breadthFirstSearchTestWithSingleVertexCycle() {
        Graph graph = new Graph(1, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 0);
        String expectedResult = "0,";
        String actualResult = graph.BFS(0);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void breadthFirstSearchTestWithTwoVertexCycles() {
        Graph graph = new Graph(2, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 0);
        graph.addEdge(1, 1);
        graph.addEdge(0, 1);
        String expectedResult = "0,1,";
        String actualResult = graph.BFS(0);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void breadthFirstSearchTestWithTwoIsolatedVertex() {
        Graph graph = new Graph(2, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 0);
        graph.addEdge(1, 1);
        String expectedResult = "0,";
        String actualResult = graph.BFS(0);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void breadthFirstSearchTestWithTwoIsolatedVertexNegative() {
        Graph graph = new Graph(2, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 0);
        graph.addEdge(1, 1);
        String expectedResult = "1,";
        String actualResult = graph.BFS(1);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }


    @Test
    public void directedDepthFirstSearchTest() {
        Graph graph = new Graph(4, Graph.Type.DIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);
        String expectedResult = "2,0,1,3,";
        String actualResult = graph.DFS(2);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

    @Test
    public void depthFirstSearchTest() {
        Graph graph = new Graph(4, Graph.Type.UNDIRECTED);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);
        String expectedResult = "2,0,1,3,";
        String actualResult = graph.DFS(2);
        Assert.assertEquals(actualResult, expectedResult, "Expected " + expectedResult + " but got " + actualResult);
    }

}
