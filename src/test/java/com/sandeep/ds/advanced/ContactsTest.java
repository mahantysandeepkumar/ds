package com.sandeep.ds.advanced;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ContactsTest {

    Contacts obj = new Contacts();

    @BeforeClass
    public void init() {
        obj.insert("sand");
        obj.insert("sandy");
        obj.insert("sandeep");
        obj.insert("aroma");
        obj.insert("around");
        obj.insert("are");
        obj.insert("arone");
        obj.insert("atone");
    }

    @Test
    public void testWhenOneElement() {
        Assert.assertTrue(obj.isPresent("sandeep"));
    }

    @Test
    public void testWhenElementNotPresent() {
        Assert.assertFalse(obj.isPresent("boi"));
    }

    @Test
    public void testWithPrefixMatchingElement() {
        Assert.assertFalse(obj.isPresent("sandal"));
    }

    @Test
    public void testWithPrefixElement() {
        Assert.assertFalse(obj.isPresent("san"));
    }

    @Test
    public void testWith4CharPrefixElement() {
        Assert.assertEquals(obj.find("sand"), 3);
    }

    @Test
    public void testWithPrefixWordElement() {
        Assert.assertEquals(obj.find("sandy"), 1);
    }

    @Test
    public void testWithNonExistingElement() {
        Assert.assertEquals(obj.find("mand"), 0);
    }

    @Test
    public void testWithOneCharElement() {
        Assert.assertEquals(obj.find("a"), 5);
    }

    @Test
    public void testWithTwoCharElement() {
        Assert.assertEquals(obj.find("ar"), 4);
    }

    @Test
    public void testWithThreeCharElement() {
        Assert.assertEquals(obj.find("aro"), 3);
    }

    @Test
    public void testWithFullCharElement() {
        Assert.assertEquals(obj.find("are"), 1);
    }

}
