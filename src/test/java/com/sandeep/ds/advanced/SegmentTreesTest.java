package com.sandeep.ds.advanced;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SegmentTreesTest {
    SegmentTrees obj = new SegmentTrees();

    @Test
    public void positiveTest() throws Exception{
        String input = "5 5\n" +
                "1 5 2 4 3\n" +
                "q 1 5\n" +
                "q 1 3\n" +
                "q 3 5\n" +
                "u 3 6\n" +
                "q 1 5";

        String expectedOutput = "1 1 2 1";
        String actualOutput = obj.query(input);

        Assert.assertEquals(actualOutput.trim(), expectedOutput, "Expected " + expectedOutput + " but got " + actualOutput + " instead.");
    }
}
