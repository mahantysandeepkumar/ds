package com.sandeep.ds.stringprocessing;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 10/28/16.
 */
public class StringSearchingTest extends TestBase {
    StringSearching obj = new StringSearching();

    @Test
    public void positiveTest() {
        String text = "abcsandeepasasas";
        String pattern = "sandeep";
        int result = obj.findByBoyreMooreMethod(text, pattern);
        Assert.assertTrue(result == 3, " Match expected 3 but found : " + result);
    }

    @Test
    public void negativeTest() {
        String text = "abcdwqwertyuiops";
        String pattern = "sandeep";
        int result = obj.findByBruteForce(text, pattern);
        Assert.assertTrue(result == -1, " Match should not be found");
    }

    @Test
    public void patternGreaterThanTextTest() {
        String text = "abcd";
        String pattern = "abcde";
        int result = obj.findByBruteForce(text, pattern);
        Assert.assertTrue(result == -1, " Match should not be found");
    }
}
