package com.sandeep.ds.stringprocessing;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by mahantys on 08/07/17.
 */
public class LongestPalindromicSubStringTest {
    LongestPalindromicSubString obj = new LongestPalindromicSubString();

    @Test
    public void positiveTest() {
        String input = "forgeeksskeegfor";
        String output = obj.findPalindromicSubString(input);
        Assert.assertEquals(output, "geeksskeeg", " Expected geeksskeeg but got " + output);
    }
}
