package com.sandeep.ds.sorting;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.Arrays;

public class SortPerformanceTest {
    ArrayList<Sort> sortingAlgorithms = new ArrayList<>();

    @BeforeClass
    public void setUp() {
        sortingAlgorithms.add(new QuickSort());
        sortingAlgorithms.add(new MergeSort());
    }

    long startTime;
    long endTime;

    @DataProvider(name = "positiveDataProvider")
    public Object[][] positiveDataProvider() {
        return new Object[][]{
                {new int[]{4, 3, 1, 6, 2, 5}, new int[]{1, 2, 3, 4, 5, 6}},
                {new int[]{4, 3}, new int[]{3, 4}},
                {new int[]{4}, new int[]{4}},
                {new int[]{9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}}
        };
    }


    @DataProvider(name = "negativeDataProvider")
    public Object[][] negativeDataProvider() {
        return new Object[][]{
                {new int[]{4}, new int[]{4}},
                {new int[]{4, 4, 4, 4, 4, 4}, new int[]{4, 4, 4, 4, 4, 4}},
                {new int[]{11, 3, 5, 11, 7, 1}, new int[]{1, 3, 5, 7, 11, 11}},
        };
    }


    @Test(dataProvider = "positiveDataProvider")
    public void positiveTests(int[] arr, int[] expectedResult) {
        for (Sort sort : sortingAlgorithms) {
            long currentTime = System.currentTimeMillis();
            int[] actualResult = sort.sort(arr);
            long endTime = System.currentTimeMillis();
            System.out.println("Input: " + Arrays.toString(arr) + " Algorithm: " + sort.getClass().getName() + "Time:  " + (endTime - currentTime) + " ms");
            Assert.assertTrue(Arrays.equals(actualResult, expectedResult), " Expected " + expectedResult);
        }
    }

    @Test(dataProvider = "negativeDataProvider")
    public void negativeTests(int[] arr, int[] expectedResult) {
        for (Sort sort : sortingAlgorithms) {
            long currentTime = System.currentTimeMillis();
            int[] actualResult = sort.sort(arr);
            long endTime = System.currentTimeMillis();
            System.out.println("Input: " + Arrays.toString(arr) + " Algorithm: " + sort.getClass().getName() + "Time:  " + (endTime - currentTime) + " ms");
            Assert.assertTrue(Arrays.equals(actualResult, expectedResult), " Expected " + expectedResult);
        }
    }
}
