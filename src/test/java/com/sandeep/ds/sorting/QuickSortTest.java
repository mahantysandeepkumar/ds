package com.sandeep.ds.sorting;

import com.sandeep.base.TestBase;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 4/7/17.
 */
public class QuickSortTest extends TestBase {
    QuickSort obj = new QuickSort();

    @DataProvider(name = "positiveDataProvider")
    public Object[][] positiveDataProvider() {
        return new Object[][]{
                {new int[]{4, 3, 1, 6, 2, 5}, "1,2,3,4,5,6"},
                {new int[]{4, 3}, "3,4"},
                {new int[]{4}, "4"},
                {new int[]{9, 8, 7, 6, 5, 4, 3, 2}, "1,2,3,4,5,6,7,8,9"}
        };
    }


    @DataProvider(name = "negativeDataProvider")
    public Object[][] negativeDataProvider() {
        return new Object[][]{
                {new int[]{4}, "4"},
                {new int[]{4, 4, 4, 4, 4, 4}, "4,4,4,4,4,4"},
                {new int[]{11, 3, 5, 11, 7, 1}, "1,3,5,7,11,11"},
        };
    }

    @Test(dataProvider = "positiveDataProvider")
    public void positiveTests(int[] arr, String expectedResult) {
        Assert.assertTrue(assertSorted(obj.sort(arr)), " Expected " + expectedResult);
    }

    @Test(dataProvider = "negativeDataProvider")
    public void negativeTests(int[] arr, String expectedResult) {
        Assert.assertTrue(assertSorted(obj.sort(arr)), " Expected " + expectedResult);
    }


    private boolean assertSorted(int[] actual) {
        boolean isSorted = true;

        for (int i = 0; i < actual.length - 1; i++) {
            if (actual[i] > actual[i + 1]) {
                return false;
            }
        }

        return isSorted;
    }
}
