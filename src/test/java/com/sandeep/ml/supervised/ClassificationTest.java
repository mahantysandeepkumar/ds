package com.sandeep.ml.supervised;

import com.sandeep.ml.DataPoint;
import com.sandeep.utils.FileUtil;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smahanty on 5/12/17.
 */
public class ClassificationTest {
    Classification obj = new Classification();

    @DataProvider(name = "handwritingDataProvider")
    public Object[][] handwritingDataProvider() {

        return new Object[][]{
                {"0_1.txt", "0"},
                {"1_25.txt", "1"},
                {"2_10.txt", "2"},
                {"3_36.txt", "3"},
        };
    }

    @Test
    public void dummyTest() {
        List<DataPoint> dataPointList = new ArrayList<>();
        dataPointList.add(new DataPoint(new double[]{1, 1.1}, "A"));
        dataPointList.add(new DataPoint(new double[]{1, 1}, "A"));
        dataPointList.add(new DataPoint(new double[]{0, 0}, "B"));
        dataPointList.add(new DataPoint(new double[]{0, .1}, "B"));
        String label = obj.getClassificationWithKNearest(dataPointList, 3, new DataPoint(new double[]{1, 1}, ""));
        Assert.assertTrue(label.equals("A"), "Expected A but got " + label);
    }

    @Test(dataProvider = "handwritingDataProvider")
    public void handWritingTest(String fileName, String expectedOutput) {
        List<DataPoint> dataPointMap = FileUtil.getFeaturesListFromFile();
        DataPoint toTest = FileUtil.getFeatureFromFileToTest(fileName);

        String label = obj.getClassificationWithKNearest(dataPointMap, 10, toTest);

        Assert.assertTrue(label.equalsIgnoreCase(expectedOutput), " Expected to be 3 but found : " + label);
    }
}
