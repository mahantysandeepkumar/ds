package com.sandeep.base;

import java.util.HashMap;

/**
 * Created by smahanty on 4/12/17.
 */
public class Test {
    private String name;
    private int result;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "Test{" +
                "name='" + name + '\'' +
                ", result=" + result +
                '}';
    }

    public void setResult(int result) {
        this.result = result;
    }
}
