package com.sandeep.java.designpatterns.structural.proxy;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 5/30/17.
 */
public class ProxyTest {
    private CommandExecutorProxy proxy;

    @Test
    public void adminTest() {
        proxy = new CommandExecutorProxy("sandeep", "sandeep");
        Assert.assertTrue(proxy.execute("ls -ltr"));
    }

    @Test
    public void nonAdminTest() {
        proxy = new CommandExecutorProxy("sandeep", "abc");
        Assert.assertTrue(proxy.execute("ls -ltr"));
    }

    @Test
    public void nonAdminNoAccessTest() {
        proxy = new CommandExecutorProxy("sandeep", "abc");
        Assert.assertFalse(proxy.execute("rm"));
    }
}
