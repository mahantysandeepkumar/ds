package com.sandeep.java.designpatterns.creational.prototype;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 5/29/17.
 */
public class PrototypeTest {

    @Test
    public void sameDataTest() {
        Employees emp1 = new Employees();
        emp1.loadData();
        Employees emp2 = emp1.clone();
        Assert.assertTrue(emp1.getEmployees().equals(emp2.getEmployees()));
    }

    @Test
    public void dataChangedTest() {
        Employees emp1 = new Employees();
        emp1.loadData();
        Employees emp2 = emp1.clone();
        emp2.getEmployees().remove(0);
        Assert.assertTrue(emp1.getEmployees().size() != emp2.getEmployees().size());
    }
}
