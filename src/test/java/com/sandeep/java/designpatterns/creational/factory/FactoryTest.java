package com.sandeep.java.designpatterns.creational.factory;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 5/29/17.
 */
public class FactoryTest {

    @Test
    public void gamingPCTest() {
        Computer computer = ComputerFactory.getComputer("GAMINGPC", "Core i7 - 7700K", "GTX - 1080 Ti", 16, 1);
        Assert.assertTrue(computer.getGPU() != null);
    }

    @Test
    public void serverTest() {
        Computer computer = ComputerFactory.getComputer("SERVER", "Core i7 - 7700K", "GTX - 1080 Ti", 16, 1);
        Assert.assertTrue(computer.getGPU() == null);
    }

    @Test
    public void capturePCTest() {
        Computer computer = ComputerFactory.getComputer("CAPTURE", "Core i7 - 7700K", "GTX - 1080 Ti", 16, 1);
        Assert.assertTrue(computer == null);
    }


    @Test
    public void gamingPCAbstractFactoryTest() {
        Computer computer = ComputerFactory.getComputer(new GamingPCFactory("Core i7 - 7700K", 16, 1, "GTX - 1080 Ti"));
        Assert.assertTrue(computer.getGPU() != null);
    }

    @Test
    public void serverAbstractFactoryTest() {
        Computer computer = ComputerFactory.getComputer(new ServerPCFactory("Core i7 - 7700K", 16, 1));
        Assert.assertTrue(computer.getGPU() == null);
    }
}
