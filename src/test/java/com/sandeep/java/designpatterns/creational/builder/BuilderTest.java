package com.sandeep.java.designpatterns.creational.builder;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 5/29/17.
 */
public class BuilderTest {

    @Test
    public void clanArmyTest() {
        Clan clan = new Clan.ClanBuilder("Sandeep's Clan").setArmy(true).build();

        Assert.assertTrue(clan.isArmy());
    }

    @Test
    public void clanResourcesTest() {
        Clan clan = new Clan.ClanBuilder("Sandeep's Clan").setResources(true).build();

        Assert.assertTrue(clan.isResources());
    }


    @Test
    public void clanArmyNegativeTest() {
        Clan clan = new Clan.ClanBuilder("Sandeep's Clan").setArmy(false).build();

        Assert.assertFalse(clan.isArmy());
    }

    @Test
    public void clanResourcesNegativeTest() {
        Clan clan = new Clan.ClanBuilder("Sandeep's Clan").setResources(false).build();

        Assert.assertFalse(clan.isResources());
    }


}
