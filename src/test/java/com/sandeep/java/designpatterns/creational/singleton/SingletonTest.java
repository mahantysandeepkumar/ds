package com.sandeep.java.designpatterns.creational.singleton;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 5/29/17.
 */
public class SingletonTest {

    @DataProvider(name = "objectProvider")
    public Object[][] objectProvider() {
        return new Object[][]{
                {BillPughSingleton.getInstance(), BillPughSingleton.getInstance()},
                {DoubleCheckLockingSingleton.getInstance(), DoubleCheckLockingSingleton.getInstance()}
        };
    }

    @Test(dataProvider = "objectProvider")
    public void singleThreadedTest(Object obj1, Object obj2) {
        Assert.assertTrue(obj1.hashCode() == obj2.hashCode());
    }

    @Test(singleThreaded = false, invocationCount = 8, threadPoolSize = 8, dataProvider = "objectProvider")
    public void multiThreadedTest(Object obj1, Object obj2) {
        Assert.assertTrue(obj1.hashCode() == obj2.hashCode());
    }

}
