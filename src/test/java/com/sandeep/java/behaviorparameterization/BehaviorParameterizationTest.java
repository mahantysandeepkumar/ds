package com.sandeep.java.behaviorparameterization;

import com.sandeep.base.TestBase;
import com.sandeep.java.common.Apple;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by smahanty on 11/18/16.
 */
public class BehaviorParameterizationTest {
    BehaviorParameterization obj = new BehaviorParameterization();

    @Test
    public void simplePrettyPrintTest() {
        Apple apple = new Apple("green", 130);
        String expectedOutput = "An apple of 130g";
        String actualOutput = obj.getApplePrettyPrint(apple, new AppleSimpleFormatter());
        Assert.assertTrue(expectedOutput.equals(actualOutput));
    }

    @Test
    public void characteristicHeavyPrettyPrintTest() {
        Apple apple = new Apple("green", 180);
        String expectedOutput = "A heavy green apple";
        String actualOutput = obj.getApplePrettyPrint(apple, new AppleCharacteristicsFromatter());
        Assert.assertTrue(expectedOutput.equals(actualOutput));
    }

    @Test
    public void characteristicLightPrettyPrintTest() {
        Apple apple = new Apple("green", 130);
        String expectedOutput = "A light green apple";
        String actualOutput = obj.getApplePrettyPrint(apple, new AppleCharacteristicsFromatter());
        Assert.assertTrue(expectedOutput.equals(actualOutput));
    }

    @Test
    public void characteristicHeavyDiffColorPrettyPrintTest() {
        Apple apple = new Apple("red", 180);
        String expectedOutput = "A heavy red apple";
        String actualOutput = obj.getApplePrettyPrint(apple, new AppleCharacteristicsFromatter());
        Assert.assertTrue(expectedOutput.equals(actualOutput));
    }

    @Test
    public void characteristicLightDiffColorPrettyPrintTest() {
        Apple apple = new Apple("red", 130);
        String expectedOutput = "A light red apple";
        String actualOutput = obj.getApplePrettyPrint(apple, new AppleCharacteristicsFromatter());
        Assert.assertTrue(expectedOutput.equals(actualOutput));
    }

    @Test
    public void simplePrettyPrintLambdaTest() {
        Apple apple = new Apple("green", 130);
        String expectedOutput = "An apple of 130g";
        String actualOutput = obj.getApplePrettyPrint(apple, (Apple apple1) -> "An apple of " + apple.getWeight() + "g");
        Assert.assertTrue(expectedOutput.equals(actualOutput));
    }
}